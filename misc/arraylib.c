#include <stdio.h>

int display(int *l, int n);
void shiftr(int *l, int length, int start, int end);
void shiftl(int *l, int length, int start, int end);
void insert(int *l, int length, int from, int to);
int find_sorted_location(int *l, int length, int value);
void insertion_sort(int *l, int length);
int max(int *l, int length);
int min(int *l, int length);

/*void main(void) {
    int l[6] = {5,1,6,4,3,2};

    int k;
    display(l,6);
    k = min(l,6);
    printf("%d\n",k);
    //loc = find_sorted_location(l,6,3);
    //insertion_sort(l,6);
    //display(l,6);
}*/

int display(int *l, int n) {
    int i;
    //printf("%d\n",n);
    printf("[");
    for(i = 0;i < n;i++)
        if(i == (n - 1))
            printf("%d",l[i]);
        else
            printf("%d, ",l[i]);
    puts("]");
}

void shiftr(int *l, int length, int start, int end) {
    int i;
    // starting at index start, move all the elements in array l to the right,
    // ending at end, and dumping the last element
    for(i = end - 1;i >= start;i--) {
        l[i+1] = l[i];
    }
}

void shiftl(int *l, int length, int start, int end) {
    int i;
    // starting at index start, move all the elements in array l to the right,
    // ending at end, and dumping the last element
    for(i = start+1;i <= end;i++) {
        l[i-1] = l[i];
    }
}

void insert(int *l, int length, int from, int to) {
    // inserts value at from to to, shifting everything else down
    // TODO - add shiftl
    int v;
    v = l[from];
    shiftr(l,length,to, from);
    l[to] = v;
}

int find_sorted_location(int *l, int length, int value) {
    // assuming forward sorting(leastto greatest), finds out where value
    // should go if to ultimately achieve a sorted list.
    int i;
    i = 0;
    while(i < length && l[i] < value) {
        i++;
    }

    return i;
}

void insertion_sort(int *l, int length) {
    int i,prev,targetloc;
    // loop through the array. if you find that a value is less than
    // the previous value, then move that value to the correct place
    i = 0;
    prev = -1; // TODO - set to min of array
    while(i < length) {
        if(l[i] < prev) {
            targetloc = find_sorted_location(l,length,l[i]);
            insert(l,length,i,targetloc);
            display(l,length);
        } else {
            prev = l[i];
            i++;
        }
    }
}

int max(int *l, int length) {
    int k,i;
    k = l[0];
    for(i = 0;i < length;i++) {
        if(l[i] > k)
            k = l[i];
    }

    return k;
}

int min(int *l, int length) {
    int k,i;
    k = l[0];
    for(i = 0;i < length;i++) {
        if(l[i] < k)
            k = l[i];
    }

    return k;
}
