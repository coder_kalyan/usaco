"""
ID: kalyan2
LANG: PYTHON3
TASK: frac1
"""
with open("frac1.in") as f:
    N = int(f.readline())

# print(N)
names = []
vals = []

for den in range(1, N + 1):
    for num in range(0, den + 1):
        # print(num, den)
        if num / den not in vals:
            vals.append(num / den)
            names.append("{}/{}".format(num, den))

sorted_names = [x for _, x in sorted(zip(vals, names))]
print("\n".join(sorted_names))

with open("frac1.out", "w") as f:
    f.write("\n".join(sorted_names))
    f.write("\n")
