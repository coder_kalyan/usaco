#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>
#include <climits>
#include <cmath>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define RREP(i,a,b) for (long i=b-1;i>=a;i--)
#define LOG(a,b) clog << a << ": " << b << endl;
#define MP(a,b) make_pair(a,b)
#define DIST(x1,y1,x2,y2) hypot(abs(x2-x1),abs(y2-y1))

class cmp;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;
typedef priority_queue<ii> pqiif;
typedef priority_queue<ii,vii,cmp> pqiis;
typedef vector<vi> vvi;
typedef vvi::iterator vvi_i;
typedef vector<vii> vvii;
typedef vvii::iterator vvii_i;

class cmp {
    bool operator() (const ii &a, const ii &b) {
        return a.second > b.second;
    }
};

#define INF INT_MAX

const string PROG = "moocast";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

int N;

struct Edge {
    int a, b, w;
};

bool cmpEdge(Edge a, Edge b) {
    return a.w < b.w;
}

vector<Edge> edges;
vi par;

int find(int x) {
    return (x == par[x]) ? x : (par[x] = find(par[x]));
}

void merge(int x, int y) {
    par[find(x)] = find(y);
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N;
    vi x, y;
    x.resize(N);
    y.resize(N);
    REP(i,0,N)
        fin >> x[i] >> y[i];
    par.resize(N);
    Edge tmp;
    REP(i,0,N) {
        par[i] = i;
        REP(j,0,N) {
            int d1 = x[j] - x[i];
            int d2 = y[j] - y[i];
            tmp.a = i;
            tmp.b = j;
            tmp.w = (d1*d1) + (d2*d2);
            edges.push_back(tmp);
        }
    }
    sort(edges.begin(),edges.end(),cmpEdge);

    int n = 0;
    int last = 0;
    for (auto edge : edges) {
        if (find(edge.a) != find(edge.b)) {
            merge(edge.a, edge.b);
            last = edge.w;
            if (n >= N - 1)
                break;
            n++;
        }
    }

    cout << last << endl;
    fout << last << endl;

	return 0;
}
