with open("snakes.in") as f:
    N, K = map(int, f.readline().split())
    GR = list(map(int, f.readline().split()))

#  print(N, K)
#  print(GR)

dp = [[[-1 for i in range(N*2)] for j in range(K+2)] for k in range(N+2)]

def dfs(nth, size, k, waste):
    if dp[nth][k][size] != -1:
        print(dp[nth][k][size])
        #  return dp[nth][k][size]
    if nth >= N:
        return waste
    if size < GR[nth]:
        return 100000000000000000
    if k > K:
        return 100000000000000000

    cur_waste = size - GR[nth]
    ret = min(dfs(nth+1,size,k,waste+cur_waste), min([dfs(nth+1,GR[i],k+1,waste+cur_waste) for i in range(N)]))
    #  print(ret)
    #  dp[nth][k] = (size, ret)
    dp[nth][k][size] = ret
    return ret

ans = dfs(0, GR[0], 0, 0)
print(ans)
with open("snakes.out", "w") as f:
    f.write(str(ans))
    f.write("\n")
