/*
  ID: kalyan2
  LANG:C++11
  TASK: lamps
*/

#include <iostream>
#include <cstdio>
#include <fstream>
#include <vector>
#include <algorithm>
#include <bitset>
#include <sstream>
#include <string>

using namespace std;

#define MAXSIZE 200

typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef long long ll;
// typedef uint128_t li;
typedef bitset<MAXSIZE> li;


int N, C;
vi onEnd, offEnd;
vector<string> ans;
// vector< tuple<li, int, int> > memo;
//vector< pair<li, ii> > memo;
vector<pair<li, int> > memo;
bool found = false;

bool fitsReq(li out) {
  // this function assumes that out[] is of length N
  for (int on : onEnd)
    // if (!(out & (1 << on)))
    if (!out[on])
      return false;
  for (int off : offEnd)
    // if (out & (1 << off))
      if (out[off])
      return false;

  return true;
}

void flip(li &cur, int btn) {
  switch (btn) {
  case 1:
    for (int i = 1;i <= N;i ++) {
      // cur ^= (1 << i);
      cur[i] = !cur[i];
    }
    break;
  case 2:

    for (int i = 1;i < N;i += 2) {
      // cur ^= (1 << i);
      cur[i] = !cur[i];
    }
    break;

  case 3:
    for (int i = 2;i <= N;i += 2) {
      // cur ^= (1 << i);
      cur[i] = !cur[i];
    }
    break;

  case 4:
    for (int i = 1;i <= N;i += 3) {
      // cur ^= (1 << i);
      cur[i] = !cur[i];
    }
    break;

  default:
    break;
  }
}

void solve(li curprev, int c, int btn) {
  // lets perform the button press:
  li cur = curprev;
  flip(cur, btn);

  // we are computing solve for a particular combination.
  // to avoid repeating these computations, we must first
  // check if this combination of solve parameters is in
  // our memo table. If it isn't, we add it and continue...
  // pair<li, ii> comb = make_pair(cur, make_pair(c, btn));
  pair<li, int> comb = make_pair(cur, c);
  // if (memo.find(comb) != memo.end()) {
  if (find(memo.begin(), memo.end(), comb) != memo.end()) {
    // we have computed this before - break here
    return;
  } else {
    // this is a new combination - store it in memo for future reference
    memo.push_back(comb);
    }

  // if we have made c moves, check if the current solution fits the requirement
  if (c == C - 1) {
    if (fitsReq((li) (cur))) {
      string an;
      for (int i = 1;i <= N;i++) {
        //if (cur & (1 << i))
        if (cur[i])
          an += "1";
          // cout << 1;
        else
          // cout << 0;
          an += "0";
      }
      // cout << endl;
      ans.push_back(an);

      if (!found)
        found = true;
    }

    return;
  }

  // we can press btn 1, 2, 3, or 4. To allow for dp, we will pass
  // which btn into solve and recurse
  // int d1[MAXSIZE], d2[MAXSIZE], d3[MAXSIZE], d4[MAXSIZE];
  // copy(cur, cur + MAXSIZE, d1);
  // copy(cur, cur + MAXSIZE, d2);
  // copy(cur, cur + MAXSIZE, d3);
  // copy(cur, cur + MAXSIZE, d4);
  solve(cur, c + 1, 1);
  solve(cur, c + 1, 2);
  solve(cur, c + 1, 3);
  solve(cur, c + 1, 4);
}
int main() {
  ifstream fin("lamps.in"); // = fopen("lamps.in", "r");

  fin >> N >> C;
  // cout << N << " " << C << endl;

  int tmp;
  fin >> tmp;
  while (tmp != -1) {
    onEnd.push_back(tmp);
    // cout << tmp;
    fin >> tmp;
  }
  // cout << endl;

  fin >> tmp;
  while (tmp != -1) {
    offEnd.push_back(tmp);
    //  cout << tmp;
    fin >> tmp;
  }
  // cout << endl;

  freopen("lamps.out", "w", stdout);
  li allOn;
  // for (int i = 0;i < 128;i++)
  //   allOn |= (1 << i);
  allOn.reset();
  if (C != 0) {
    solve(allOn, 0, 1);
    solve(allOn, 0, 2);
    solve(allOn, 0, 3);
    solve(allOn, 0, 4);

    // sort the answers
    if (ans.size() > 1) {
      // sort(ans.begin(), ans.end(), numCmp);
      sort(ans.begin(), ans.end(), [](const string &lhs, const string &rhs)
                                   { return lhs < rhs;});
    }

    for(string answer : ans)
      cout << answer << endl;
  } else {
    // the only possible state is all turned on - if this fits requirements its an answer
    if (fitsReq(allOn)) {
      found = true;
      for (int i = 0;i < N;i++)
        cout << "1";
      cout << endl;
    }
  }

  if (!found)
    cout << "IMPOSSIBLE" << endl;
  return 0;
}
