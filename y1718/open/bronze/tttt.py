with open("tttt.in") as f:
    board = f.readline().replace("\n","")
    board += f.readline().replace("\n","")
    board += f.readline().replace("\n","")
    
print(board)
r1 = board[0:3]
r2 = board[3:6]
r3 = board[6:9]
c1 = board[0] + board[3] + board[6]
c2 = board[1] + board[4] + board[7]
c3 = board[2] + board[5] + board[8]
d1 = board[0] + board[4] + board[8]
d2 = board[2] + board[4] + board[6]

print(r1, r2, r3)
print(c1, c2, c3)
print(d1, d2)

def single(comb):
    first = comb[0]
    for c in comb[1:]:
        if c != first:
            return (False, first)

    return (True, first)

def double(comb):
    first = comb[0]
    second = -1
    for c in comb[1:]:
        if c == first:
            continue
        elif c == second:
            continue
        elif second == -1:
            second = c
        else:
            return (False, first, second)

    if second != -1:
        return (True, first, second)
    else:
        return (False, first, second)

single_count = 0
single_used = []
for comb in [r1, r2, r3, c1, c2, c3, d1, d2]:
    yes, c = single(comb)
    if yes and c not in single_used:
        single_count += 1
        single_used.append(c)

double_count = 0
double_used = []
for comb in [r1, r2, r3, c1, c2, c3, d1, d2]:
    yes, c1, c2 = double(comb)
    if yes and (c1, c2) not in double_used and (c2, c1) not in double_used:
        double_count += 1
        double_used.append((c1, c2))

print(single_count)
print(double_count)

with open("tttt.out", "w") as f:
    f.write(str(single_count) + "\n")
    f.write(str(double_count) + "\n")
