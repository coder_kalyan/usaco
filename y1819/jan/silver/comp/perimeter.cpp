#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;

#define MAXN 1000

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;

const string PROG = "perimeter";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N;
char board[MAXN][MAXN];
long region[MAXN][MAXN];
char visited[MAXN][MAXN];
// map<long, ii> regions;
vii regions;

// struct Neighbors {
    // bool n = false;
    // bool s = false;
    // bool e = false;
    // bool w = false;
// };

int neighbors(int i, int j) {
    int num = 0;
    if ((i+1>=0) && (i+1<N) && (j>=0) && (j<N))
        if (board[i+1][j] == '#')
            num += 1;
            // n.n = true;

    if ((i-1>=0) && (i-1<N) && (j>=0) && (j<N))
        if (board[i-1][j] == '#')
            num += 1;
            // n.s = true;

    if ((i>=0) && (i<N) && (j+1>=0) && (j+1<N))
        if (board[i][j+1] == '#')
            num += 1;
            // n.e = true;

    if ((i>=0) && (i<N) && (j-1>=0) && (j-1<N))
        if (board[i][j-1] == '#')
            num += 1;
            // n.w = true;

    return num;
}


void visit(int i, int j, int r) {
    cout << i << " " << j << " " << r << endl;
    if ((i<0) || (i>=N) || (j<0) || (j>=N)) return;
    if (region[i][j]!=0) return;
    if (board[i][j] == '.') return;
    regions[r].first++;
    regions[r].second += (4 - neighbors(i,j));
    region[i][j] = r;
    visit(i+1,j,r);
    visit(i-1,j,r);
    visit(i,j+1,r);
    visit(i,j-1,r);
}
        
int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N;
    REP(i,0,N)
        REP(j,0,N)
            fin >> board[j][i];

    // REP(i,0,N) {
        // REP(j,0,N)
            // cout << board[j][i];
        // cout << endl;
    // }

    memset(region,0,sizeof(region));

    long r = 1;

    regions.push_back(make_pair(0,0)); // index 0 doesn't count
    regions.push_back(make_pair(0,0));
    REP(i,0,N) {
        REP(j,0,N) {
            visit(i,j,r);
            if (regions[r].first > 0) {
                r++;
                regions.push_back(make_pair(0,0));
            }
        }
    }

    long ma = 0, mp = 0;
    REP(x,1,r) {
        if (regions[x].first >= ma) {
            ma = regions[x].first;
            mp = max(mp,regions[x].second);
        }
        // cout << regions[x].first << " ";
        // cout << regions[x].second << endl;
    }
    // cout << endl;
    
    cout << ma << " " << mp << endl;
    fout << ma << " " << mp << endl;
	return 0;
}
