/*
ID: kalyan2
LANG: C++11
TASK: humble
*/

#include <iostream>
#include <fstream>
#include <climits>
#include <vector>

using namespace std;

int K, N, S[100], m[100], h[100001];

int main() {
    ifstream fin("humble.in");
    ofstream fout("humble.out");

    fin >> K >> N;
    for (int i = 0;i < K;i++) fin >> S[i];

    fill(m, m + K, 0);
    fill(h, h + N + 1, 0);
    h[0] = 1;

    int pos = 0;
    while (pos < N + 1) {
        long next = INT_MAX;
        vector<int> incr;
        for (int i = 0;i < K;i++) {
            long tmp = S[i] * h[m[i]];
            if (tmp > h[pos] && tmp <= next) {
                if (tmp < next) {
                    next = tmp;
                    incr.resize(1);
                    incr[0] = i;
                } else if (tmp == next)
                    incr.push_back(i);
            }
        }

        h[++pos] = next;
        for (auto idx : incr) m[idx]++;
    }

    cout << h[N] << endl;
    fout << h[N] << endl;

    return 0;
}
