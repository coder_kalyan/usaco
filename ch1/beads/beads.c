/* USACO Program
ID: kalyan2
LANG: C
TASK: beads
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned short spanaway(unsigned short position, int dirn);
unsigned short previousposn(unsigned short position);
unsigned short nextposn(unsigned short position);

unsigned short N;
char beads[350];
unsigned int checkedbead[350];
// keep track of whether we've checked a bead already or not

void main(void) {
	FILE *fin, *fout;
	unsigned short maxcount, mycount, i, j;


	// open the files
	fin = fopen("beads.in","r");
	fout = fopen("beads.out", "w");

	// read in the value of  and the beads string
	fscanf(fin, "%hu\n", &N);
	fscanf(fin,"%s\n",beads);


	// ok lets look at the beads and see where a break will be max
	maxcount = 0;
	for (i = 0; i < strlen(beads); i++)
	{
		// break the necklace there

		// reset all beads to unchecked;
		for (j=0; j<strlen(beads); j++)
			checkedbead[j] = 0;

		// and count the total backwards and forwards
		mycount = spanaway(i,1);
		if (mycount < strlen(beads)) {
			mycount += spanaway(previousposn(i),-1);
			// otherwise you had the whole string, so no need to count the other way
		}
		if (mycount > maxcount)
			maxcount = mycount;
	}

	// printf("%hu\n",N);
	// printf("%s\n",beads);
	fprintf(fout,"%hu\n",maxcount);
	printf("%hu\n",maxcount);

	fclose(fin);
	fclose(fout);

	exit(0);
}


// get the number of beads of a single color going right or left from a position
// use a flag to indicate directiopn - 1 is right, any other number is left
unsigned short spanaway(unsigned short position, int dirn)
{
	char color;
	unsigned short count, myposn;
	// check what the color of the bead at the position is
	color = beads[position];
	myposn = position;
	// say its red or blue, we start going to the right
	// until we hit the opposite color or go all the way around
	checkedbead[myposn] = 1;
	if ( (color=='r') || (color=='b') )
	{
		count = 1;
		if (dirn == 1)
			myposn = nextposn(myposn);
		else
			myposn = previousposn(myposn);
		while (((beads[myposn] == color)||(beads[myposn] == 'w')) && (myposn != position) && (checkedbead[myposn]==0))
		{
			count++;
			checkedbead[myposn] = 1;
			if (dirn == 1)
				myposn = nextposn(myposn);
			else
				myposn = previousposn(myposn);
		}
		// if you went all the way round!
		// if (myposn == position)
		// {}
	}
	// but if we start with a white, then we go right until we find a non-white
	// and count from there onwards too (recursively)
	else if (color == 'w') {
		count = 1;
		myposn = nextposn(myposn);
		while ((beads[myposn] == 'w') && (myposn != position) && (checkedbead[myposn]==0))
		{
			count++;
			checkedbead[myposn] = 1;
			if (dirn == 1)
				myposn = nextposn(myposn);
			else
				myposn = previousposn(myposn);
		}
		// ok we reached a non-white color, whatever it is, we can recursively
		// assuming we did not already go all the way around
		// if you went all the way round!
		if ((checkedbead[myposn]==0) && (myposn != position ))
			count += spanaway(myposn,dirn);
	}
	return count;
}

// return the previous posn from a position in the string
unsigned short previousposn(unsigned short position)
{
	if ( position>0 )
		return position-1;
	else
		return strlen(beads)-1;
}

// return the next posn from a position in the string
unsigned short nextposn(unsigned short position)
{
	if ( position<strlen(beads)-1)
		return position+1;
	else
		return 0;
}
