with open("teleport.in") as f:
    a, b, x, y = map(int, f.readline().split())

ans = min([abs(b-a), abs(x-a) + abs(y-b),abs(y-a) + abs(x-b)])

with open("teleport.out", "w") as f:
    f.write(str(ans) + "\n")
