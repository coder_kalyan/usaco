/*
  ID: kalyan2
  LANG:C++11
  TASK: money
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>

using namespace std;

#define MAXV 25
#define MAXN 10000

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef uint_least8_t int8;

const string PROG = "money";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

short V, N;
int8 coins[MAXV];
int8 ways[MAXN][MAXV];
ll nWays;

void count(short tot) {
    cout << tot << endl;
    

}

int main() {
	ifstream fin (FIN);
    fin >> V >> N;
    uint8_t tmp;
    for (int i = 0;i < V;i++) {
        fin >> tmp;
        coins[i] = tmp;
    }   
    sort(coins,coins + V);
    for (int i = 0;i < V;i++)
        cout << coins[i] << " ";
    cout << endl;

    count(N);
    
	
	ofstream fout (FOUT);
    cout << nWays << endl;

	return 0;
}
