#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>
#include <climits>
#include <cmath>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define RREP(i,a,b) for (long i=b-1;i>=a;i--)
#define LOG(a,b) clog << a << ": " << b << endl;
#define MP(a,b) make_pair(a,b)
#define DIST(x1,y1,x2,y2) hypot(abs(x2-x1),abs(y2-y1))

#define DEBUG 1

#ifdef DEBUG
#define COUT(str) cout str;
#else
#define COUT(str) {}
#endif


class cmp;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;
typedef priority_queue<ii> pqiif;
typedef priority_queue<ii,vii,cmp> pqiis;
typedef vector<vi> vvi;
typedef vvi::iterator vvi_i;
typedef vector<vii> vvii;
typedef vvii::iterator vvii_i;

class cmp {
    bool operator() (const ii &a, const ii &b) {
        return a.second > b.second;
    }
};

#define INF INT_MAX

#define MAXN 100000

const string PROG = "cowland";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N, Q;
vi adj[MAXN];
long enj[MAXN+1], dis[MAXN+1];
bool vis[MAXN];

long bfs(long src, long dest) {
    long ret = 0;
    fill(vis,vis+MAXN,false);
    queue<ii> q;
    q.push(MP(src, enj[src]));
    while (!q.empty()) {
        ii curr = q.front();
        q.pop();
        if (vis[curr.first])
            continue;
        // cout << curr.first << " " << curr.second << endl;
        vis[curr.first] = true;
        if (curr.first == dest) {
            ret = curr.second;
            break;
        }
        for (long nb : adj[curr.first]) {
            // cout << "NB" << nb << " " << curr.second << " " << enj[nb] << endl;
            q.push(MP(nb, curr.second^enj[nb]));
        }
    }

    return ret;
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N >> Q;
    REP(i,1,N+1)
        fin >> enj[i];
    int s, a, b;
    REP(i,1,N) {
        fin >> a >> b;
        adj[a].push_back(b);
        adj[b].push_back(a);
    }
    // REP(i,1,N) {
        // for (int j : adj[i])
            // cout << j << " ";
        // cout << endl;
    // }
    // cout << endl;
    
    long ans;
    REP(i,0,Q) {
        fin >> s >> a >> b;
        if (s == 2) {
            ans = bfs(a,b);
            // cout << ans << endl;
            fout << ans << endl;
        } else {
            enj[a] = b;
        }
    }
	return 0;
}
