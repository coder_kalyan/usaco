"""
ID: kalyan2
TASK: ariprog
LANG: PYTHON3          
"""

from itertools import combinations
from math import *

with open("ariprog.in") as f:
	N = int(f.readline())
	M = int(f.readline())

twom2 = 2*M*M

bisquares = list({(p * p + q * q) for p in range(M + 1) for q in range(p, M + 1)})
bisquares.sort()

progs = []
closed = []
# the elements in the sequence

for n, square in enumerate(bisquares):
	# added_second = []
	#for pn, prog in enumerate(progs):
	for pn in range(len(progs)):
		prog = progs[pn]
		# if the progression is closed, skip it
		if len(prog) == 1:
			# this progression only has 1 element, and doesn't have a difference yet
			# so, we can add this square to that progression
			# prog[0] = square - prog[2]
			prog.append(square)
			progs.append([prog[0]])
			# added_second.append(prog[0])
		else:
			# this progression has a defined difference
			# print(prog[1] - prog[0],square - prog[-1])
			if prog[1] - prog[0] == square - prog[-1]:
				# this square fits in this progression!
				# add it
				prog.append(square)
			#elif square > prog[-1] + (prog[1] - prog[0]):
			 	# square is already higher than the next number in the progression - close it
			 	# by moving it to a closed pile
			 	# closed.append(progs[pn])
			 	#if len(prog) < N:
			 	#	del progs[pn]
			 	#pass

	# this square can also be part of a new progression - create one
	#for i in range(len(left) - n):
	# print(added_second)
	#for a in added_second:
	#	progs.append([a])
	progs.append([square])
print("finished doing this stuff")
# print(closed)
#for prog in progs:
#	if prog[0] == 12:
#		print(prog)
#print(progs)
"""
for prog in progs:
	# print(str(prog[0]) + " " + ("#" * (len(prog) - 2)))
	for num in prog[2:]:
		print(num, end=" ")
	print()
print()
"""
# print()
# progs = list(filter(lambda x: len(x) == N + 2, progs))
# for i in closed + progs:
# 	print(len(i))
# 	if len(i) >= N:
# 		print(i)
closed += progs

# closed = list(filter(lambda x: len(x) >= N, closed))
#print(closed)
#print()
# print(closed)
# progs = list(filter(lambda x: len(x) == N, progs))
#print(closed)
#print(len(closed))
#print()
for i, prog in enumerate(closed):
	num = len(prog)
	# print(i, num)
	if num > N:
		#print(prog)
		extra_num = num - N
		del closed[i]
		#print(extra_num, N)
		for e in range(extra_num):
			# create a new progression of length N, shifted by i
			shifted = prog[0 + e:5 + e]
			# print(shifted)
			closed.append(shifted)
			# print(shifted)
		#closed[i] = prog[:-extra_num]
		#print(prog)
#print()
#for c in closed:
#	if len(c) < 2:
#		print(closed)
closed = list(filter(lambda x: len(x) == N, closed))
#print(closed)
# closed = list(filter(lambda x: len(x) == N, closed))
closed = sorted(closed, key=lambda x: (x[1] - x[0], x[0]))
# closed = sorted(closed, key=lambda x:x[0])
#for prog in progs:
#	print("{} {}".format(prog[2],prog[0]))
if len(closed) == 0:
	print("NONE")
	with open("ariprog.out", "w") as f:
		f.write("NONE\n")
else:
	with open("ariprog.out", "w") as f:
		write = f.write
		for p in closed:
			print(p[0], p[1] - p[0])
			# print(p)
			write("{} {}\n".format(p[0], p[1] - p[0]))
"""
print("Finished generating bisquares")
combs = combinations(bisquares, 2)

progs = []
append = progs.append


for comb in combs:
	if search(comb[0], comb[1]):
		append((comb[0], comb[1] - comb[0]))

progs = sorted(progs, key = lambda x: (x[1], x[0]))

if len(progs) == 0:
	print("NONE")
	with open("ariprog.out", "w") as f:
		f.write("NONE\n")
else:
	with open("ariprog.out", "w") as f:
		write = f.write
		for (a, b) in progs:
			print(a, b)
			write("{} {}\n".format(a, b))
"""
