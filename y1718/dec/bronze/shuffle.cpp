#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>

using namespace std;

#define MAXN 100

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const string PROG = "shuffle";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

short N;
short unshuffle[MAXN];
ul ids[MAXN];

int main() {
	ifstream fin (FIN.c_str());
    fin >> N;
    short tmp;
    for (short id = 0;id < N;id++) {
        fin >> tmp;
        cout << tmp << " ";
        // shuffle is id -> tmp, so unshuffle is tmp -> id
        unshuffle[tmp - 1] = id;
    }
    cout << endl;

    ul tmp2;
    for (short id = 0;id < N;id++) {
        fin >> tmp2;
        cout << tmp2 << " ";
        ids[id] = tmp2;
    }
    cout << endl;

    // 3 unshuffles
    ul a[MAXN], b[MAXN];
    copy(ids,ids + N, a);
    cout << endl;
    for (int i = 0;i < 3;i++) {
        for (short id = 0;id < N;id++) {
            b[unshuffle[id]] = a[id];
        }
        copy(b, b + N, a);
        for (short id = 0;id < N;id++) {
            cout << a[id] << " ";
        }
        cout << endl;
    }
    cout << endl;
        
	ofstream fout (FOUT.c_str());
    for (short id = 0;id < N;id++) {
        cout << a[id] << endl;
        fout << a[id] << endl;
    }

	return 0;
}
