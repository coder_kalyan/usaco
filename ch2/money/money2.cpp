/*
 ID: kalyan2
 LANG: C++
 TASK: subset
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <bitset>
#include <set>

using namespace std;

typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef long long ll;
typedef unsigned long long ull;

#define MAXSIZE 39

void divide(int N, bitset<MAXSIZE> left);

short N;
short sig, halfsig;
set<ull> ans;

int main(void)
{
	ifstream fin;
  ofstream fout;

	fin.open("subset.in");

  fin >> N;

  fin.close();
  cout << N << endl;

  sig = N * (N + 1) / 2;

  // the objective is to devide the set {1..N}
  // into subsets of equal sum
  // these subsets should all equal half of
  // the sum of the original set (which itself can be calculated using sigma N)
  // as a result of this, if sigma N is not even, there will be no answers
  if (sig % 2 == 1) {
    cout << 0 << endl;
    fout.open("subset.out");
    fout << 0 << endl;
    fout.close();
  }

  halfsig = sig / 2;    // this value can be used to check if subset is half

  bitset<MAXSIZE> bb;
  bb.set();
  divide(0, bb);

  set<ull>::iterator it;
  for (it = ans.begin(); it != ans.end(); ++it) {
    ull a = *it;
    for (int i = 1; i < N; i++) {
      if ((a & (1 << i)) == 0)
        cout << i << " ";
    }
    cout << endl;
  }
  return 0;
}

// now our objective is to find all subsets of {1..N} that
// equal halfsig

void divide(int N, bitset<MAXSIZE> left) {
  if (N == halfsig) {
    // hooray! we have found an answer!
    for (int i = 0; i < N; i++) {
      if (left[i] == 0)
        cout << i << " ";
    }
    cout << endl;
    // for (int i = 1; i < N; i++) {
    //   if ((left.to_ullong() & (1 << i)) == 0)
    //     cout << i << " ";
    // }
    // cout << endl;
    ans.insert(left.to_ullong());

    return;
  } else if (N > halfsig) {
    // note: this case shouldn't really occur, but we are catching it anyway
    // we have already passed the required sum, no use going on
    return;
  }

  // so we need to go through remaining number in left
  // that is (<= halfsig - N)
  int i = 1;
  int diff = halfsig - N;
  while (i < diff) {
    // i is the next number left, if left[i] is turned on
    // cout << (left[i]) << endl;
    if (left[i]) {
      // we should now recurse, with i added to N and left[i] turned off
      left.flip(i);
      divide(N + i + 1, left);
      left.flip(i);
    }

    i++;
  }
}
