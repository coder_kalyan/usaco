"""
ID: kalyan2
LANG: PYTHON3
TASK: milk
"""

farmers = []
with open("milk.in", "r") as f:
	N, M = f.readline().replace("\n", "").split(" ")
	N = int(N)
	M = int(M)
	for i in range(M):
		price, amount = f.readline().replace("\n", "").split(" ")
		price = int(price)
		amount = int(amount)
		farmers.append([price, amount])

if N == 0:
	with open("milk.out", "w") as f:
 		f.write("0\n")
	exit(0)
farmers = sorted(farmers, key=lambda farmer:farmer[0])

i = 0
cost = 0
while i < len(farmers) and N >= farmers[i][1]:
	N -= farmers[i][1]
	cost += farmers[i][0] * farmers[i][1]
	i += 1

if len(farmers) > 1:
	cost += N * farmers[i][0]

print(cost)

with open("milk.out", "w") as f:
 	f.write("{}\n".format(cost))