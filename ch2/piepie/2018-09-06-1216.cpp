#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <unordered_set>
#include <set>

using namespace std;

#define MAXN 100000

typedef unsigned long ul;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vector<ii> vii;
typedef long long ll;
// typedef unordered_set<ul> uset;
typedef set<ul> uset;

long N, D;
ul tb[MAXN * 2], te[MAXN * 2];  // represents the taste values according to each cow


vi findPresent(ul taste, bool isBessie, uset used) {
	// find a cow a gift to give
	// problem statement says that the new gift must be at least
	// "taste" tasty, but no more than "taste + D" tasty
	// also, gifted pies cannot be regifted, so we must keep track of them
	// note - the use of dynamic programming will probably take care of this for us,
	// but we will implement it for now just in case
	vi foundInd;
    ul *tmp;
	for (ul ind = 0;ind < N;ind++) {
		// check if the tastyness of the pie fits constraints
		tmp = isBessie ? &(tb[ind]) : &(te[ind]);
		if (((*tmp) >= taste) && ((*tmp) <= (taste + D)) && (used.count(ind))) {
			// we have found a pie that works
			foundInd.push_back(ind);
		}
	}

	return foundInd;
	/*if (foundInd.size() == 0)
		return -1;
	else
		return min_element(foundInd.begin(), foundInd.end());  // we are greedy at this point - assuming that minimum value here will achieve global minimum*/
}

ul present(ul prevId, bool isBessie, uset used,ul counter) {
    // cout << "at least got called " << counter << used.size() << endl;
	// we will recursively present a pie to Bessie and Elsie, taking turns
	// we can find out the current tastyness value by looking up tb and te arrays
	// (based on whether or not isBessie is true)
	ul taste = isBessie ? tb[prevId] : te[prevId];

	// there are 2 based cases:
	// 1) if the tastyness value is 0
	if (taste == 0) {
		// problem statement says that both cows are happy at this point,
		// and the pie presents stop
		return 0;  // 0 since the trade is done, and things ended happily
	}

    // cout << "yeah" << taste << isBessie << endl;
	vi foundInd = findPresent(taste, isBessie, used);
	if (foundInd.size() == 0) {
		// apparently the cow will have to exile herself - the trading is over
		return -1;  // -1 since this was an unhappy ending
	} else {
        ul tmp, min = -1;
        for (ul pieInd : foundInd) {
            cout << used.size() << " ";
            used.insert(pieInd);
            cout << used.size() << " ";
            tmp = present(pieInd, !isBessie, used, counter + 1);
            cout << used.size() << " ";
            used.erase(pieInd);
            cout << used.size() << " ";
            cout << endl;
            if ((tmp != -1) && (min == -1))
                min = tmp;
            else if ((tmp != -1) && (tmp < min))  // remember that -1s are failures - they don't count
                min = tmp;
        }

        return min;  // if the only pie(s) found resulted in -1, min will be -1, and will be returned as we want
	}

    cout << "finished" << endl;
}

int main(void) {
	ifstream fin;
	fin.open("piepie.in");

    cout << "opened file" << endl;
	fin >> N >> D;
	for (long i = 0;i < (N * 2);i++) {
		fin >> tb[i] >> te[i];
	}

    uset empty;
    ul tmp;
    for (ul i = 0;i < N;i++) {
        // find the minimum number required starting with pie "i"
        empty.insert(i);
        tmp = present(i, true, empty, 1);
        empty.erase(i);
        
        cout << tmp << endl;
    }
}

