"""
ID: kalyan2
LANG: PYTHON3
TASK: money
"""

coins = []

nWays = 0
ways = []
def count(tot, ind, path=[]):
    global nWays

    if tot == 0:
        print(path)
        #  if sorted(path) not in ways:
            #  ways.append(sorted(path))
        nWays += 1
        return
    elif tot < 0:
        return

    for ci in range(ind, V):
        #  if len(path) > 0 and coin > path[-1]:
            #  continue
        #  if coin > tot:
            #  break
 
        #  print(tot - coins[ci])
        count(tot - coins[ci], ci, path + [coins[ci]])

with open("money.in", "r") as f:
    V, N = f.readline().split()
    V = int(V)
    N = int(N)
    for line in f:
        for word in line.split():
            coins.append(int(word))
    
coins.sort(reverse=True)
count(N, 0)
print(nWays)

with open("money.out", "w") as f:
    f.write(str(nWays) + "\n")

