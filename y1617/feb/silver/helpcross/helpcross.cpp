#include <iostream>
#include <fstream>
#include <set>
#include <vector>
#include <algorithm>

using namespace std;

typedef pair<long, long> ii;

#define MAXN 20000
long C, N;
multiset<long> T;
vector<ii> COWS;

int main() {
    ifstream fin("helpcross.in");
    ofstream fout("helpcross.out");

    fin >> C >> N;
    long t;
    for (long i = 0;i < C;i++) {
        fin >> t;
        T.insert(t);
    }
    COWS.resize(N);
    for (long i = 0;i < N;i++)
        fin >> COWS[i].second >> COWS[i].first;

    sort(COWS.begin(), COWS.end());
    long ans = 0;
    for (long i = 0;i < N;i++) {
        auto candidate = T.lower_bound(COWS[i].second);
        if (candidate != T.end() && *candidate <= COWS[i].first) {
            ans++;
            T.erase(candidate);
        }
    }

    fout << ans << endl;

    return 0;
}

