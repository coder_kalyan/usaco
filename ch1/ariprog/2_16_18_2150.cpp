#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;

/**
	Generates combinations recursively of given size from list of bisquares
	@param step counts down from combinations of size N to 0
	@param firstAvail (N - step)
	@first the first element in a found sequence, only necessary for cataloging progs
	@diff the difference of the currently built progression, to check against
	@prev the parent bisquare, to check/calculate diff
**/
void gen_combs(int step, int firstAvail, int first, int diff, int prev);

short N, M;
vi bisquares;
vii progs;

void gen_combs(int step, int firstAvail, int first, int diff, int prev) {
	if(step == 0) {
		// for(int i = 0;i < done.size();i++) {
		// 	cout << done[i] << " ";
		// }
		// cout << endl;
		progs.push_back(make_pair(diff, first));

		return;
	}

	for(int i = firstAvail;i < bisquares.size();i++) {
		if(diff != -1) {
			// this is the main case - we have everything we need to work with
			// just call the next run if we are still part of a valid sequence
			if(bisquares[i] - prev == diff) {
				gen_combs(step - 1, i + 1, first, diff, bisquares[i]);
			}
		} else if(prev != -1) {
			// this is the case for the second run - we have a previous but not a diff
			// call the next run by calculating the diff based on previous and current
			gen_combs(step - 1, i + 1, first, bisquares[i] - prev, bisquares[i]);
		} else {
			// this is the case for the first run - we don't have a previous yet
			// in this case, we should call the next run with 'temp' as our first
			// since we don't have a first
			gen_combs(step - 1, i + 1, bisquares[i], -1, bisquares[i]);
		}
	}
}

int main() {
	ifstream fin;
	fin.open("ariprog.in");
	
	fin >> N;
	fin >> M;

	// cout << N << endl << M << endl;

	for(int i = 0;i <= M;i++) {
		for(int j = i;j <= M;j++) {
			bisquares.push_back(i * i + j * j);
		}
	}

	sort(bisquares.begin(), bisquares.end());
	bisquares.erase(unique(bisquares.begin(), bisquares.end()), bisquares.end());
	
	cout << "Finished generating bisquares" << endl;
	// for(int i = 0;i < bisquares.size();i++) {
	// 	cout << bisquares[i] << " ";
	// }
	// cout << endl;

	// now we have to find all the valid combinations of N elements from
	// the list of bisquares. Note: this will be at most (M*M) Choose N
	// but we are building the generator recursively to automatically filter
	// out non-sequences while generating, even before it finishes
	// generating a set

	// for(int i = 0;i < bisquares.size()-1;i++) {
	// 	gen_combs(N-1, i+1, bisquares[i], -1, bisquares[i]);
	// }

	gen_combs(N, 0, -1, -1, -1);
	cout << "Generated combinations" << endl;
	sort(progs.begin(), progs.end());
	cout << "Sorted combinations" << endl;
	// progs.erase(unique(progs.begin(), progs.end()), progs.end());
	
	for(int i = 0;i < progs.size();i++) {
		printf("%d %d\n", progs[i].second, progs[i].first);
	}

	return 0; 
}