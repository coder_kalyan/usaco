from itertools import *
from math import *

with open("hoofball.in") as f:
    N = int(f.readline())
    dists = list(map(int, f.readline().split()))

#  print(dists, sorted(dists))
close = []
for i in range(N):
    leftd = 1000
    lefti = -1
    rightd = 1000
    righti = -1
    for j in range(N):
        if dists[j] < dists[i] and dists[i]-dists[j] < leftd:
            leftd = dists[i] - dists[j];
            lefti = j
    for j in range(N):
        if dists[j] > dists[i] and dists[j]-dists[i] < rightd:
            rightd = dists[j] - dists[i];
            righti = j
    
    if leftd <= rightd:
        close.append(lefti)
    else:
        close.append(righti)
print(" ".join(map(str, close)))

passers = [0] * N
for i in range(N):
    passers[close[i]] += 1
    #  closest = close[i]
    #  for j in range(N):
        #  if i == j:
            #  continue
        #  if close[j] == i:
            #  passers[i] += 1


print(passers)
count = 0
for i, p in enumerate(passers):
    if p == 0:
        count += 1
    if (i < close[i]) and (p == 1) and (passers[close[i]] == 1) and (close[close[i]] == i):
        # we have found a pair that just keeps passing to each other
        count += 1

# we have found an answer!
print(count)
with open("hoofball.out", "w") as f:
    f.write(str(count) + "\n")
