words = []
with open("blocks.in") as f:
    N = int(f.readline())
    for i in range(N):
        front, back = f.readline().split()
        words.append([front, back])
        #words.append(back)

def count(word):
    needed = [0] * 26
    for idx in range(26):
        needed[idx] += word.count(chr(idx + 97))

    return needed

each = [0] * 26
for word in words:
    tmp1 = count(word[0])
    tmp2 = count(word[1])
    for i in range(26):
        each[i] += max(tmp1[i], tmp2[i]) #max(each[i], c)

print("\n".join(map(str, each)))

with open("blocks.out", "w") as f:
    f.write("\n".join(map(str,each)))
    f.write("\n")
