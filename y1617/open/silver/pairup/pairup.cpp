#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

#define MAXN 100000
int N;
pair<int, int> COWS[MAXN];

int main() {
    ifstream fin("pairup.in");
    ofstream fout("pairup.out");

    fin >> N;
    for (int i = 0;i < N;i++)
        fin >> COWS[i].second >> COWS[i].first;

    sort(COWS, COWS + N);
    //for (int i = 0;i < N;i++) cout << COWS[i].first << " " << COWS[i].second << endl;
    int i = 0, j = N-1, remove, ans = 0;
    while (i < j) {
        //cout << i << " " << j << endl;
        remove = min(COWS[i].second, COWS[j].second);
        COWS[i].second -= remove;
        COWS[j].second -= remove;
        ans = max(ans, COWS[i].first + COWS[j].first);
        if (COWS[i].second == 0)
            i++;
        if (COWS[j].second == 0)
            j--;
    }

    cout << ans << endl;
    fout << ans << endl;

    return 0;
}
