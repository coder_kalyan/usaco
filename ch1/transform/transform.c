/* USACO Program
ID: kalyan2
LANG: C
TASK: transform
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Point {
	int x;
	int y;
	char value;
};

int N;

void rotate90(struct Point *a);
void rotate180(struct Point *a);
void rotate270(struct Point *a);
void reflect(struct Point *a);
void reset(struct Point *a, struct Point *b);
int cmppoint(struct Point a, struct Point b);
int cmpmap(struct Point *map1, struct Point *map2);
int exists(struct Point *map, struct Point a);

void main(void) {
	FILE *fin, *fout;
	// we are storing the grid in a flat array of points`
	char temp;
    int i,j;
    struct Point map1[100];		// first map given
	struct Point map2[100];		// second map given
	struct Point map3[100];		// temp map for checking transformations

	// open the files
	fin = fopen("transform.in","r");
	fout = fopen("transform.out", "w");


	for(i = 0;i < 100;i++) {
		map1[i].x = 0;
		map1[i].y = 0;
		map1[i].value = 0;
		map2[i].x = 0;
		map2[i].y = 0;
		map2[i].value = 0;
	}

	fscanf(fin, "%d\n", &N);

	for(j = 0;j < N;j++) {
        for(i = 0;i < N;i++) {
            fscanf(fin,"%c",&temp);
			map1[(N*j)+i].x = i+1;
			map1[(N*j)+i].y = j+1;
			map1[(N*j)+i].value = temp;
			//printf("%c",map1[(N*j)+i].value);
		}
		fscanf(fin,"\n");
	}

	for(j = 0;j < N;j++) {
        for(i = 0;i < N;i++) {
            fscanf(fin,"%c",&temp);
			map2[(N*j)+i].x = i+1;
			map2[(N*j)+i].y = j+1;
			map2[(N*j)+i].value = temp;
		}
		fscanf(fin,"\n");
	}

	/*for(j = 0;j < N;j++) {
        for(i = 0;i < N;i++) {
			printf("%c",map1[(N*j)+i].value);
		}
		printf("\n");
	}*/

	/*rotate90(map1);

	/*for(j = 0;j < N;j++) {
        for(i = 0;i < N;i++) {
			printf("%c",map1[(N*j)+i].value);
		}
		printf("\n");
	}*/

	/*for(i = 0;i < 100;i++) {
		if(map1[i].x != 0)
			printf("%d %d %c \n",map1[i].x, map1[i].y, map1[i].value);
	}

	puts("");

	for(i = 0;i < 100;i++) {
		if(map2[i].x != 0)
			printf("%d %d %c \n",map2[i].x, map2[i].y, map2[i].value);
	}

	i = cmpmap(map1,map2);
	printf("%d",i);*/
    //printf("%d %d\n",longestMilk,longestSleep);
    //fprintf(fout,"%d %d\n",longestMilk,longestSleep);
	//printf("%d",i);

	// 1 - rotate90

	reset(map3,map1);
	rotate90(map3);
	if(cmpmap(map3,map2) == 1) {
		fputs("1\n",fout);
		fclose(fin);
		fclose(fout);
		exit(0);
	}

	reset(map3,map1);
	rotate180(map3);
	if(cmpmap(map3,map2) == 1) {
		fputs("2\n",fout);
		fclose(fin);
		fclose(fout);
		exit(0);
	}

	reset(map3,map1);
	rotate270(map3);
	if(cmpmap(map3,map2) == 1) {
		fputs("3\n",fout);
		fclose(fin);
		fclose(fout);
		exit(0);
	}

	reset(map3,map1);
	reflect(map3);
	if(cmpmap(map3,map2) == 1) {
		fputs("4\n",fout);
		fclose(fin);
		fclose(fout);
		exit(0);
	}

	reset(map3,map1);
	reflect(map3);
	rotate90(map3);
	if(cmpmap(map3,map2) == 1) {
		fputs("5\n",fout);
		fclose(fin);
		fclose(fout);
		exit(0);
	}

	reset(map3,map1);
	reflect(map3);
	rotate180(map3);
	if(cmpmap(map3,map2) == 1) {
		fputs("5\n",fout);
		fclose(fin);
		fclose(fout);
		exit(0);
	}

	reset(map3,map1);
	reflect(map3);
	rotate270(map3);
	if(cmpmap(map3,map2) == 1) {
		fputs("5\n",fout);
		fclose(fin);
		fclose(fout);
		exit(0);
	}

	if(cmpmap(map1,map2) == 1) {
		fputs("6\n",fout);
		fclose(fin);
		fclose(fout);
		exit(0);
	}

	fputs("7\n",fout);

	fclose(fin);
	fclose(fout);

	exit(0);
}

void reset(struct Point *a, struct Point *b) {
	int i;
	for(i = 0;i < 100;i++)
		a[i] = b[i];
}

void rotate90(struct Point *a) {
	// (x,y) -> (N - y, x)
	int i,temp;
	for(i = 0;i < 100;i++) {
		if((a[i].x == 0) || (a[i].y == 0) || (a[i].value == 0))
			continue;

		temp = a[i].x;
		a[i].x = N + 1 - a[i].y;
		a[i].y = temp;
	}
}

void rotate180(struct Point *a) {
	// (x,y) -> (N - x, y)
	int i;
	for(i = 0;i < 100;i++) {
		if((a[i].x == 0) || (a[i].y == 0) || (a[i].value == 0))
			continue;

		a[i].x = N + 1 - a[i].x;
		a[i].y = N + 1 - a[i].y;
	}
}

void rotate270(struct Point *a) {
	// (x,y) -> (y, N - x)
	int i,temp;
	for(i = 0;i < 100;i++) {
		if((a[i].x == 0) || (a[i].y == 0) || (a[i].value == 0))
			continue;

		temp = a[i].x;
		a[i].x = a[i].y;
		a[i].y = N + 1 - temp;
	}
}

void reflect(struct Point *a) {
	// (x,y) -> (N - x, y)
	int i;
	for(i = 0;i < 100;i++) {
		if((a[i].x == 0) || (a[i].y == 0) || (a[i].value == 0))
			continue;

		a[i].x = N + 1 - a[i].x;
	}
}

int cmppoint(struct Point a, struct Point b) {
	if((a.x == b.x) && (a.y == b.y) && (a.value == b.value)) {
		return 1;
	} else {
		return 0;
	}
}

int exists(struct Point *map, struct Point a) {
	int i;
	i = 0;
	while(i < 100) {
		if(cmppoint(map[i],a) == 1) {
			return 1;
		}
		i++;
	}
	return 0;
}

int cmpmap(struct Point *map1, struct Point *map2) {
	// compares to of the maps created above
	// for each point in the first map, check if there is an equivalent
	// node in the second map
	int i;
	for(i = 0;i < 100;i++) {
		if(map1[i].value == 0)
			continue;
		if(exists(map2,map1[i]) == 0) {
			return 0;
		}
	}
	return 1;
}
