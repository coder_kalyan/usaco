#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;

const string PROG = "lifeguards";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N;
struct Entry {
    long i;
    bool isStart;
    long t;
};
vector<struct Entry> guards;

bool cmpEntry(struct Entry a, struct Entry b) {return a.t<b.t;}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N;
    // ii tmp_pair;
    struct Entry tmp_entry;
    guards.resize(N);
    REP(i,0,N) {
        tmp_entry.i = i;
        tmp_entry.isStart = true;
        fin >> tmp_entry.t;
        guards.push_back(tmp_entry);
        tmp_entry.isStart = false;
        fin >> tmp_entry.t;
        guards.push_back(tmp_entry);
        // tmp_pair.first = i;
        // fin >> tmp_pair.second;
        // clog << tmp_pair.first << " " << tmp_pair.second << endl;
        // start.push_back(tmp_pair);
        // fin >> tmp_pair.second;
        // end.push_back(tmp_pair);
    }

    sort(guards.begin(),guards.end(),cmpEntry);
    // vi cur;
    long tot, solo[N], prev = 0;
    set<long> queue;
    for (vector<struct Entry>::iterator it=guards.begin();it!=guards.end();it++) {
        if(queue.size() == 1)
            solo[it->i] += it->t - prev;
        if(queue.size() > 0)
            tot += it->t - prev;
        if (it->isStart) {
            queue.insert(it->i);
        } else {
            queue.erase(it->i);
        }

        prev = it->t;
    }

    long minSolo = min(solo,solo+N);
    cout << tot-minSolo << endl;
	return 0;
}
