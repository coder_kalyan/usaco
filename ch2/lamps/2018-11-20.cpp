/*
  ID: kalyan2
  LANG:C++11
  TASK: lamps
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef bitset<100> bs;

const string PROG = "lamps";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

short N, C;

vi on, off;

set<string> ans;

bool works(bs comb) {
	vi::iterator it;
    // cout << "Checking: " << comb << endl;
	for (it = on.begin();it != on.end();it++)
		if (comb[*it - 1] == 0)
			return false;
	
	for (it = off.begin();it != off.end();it++)
		if (comb[*it - 1] == 1)
			return false;

    // cout << "Works" << endl;

	return true;
}

bs press(int btn, bs state) {
	switch (btn) {
		case 1:
			return state.flip();
        case 2:
			for (int i = 0;i < N;i+=2)
				state.flip(i);
			return state;
		case 3:
			for (int i = 1;i < N;i+=2)
				state.flip(i);
			return state;
		case 4:
			for (int i = 0;i < N;i+=3)
				state.flip(i);
			return state;
		default:
			return state;
	}
}

void sim(int btn, bs state, string comb, int numPress) {
    // for (int i = 0;i < N;i++) {
        // cout << state[i];
    // }
    // cout << endl;
	if ((btn > 4)) {
        // if (numPress % 2 == C % 2) {
            // string tmp;
            // for (int i = 0;i < N;i++) {
                // tmp += to_string(state[i]);
                // //cout << state[i];
                // //fout << state[i];
            // }
            // ans.insert(tmp);
        // } else
            // cout << numPress << " " << C << endl;
		if ((numPress % 2 == C % 2) && works(state)) {
            string tmp;
            for (int i = 0;i < N;i++) {
                tmp += to_string(state[i]);
                //cout << state[i];
                //fout << state[i];
            }
            ans.insert(tmp);
            // cout << endl;
            // fout << endl;
		}
			
		return;
	}
	
	// either we can press the button or not
	sim(btn + 1, state, comb + "0", numPress);
	sim(btn + 1, press(btn, state), comb + to_string(btn), numPress + 1);
}

int main() {
	ifstream fin (FIN);
	int tmp;
	fin >> N >> C;
	while (fin >> tmp) {
		if (tmp != -1)
			on.push_back(tmp);
		else
			break;
	}
	while (fin >> tmp) {
		if (tmp != -1)
			off.push_back(tmp);
		else
			break;
	}
	//cout << "On: ";
	//for (vi::iterator it = on.begin();it != on.end();it++)
	//	cout << *it << " ";
	//cout << endl;
	//cout << "Off: ";
	//for (vi::iterator it = off.begin();it != off.end();it++)
	//	cout << *it << " ";
	//cout << endl;

	bitset<100> start;
	start.set();
    // start = press(4, start);
    // cout << start << endl;
	if (C > 0)
        sim(1, start, "", 0);
    else if (works(start)) {
        string tmp;
        for (int i = 0;i < N;i++)
            tmp += "1";
        ans.insert(tmp);
    }
	ofstream fout (FOUT);
    for (set<string>::iterator it = ans.begin();it != ans.end();it++) {
        cout << *it << endl;
        fout << *it << endl;
    }

    if (ans.size() == 0) {
        cout << "IMPOSSIBLE" << endl;
        fout << "IMPOSSIBLE" << endl;
    }
	

	return 0;
}
