#include <iostream>
#include <fstream>

using namespace std;

#define MAXN 100
int N, K, R, graph[MAXN*MAXN][MAXN*MAXN] = {}, COWS[MAXN], V;
#define RC2I(R,C) R*N + C
int main() {
    ifstream fin("countcross.in");
    ofstream fout("countcross.out");

    fin >> N >> K >> R;
    int r, c, rp, cp;
    for (int i = 0;i < R;i++) {
        fin >> r >> c >> rp >> cp;
        r--; c--; rp--; cp--;
        cout << RC2I(r,c) << " " << RC2I(rp,cp) << endl;
        graph[RC2I(r,c)][RC2I(rp,cp)] = 1;
        graph[RC2I(rp,cp)][RC2I(r,c)] = 1;
    }
    V = N*N;
    for (int k = 0;k < V;k++)
        for (int i = 0;i < V;i++)
            for (int j = 0;j < V;j++)
                if (graph[i][k] + graph[k][j] < graph[i][j])
                    graph[i][j] = graph[i][k] + graph[k][j];
    for (int i = 0;i < V;i++) {
        for (int j = 0;j < V;j++)
            cout << graph[i][j] << " ";
        cout << endl;
    }
    int ans = 0;
    for (int i = 0;i < V;i++)
        for (int j = 0;j < V;j++)
            if (i != j && graph[i][j] > 0)
                ans++;
    ans /= 2;
    cout << ans << endl;

    return 0;
}
