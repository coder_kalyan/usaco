with open("measurement.in") as f:
    N, G = map(int, f.readline().split())
    log = []
    for i in range(N):
        log.append(tuple(map(int, f.readline().split())))

    log.sort()
    # print(log)

cows = dict()
leader = -1

ei = 0
ans = 0
while ei < N:
    first = log[ei][0]
    while ei < N and log[ei][0] == first:
        cowi = log[ei][1]
        change = log[ei][2]
        if cowi not in cows:
            cows[cowi] = G
        cows[cowi] += change
        ei += 1
    if len(cows) > 0:
        newleader = 0
        newgal = 0
        for i, g in cows.items():
            if g > newgal:
                g = newgal
                newleader = i
        if newleader != leader:
            leader = newleader
            ans += 1

print(ans)
with open("measurement.out", "w") as f:
    f.write(str(ans))
    f.write("\n")
