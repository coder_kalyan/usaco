"""
ID: kalyan2
LANG: PYTHON3
TASK: cowtour
"""

from math import *

locs = []
mat = []

with open("cowtour.in") as f:
    N = int(f.readline())
    for i in range(N):
        x, y = map(int,f.readline().split())
        locs.append((x, y))
    for i in range(N):
        mat.append(list(map(int,list(f.readline().rstrip()))))

def dist(a,b):
    d1 = abs(locs[a][0] - locs[b][0])
    d2 = abs(locs[a][1] - locs[b][1])
    return sqrt((d1**2)+(d2**2))

for j in range(N):
    for i in range(N):
        if mat[j][i]:
            mat[j][i]=dist(i,j)

for loc in locs:
    print(loc)
for m in mat:
    print(m)

