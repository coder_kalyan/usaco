"""
ID: kalyan2
LANG: PYTHON3
TASK: fracdec
"""

with open("fracdec.in") as f:
    a, b = map(int, f.readline().split(" "))

res = ""
d = dict()
rem = a % b
while (rem != 0) and rem not in d:
    d[rem] = len(res)
    rem *= 10
    res_part = rem // b
    res += str(res_part)
    rem %= b

if rem == 0:
    ans = '%f' % (float(a/b))
else:
    ans = str(a//b) + "." + res[:d[rem]] + "(" + res[d[rem]:] + ")"

ans = ans.rstrip("0")
if ans[-1] == ".":
    ans += "0"
ans = "\n".join([ans[i:i+76] for i in range(0, len(ans), 76)])
print(ans)
with open("fracdec.out", "w") as f:
    f.write(ans)
    f.write("\n")
