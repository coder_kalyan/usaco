/*
  ID: kalyan2
  LANG:C++11
  TASK: maze1
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;

#define MAXW 38
#define MAXH 100

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;

#define INF 1000000000

const string PROG = "maze1";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

int W, H;
char field[2*MAXH+10][2*MAXW+10];
int graph[MAXH][MAXW];

struct Node {
    int id;
    int x, y;
    int dist = INF;
    bool visited = false;
    int parent = -1;
};

class Compare {
    bool operator() (const Node& a, const Node& b) const {
        return a.dist < b.dist;
    }
};

int dist1[MAXH][MAXW];
int dist2[MAXH][MAXW];
bool visited[MAXH][MAXW];
int parent[MAXH][MAXW];
vii exits;

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> W >> H;

    char tmp;
    fin >> noskipws;
    fin >> tmp; // throw away newline
    REP(j,0,2*H+1) {
        REP(i,0,2*W+1)
            fin >> field[j][i];
        fin >> tmp; // throw away newline
    }
    REP(j,0,2*H+1) {
        REP(i,0,2*W+1)
            cout << field[j][i];
        cout << endl;
    }

    // convert to implicit graph
    // UP: 8, RIGHT: 4, DOWN: 2, LEFT: 1
    REP(j,0,H) {
        REP(i, 0, W) {
            graph[j][i] = 0;
            if (field[2*j][2*i+1]==' ')
                graph[j][i] |= 8;
            if (field[2*j+2][2*i+1]==' ')
                graph[j][i] |= 2;
            if (field[2*j+1][2*i]==' ')
                graph[j][i] |= 1;
            if (field[2*j+1][2*i+2]==' ')
                graph[j][i] |= 4;
        }
    }
    REP(j,0,H) {
        REP(i, 0, W)
            cout << graph[j][i] << " ";
        cout << endl;
    }


    REP(i,0,W)
        if (graph[0][i] & 8)
            exits.push_back(make_pair(i,0));
    REP(i,0,W)
        if (graph[H-1][i] & 2)
            exits.push_back(make_pair(i,H-1));
    REP(i,0,H)
        if (graph[i][0] & 1)
            exits.push_back(make_pair(0,i));
    REP(i,0,H)
        if (graph[i][W-1] & 4)
            exits.push_back(make_pair(W-1,i));

    for(auto exit : exits)
        cout << exit.first << " " << exit.second << endl;
    
    REP(j,0,H)
        REP(i,0,W) {
            dist1[j][i] = -1;
            dist2[j][i] = -1;
            visited[j][i] = true;
            parent[j][i] = -1;
        }
    ii ex = exits[0];
    priority_queue<Node, vector<Node>,Compare> nodes;
    int ind = 1;
    REP(j,0,H) {
        REP(i,0,W) {
            if (j == ex.second && i == ex.first)
                continue;
            // cout << i << " " << j << endl;
            Node n;
            n.x = i;
            n.y = j;
            n.id = ind;
            nodes.push(n);
            ind++;
        }
    }
    Node n;
    n.x = ex.first;
    n.y = ex.second;
    n.id = 0;
    n.dist = 0;
    nodes.push(n);
    while (!nodes.empty)

	return 0;
}
