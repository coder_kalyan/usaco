#include <iostream>
#include <algorithm>

using namespace std;

int main() {
    int T;
    cin >> T;
    int l[3];
    for (int i = 0;i < T;i++) {
        cin >> l[0] >> l[1] >> l[2];
        sort(l,l+3);
        cout << "Case " << (i+1) << ": " << l[1] << endl;
    }
}
