#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>

using namespace std;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;

const string PROG = "convention2";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

struct Cow {
    int a, t, i;
};

typedef struct Cow Cow;

vector<Cow> cows, queue;
long N;

bool cmp(Cow a, Cow b) { return (a.i < b.i); }

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N;
    while(N--) {
        Cow cow;
        fin >> cow.a >> cow.t >> cow.i;
        cows.push_back(cow);
    }

    clog << "Read values" << endl;
    sort(cows.begin(),cows.end(),cmp);
    clog << "Sorted" << endl;
    clog << cows.size() << " " << cows[0].a << endl;
    long time_idx = cows[0].a;
    Cow cur = cows[0];
    long cur_start = time_idx;
    long cow_idx = 1;
    long max_wait = 0;
    clog << "Sorted, initialize" << endl;

    while (cow_idx < N) {
        // jump to the end of the current cow's eating
        time_idx += cur.t;
        // check if anyone joined the queue during that time
        while ((cow_idx<N) && (cows[cow_idx].a<=time_idx)) {
            if (queue.size() > 0) {
                long i = 0;
                while ((i<queue.size()) && (cows[cow_idx].i > queue[i].i))
                    i++;
                queue.insert(queue.begin()+i,cows[cow_idx]);
            } else {
                queue.push_back(cows[cow_idx]);
            }
            cow_idx++;
        }

        if (queue.size()>0) {
            // if the queue has cows in it, put the highest priority one in the pasture
            cur = queue[0];
            queue.erase(queue.begin());
            max_wait = max(max_wait,time_idx-cur.a);
            cout << cur.a << endl;
        } else {
            // queue is empty, so jump to the first cow and put her in the pasture
            cur = cows[cow_idx];
            cow_idx++;
            time_idx = cur.a;
        }
    }

    cout << max_wait << endl;
    fout << max_wait << endl;

	return 0;
}
