#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<ll, ll> ii;

#define MAXN 100000
ll N, M, R;
ll C[MAXN], B[MAXN], rent[MAXN], sell[MAXN], maxr;
ii S[MAXN];

int main() {
    ifstream fin("rental.in");
    ofstream fout("rental.out");

    fin >> N >> M >> R;
    for (ll i = 0;i < N;i++)
        fin >> C[i];
    for (ll i = 0;i < M;i++)
        fin >> S[i].second >> S[i].first;
    for (ll i = 0;i < R;i++)
        fin >> B[i];

    sort(C, C + N, greater<ll>());
    //for (ll i = 0;i < N;i++)
        //cout << C[i] << " ";
    //cout << endl;
    sort(S, S + M, greater<ii>());
    ll si = 0, ci = 0, pour, cash = 0;
    maxr = N;
    while (true) {
        pour = min(S[si].second, C[ci]);
        C[ci] -= pour;
        S[si].second -= pour;
        cash += S[si].first * pour;
        if (C[ci] == 0) {
            sell[ci] = cash;
            ci++;
        }
        if (S[si].second == 0)
            si++;
        if (ci >= N)
            break;
        if (si >= M) {
            maxr = ci;
            break;
        }
    }
    //for (ll i = 0;i < M;i++)
        //cout << "(" << S[i].first << ", " << S[i].second << ") ";
    //cout << endl;
    sort(B, B + R, greater<ll>());
    rent[0] = B[0];
    for (ll i = 1;i < R;i++)
        rent[i] = rent[i-1] + B[i];
    ll ans = 0;
    //cout << (N - R) << " " << maxr << endl;
    //for (ll i = max(0LL, N - R); i < R; i++) {
    //for (ll i = ; i < R; i++) {
    for (ll i = 0;i < N;i++) {
        //cout << sell[i] << " " << rent[N - 2 - i] << endl;
        //cout << (sell[i] + rent[N - i]) << " ";
        //cout << (sell[i]) << " ";
        //cout << sell[i] << endl;
        //cout << (rent[N - 1 - i]) << endl; //" ";
        ans = max(ans, sell[i] + rent[N - 2 - i]);
    }
    //cout << endl;
    cout << ans << endl;
    fout << ans << endl;

    return 0;
}
