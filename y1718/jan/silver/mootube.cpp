#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)

#define MAXQ 5000
#define MAXK 1000000000

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;

const string PROG = "mootube";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N, Q;
long orig[MAXQ][MAXQ];
long graph[MAXQ][MAXQ];

void solve(long start, long cur, long prev, long minWeight) {
    REP(v, 0, N) {
        if (orig[cur][v] == 0)
            continue;
        if (v == prev)
            continue;
        // clog << v << endl;

        // there is a connection(non-zero weight)
        // from cur to the cell we are visiting
        // aka v is a neighbor
        graph[start][v] = min(minWeight, orig[cur][v]);

        solve(start, v, cur, graph[start][v]);
    }
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N >> Q;
    long pi, qi, ri;
    REP(i,0,N-1) {
        fin >> pi >> qi >> ri;
        orig[pi][qi] = ri;
        graph[pi][qi] = ri;
    }

    long ki, vi;
    fin >> ki >> vi;
    long ans = 0;
    solve(vi, vi, 0, MAXK+1);
    clog << N << endl;
    REP(v, 0, N) {
        clog << v << " " << graph[vi][v] << endl;
        if (graph[vi][v] >= ki)
            ans++;
    }

    cout << ans << endl;
	return 0;
}
