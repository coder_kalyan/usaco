#include <iostream>
#include <fstream>
#include <algorithm>
#include <map>

int N;
map<int,int> deltalist;

int main() {
    ifstream fin("teleport.in");
    ofstream fout("teleport.out");

    int a, b, curf = 0, slopef = 0, cury = -2000000000;
    fin >> N;
    for (int i = 0;i < N;i++) {
        fin >> a >> b;
        curf += abs(a-b);
