#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

#define MAXN 100000

long N, Q, A, B;
long BALES[MAXN];

int main() {
    ifstream fin("haybales.in");
    ofstream fout("haybales.out");

    fin >> N >> Q;
    for (long i = 0;i < N;i++)
        fin >> BALES[i];
    sort(BALES, BALES + N);
    
    for (long i = 0;i < Q;i++) {
        fin >> A >> B;
        fout << distance(lower_bound(BALES,BALES+N,A),upper_bound(BALES,BALES+N,B)) << endl;
        //auto ai = lower_bound(BALES, BALES + N, A);
        //auto bi = upper_bound(BALES, BALES + N, B);
        ////cout << *ai << " " << distance(BALES, ai) << endl;
        ////cout << *bi << " " << distance(BALES, bi) << endl;
        //cout << distance(ai, bi) << endl;
        //fout << distance(ai, bi) << endl;
    }

    return 1;
}
