"""
ID: kalyan2
TASK: numtri
LANG: PYTHON3
"""

# version 1 - recursion: NOT SCALABLE

largest = 0
tri = []

def sim_path(step, col, total):
	global largest

	if step == R:
		largest = max(largest, total)
		return

	elem = tri[step][col]
	sim_path(step + 1, col, total + elem)
	sim_path(step + 1, col + 1, total + elem)


fin = open("numtri.in", "r")
fout = open("numtri.out", "w")


R = int(fin.readline())

for i in range(R):
	line = fin.readline()
	tri.append(list(map(int, line.split(" "))))

sim_path(0, 0, 0)

print(largest)
fout.write(str(largest) + "\n")
