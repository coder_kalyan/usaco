"""
ID: kalyan2
LANG: PYTHON3
TASK: subset
"""

with open("subset.in", "r") as f:
    N = int(f.readline())

# since the objective is to divide
# the set into 2 subsets of equal sum,
# we have to make sure the sum of the
# whole set is even
# otherwise, there will be no answers

sig = N * (N + 1) // 2
halfsig = sig // 2

print(sig, halfsig)

if sig % 2 != 0:
    print(0)
    with open("subset.out", "w") as f:
        f.write("0\n")

    exit(0)

nums = list(range(1, N + 1))
count = 0

def solve(first, left):
    global count

    print(first, left)

    s = sum(first)
    if s == halfsig:
        # we have found an answer!!!
        count += 1
        return

    for elem in sorted(filter(lambda x: x <= (halfsig - s), left), reverse=True):
        #if elem < (halfsig - s) / 2:
        #    break

        ind = left.index(elem)
        left.remove(elem)
        solve(first + [elem], left)
        left.insert(ind, elem)

def solve2(first, left):
    global count

#    print(first, left)

    # s = sum(first)
    # s2 = sum(left)
    if first == halfsig:
        # we have found an answer!!!
        count += 1
        print(count)
        return

    elems = sorted(filter(lambda x: x <= (halfsig - first), left), reverse=True)
    for elem in elems:
        #if elem < (halfsig - s) / 2:
        #    break

        ind = left.index(elem)
        left.remove(elem)
        solve2(first + elem, left)
        left.insert(ind, elem)

# solve([], nums)
solve2(0, nums)

print(count)
with open("subset.out", "w") as f:
    f.write(str(count) + "\n")

