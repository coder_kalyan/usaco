with open("tttt.in", "r") as f:
    a, b, c, _ = f.readline()
    c, d, e, _ = f.readline()
    g, h, i, _ = f.readline()

def eq(_a, _b, _c, is_indiv):
    if is_indiv:
        return (_a == _b) and (_b == _c)
    else:
        return len({_a, _b, _c}) == 2  # prob more efficient way than this 

# first lets count the individual
indiv = 0

if eq(a, b, c, True):
    indiv += 1
if eq(d, e, f, True):
    indiv += 1
if eq(g, h, i, True):
    indiv += 1
if eq(a, d, g, True):
    indiv += 1
if eq(b, e, h, True):
    indiv += 1
if eq(c, f, i, True):
    indiv += 1
if eq(a, e, i, True):
    indiv += 1
if eq(c, e, g, True):
    indiv += 1


# now the groups 
team = 0

if eq(a, b, c, False):
    team += 1
    print(a, b, c)
if eq(d, e, f, False):
    team += 1
    print(c, d, e)
if eq(g, h, i, False):
    team += 1
    print(g, h, i)
if eq(a, d, g, False):
    team += 1
    print(a, c, g)
if eq(b, e, h, False):
    team += 1
    print(b, d, h)
if eq(c, f, i, False):
    team += 1
    print(c, e, i)
if eq(a, e, i, False):
    team += 1
    print(a, d, i)
if eq(c, e, g, False):
    team += 1
    print(c, d, g)

print(indiv, team)

with open("tttt.out", "w") as f:
    f.write(str(indiv) + "\n" + str(team) + "\n")
