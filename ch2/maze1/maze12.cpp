/*
  ID: kalyan2
  LANG:C++11
  TASK: maze1
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>
#include <climits>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define RREP(i,a,b) for (long i=b-1;i >= a;i--)
#define LOG(a,b) clog << a << ": " << b << endl;
#define MP(a,b) make_pair(a,b)

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;
typedef priority_queue<ii> pqiif;
typedef priority_queue<ii> pqiis;
typedef vector<vi> vvi;
typedef vvi::iterator vvi_i;
typedef vector<vii> vvii;
typedef vvii::iterator vvii_i;

#define INF INT_MAX

#define MAXW 38
#define MAXH 100
#define NODES MAXW*MAXH + 10

const string PROG = "maze1";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

int dist1[NODES], dist2[NODES];
bool vis[NODES];
vii adj[NODES];

class cmp {
    bool operator() (const ii& a, const ii& b) {
        return a.second > b.second;
    }
};

void dijkstra(int source, int n, int* dist) {
    fill(dist,dist+NODES,INF);
    fill(vis,vis+NODES,false);
    dist[source] = 0;
    vis[source] = true;

    pqiis pq;
    pq.push(MP(source,0));
    while(!pq.empty()) {
        ii curr = pq.top();
        pq.pop();
        int cvert = curr.first;
        int cweight = curr.second;
        if (vis[cvert])
            continue;
        vis[cvert] = true;
        for (auto vert : adj[cvert])
            if (!vis[vert.first] && vert.second+cweight<dist[vert.first])
                pq.push(MP(vert.first,(dist[vert.first]=vert.second+cweight)));
    }
}

int W, H;
char field[2*MAXH+10][2*MAXW+10];

int ind(int w, int h) {
    return (h*W)+w+1;
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> W >> H;
    char tmp;
    fin >> noskipws;
    fin >> tmp; // throw away newline
    REP(j,0,2*H+1) {
        REP(i,0,2*W+1)
            fin >> field[j][i];
        fin >> tmp; // throw away newline
    }

    REP(j,0,H) {
        // cout << j << endl;
        REP(i,0,W) {
            if (field[2*j][2*i+1]==' ')
                adj[ind(i,j)].push_back(MP(ind(i,j-1),1));
            if (field[2*j+2][2*i+1]==' ')
                adj[ind(i,j)].push_back(MP(ind(i,j+1),1));
            if (field[2*j+1][2*i]==' ')
                adj[ind(i,j)].push_back(MP(ind(i-1,j),1));
            if (field[2*j+1][2*i+2]==' ')
                adj[ind(i,j)].push_back(MP(ind(i+1,j),1));
        }
    }

    REP(i,1,H+1) {
        cout << i
        for(auto a : adj[i])
            cout << a.first << ", " << a.second << "| ";
        cout << endl;
    }
	return 0;
}
