#include <bits/stdc++.h>

using namespace std;

typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef priority_queue<int> pqi;

int N;
vii events;
set<int> s;
int main() {
    ifstream fin("lifeguards.in");
    ofstream fout("lifeguards.out");

    fin >> N;
    events.resize(N * 2);
    for (int i = 0;i < N;i++) {
        fin >> events[i * 2].first;
        events[i * 2].second = i;
        fin >> events[i * 2 + 1].first;
        events[i * 2 + 1].second = i;
    }
    sort(events.begin(), events.end());
    for (int i = 0;i < N * 2;i++)
        cout << events[i].first << " " << events[i].second << endl;

    cout << "Got here" << endl;
    int cover = 0;
    int alone[100000];
    int last = 0;

    for (auto event: events) {
        cout << event.first << " " << event.second << endl;
        if (s.size() == 1)
            alone[*(s.begin())] += event.first - last;
        if (!s.empty())
            cover += event.first - last;
        cout << "Got here" << endl;
        if (s.find(event.second) != s.end()) 
            s.erase(s.find(event.second));
        else
            s.insert(event.second);
    }

    int ans = 0;
    for (int i = 0;i < N;i++)
        ans = max(ans, cover - alone[i]);
    cout << ans << endl;

    return 1;
}
