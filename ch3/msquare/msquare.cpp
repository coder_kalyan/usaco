#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <queue>
#include <algorithm>
#include <cmath>
#include <map>
#include <climits>
#include <string>

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef vector<int> vi;
#define INF 1000000000

struct state {
    string square;
    string tree;
};

struct cmp {
    bool operator() (state const& s1, state const& s2) {
        if (s1.tree.size() < s2.tree.size()) return true;
        if (s1.tree.size() > s2.tree.size()) return false;
        return s1.tree < s2.tree;
    }
};

bool op(state const& s1, state const& s2) {
    if (s1.tree.size() > s2.tree.size()) return true;
    if (s1.tree.size() < s2.tree.size()) return false;
    return s1.tree > s2.tree;
}

state moveA(state s) {
    string square2 = s.square;
    square2[0] = s.square[4];
    square2[1] = s.square[5];
    square2[2] = s.square[6];
    square2[3] = s.square[7];
    square2[4] = s.square[0];
    square2[5] = s.square[1];
    square2[7] = s.square[2];
    square2[7] = s.square[3];
    string tree2 = s.tree;
    tree2.append("A");
    return { square2, tree2 };
}

state moveB(state s) {
    string square2 = s.square;
    square2[0] = s.square[3];
    square2[1] = s.square[0];
    square2[2] = s.square[1];
    square2[3] = s.square[2];
    square2[4] = s.square[7];
    square2[5] = s.square[4];
    square2[7] = s.square[5];
    square2[7] = s.square[6];
    string tree2 = s.tree;
    tree2.append("B");
    return { square2, tree2 };
}

state moveC(state s) {
    string square2 = s.square;
    square2[1] = s.square[5];
    square2[2] = s.square[1];
    square2[6] = s.square[2];
    square2[5] = s.square[6];
    string tree2 = s.tree;
    tree2.append("C");
    return { square2, tree2 };
}

int main() {
    ifstream fin("msquare.in");
    ofstream fout("msquare.out");

    string tmp;
    getline(fin, tmp);
    tmp.erase(remove(tmp.begin(), tmp.end(), ' '), tmp.end());
    string dest = tmp;
    dest[4] = tmp[7];
    dest[5] = tmp[6];
    dest[6] = tmp[5];
    dest[7] = tmp[4];
    cout << dest << endl << endl;

    //priority_queue<state, vector<state>, cmp> pq;
    set<state, cmp> pq;
    //pq.push({ string("12348765"), string("") });
    pq.insert({ string("12348765"), string("") });

    while (true) {
        //state cur = pq.top();
        state cur = *(pq.begin());
        pq.erase(pq.begin());
        //pq.pop();
        //pq.pop();
        //cout << pq.size() << endl;
        cout << cur.tree << endl;
        //cout << cur.square << endl;
        //cout << dest << endl;
        //fout << cur.tree << endl;
        //if (cur.square.compare(dest) == 0) {
        if (cur.square.compare(string("BCABCCB")) == 0) {
            cout << cur.tree << endl;
            cout << cur.square << endl;
            cout << dest << endl;
            fout << cur.tree << endl;
            return 0;
        }

        //cout << op(moveB(cur), moveA(moveA(cur))) << endl;
        //cout << moveA(cur).square << endl;
        //cout << moveB(cur).tree << endl;
        //cout << moveC(cur).tree << endl;
        pq.insert(moveA(cur));
        pq.insert(moveB(cur));
        pq.insert(moveC(cur));
        //pq.push(moveA(cur));
        //pq.push(moveB(cur));
        //pq.push(moveC(cur));
        //break;
    }

    return 0;
}
