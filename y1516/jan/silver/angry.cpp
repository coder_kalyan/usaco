#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;

const string PROG = "angry";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N, K;
vi bales, m, b;
queue<long> q, eQ;

long maxelem() {
    long mI = 0, mV = m[0];
    REP(i,0,N)
        if (m[i] > mV) {
            mI = i;
            mV = m[i];
        }

    return mI;
}
    
bool sim(long r) {
    swap(q,eQ);
    b.clear();
    b.resize(N);
    // copy(bales.begin(), bales.end(), back_inserter(b));
    cout << bales.size() << endl;
    REP(i, 0, N)
        b[i] = bales[i];
    

    REP(i,0,N) cout << b[i] << " ";
    cout << endl;

    REP(i,0,N) {
        while (!q.empty() && bales[i] - q.front() < r)
            q.pop();
        q.push(bales[i]);
        m[i] = q.size();
    }
    for (long i=N-1;i>=0;i--) {
        while (!q.empty() && q.front() - bales[i] < r)
            q.pop();
        q.push(bales[i]);
        m[i] += q.size();
    }
    REP(i,0,K) {
        // find the densest place to hit
        long cPos = maxelem();
        // and hit it
        REP(j,cPos-r,cPos+r)
            b[j] = 0;
        m[cPos] = 0;
    }
    // return all_of(b.cbegin(),b.cend(),[](int i){ return i==0;});
    for (auto it : b) {
        if (it != 0)
            return false;
    }

    return true;
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N >> K;
    bales.resize(N);
    m.resize(N);
    REP(i,0,N)
        fin >> bales[i];
    
    long lowK = 1, highK = N, testK, ans;
    bool y;
    while ((lowK+1) < highK) {
        testK = (highK + lowK) / 2;
        y = sim(testK);
        cout << (y ? "Yes" : "No") << endl;
        if (y) {
            highK = testK;
        } else {
            lowK = testK;
        }
    }
    cout << "Done" << endl;
    if (y)
        ans = lowK;
    else
        ans = highK;

    cout << ans << endl;
    fout << ans << endl;

	return 0;
}
