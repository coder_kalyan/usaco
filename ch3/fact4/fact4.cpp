/*
 ID: kalyan2
 LANG: C++
 TASK: fact4
 */

#include <iostream>
#include <fstream>

using namespace std;

long N, ans;
int main() {
    ifstream fin("fact4.in");
    ofstream fout("fact4.out");
    
    fin >> N;
    ans = 1;
    for (int i = 2;i <=N;i++) {
        ans  = ans * i % 100000;
        while (ans % 10 == 0)
            ans /= 10;
    }
    ans %= 10;
    cout << ans << endl;
    fout << ans << endl;
}
