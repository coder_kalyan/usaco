/*
ID: kalyan2
TASK: ariprog
LANG: C++
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;

short N, M;
int bisquares[63002];
vii progs;
// vector<bool> mask;
int myind = 0;

// A recursive binary search function. It returns 
// location of x in given array arr[l..r] is present, 
// otherwise -1
int mybinarySearch(int arr[], int l, int r, int x)
{
   if (r >= l)
   {
        int mid = l + (r - l)/2;
 
        // If the element is present at the middle 
        // itself
        if (arr[mid] == x)  
            return mid;
 
        // If element is smaller than mid, then 
        // it can only be present in left subarray
        if (arr[mid] > x) 
            return mybinarySearch(arr, l, mid-1, x);
 
        // Else the element can only be present
        // in right subarray
        return mybinarySearch(arr, mid+1, r, x);
   }
   // We reach here when element is not 
   // present in array
   return -1;
}


int mybinarySearch2(int arr[], int l, int r, int x)
{
	while (r >= l){
        int mid = l + (r - l)/2; 
        // If the element is present at the middle 
        // itself
        if (arr[mid] == x)  
            return mid;
        if (arr[mid] > x)
        	r = mid - 1;
        else
        	l = mid+1;
	}
   // We reach here when element is not 
   // present in array
   return -1;
}



int main() {
	ifstream fin;
	fin.open("ariprog.in");
	
	fin >> N;
	fin >> M;

	// int myind = 0;
	for(int i = M;i >=0;i--) {
		for(int j = i;j >=0;j--) {
			int mytemp = (i*i) + (j*j);
			// add to array only if not duplicate
			int inarray = 0;
			for (int k =0; k < myind; k++) {
				if (bisquares[k] == mytemp) {
					inarray = 1;
					break;
				}
			}
			if (!inarray)
				bisquares[myind++] = mytemp;				
		}
	}

	sort(&bisquares[0], &bisquares[myind]);
	
	cout << "Finished generating bisquares" << endl;

	// for(int i = 0;i < myind;i++) {
	// 	cout << bisquares[i] << " ";
	// }
	// cout << endl;

	// generate all sequences of length 2
	// and then check if the rest of the sequence exists
	// sequences are N in length, so you can stop earlier
	for(int i = 0;i < myind-N+1;i++) {
		for(int j = i+1;j < myind-N+2;j++) {
			int wherelastfound = myind;
			int wherefound;
			bool isSequence = true;
			int seqlen = N - 1;
			//int seqlen = 2;
			//while ( (seqlen < N) && isSequence ) {
			while ( (seqlen >=2) && isSequence ) {
				int nextelem = bisquares[i] + seqlen*(bisquares[j] - bisquares[i]);
				// is the next element a valid bisquare?
				// if(!binary_search(&bisquares[0],&bisquares[myind],nextelem))
				// if(!binary_search(&bisquares[j+1],&bisquares[wherefound],nextelem))
				// if(!binary_search(&bisquares[j+seqlen-1],&bisquares[myind],nextelem))
				// if(!binary_search(&bisquares[j+seqlen-1],&bisquares[myind+seqlen+1-N],nextelem))
				// wherefound = mybinarySearch(&bisquares[0],j+1,wherelastfound-1,nextelem);
				wherefound = mybinarySearch2(bisquares,j+1,wherelastfound-1,nextelem);
				if(wherefound<0)
				{
					isSequence = false;
					break;
				}
				else {
					//seqlen++;
					seqlen--;
					wherelastfound = wherefound;					
				}
			} 
			// if this is a valid sequence, push it in
			if (isSequence)
				progs.push_back(make_pair(bisquares[j]-bisquares[i], bisquares[i]));
		}
	}
	cout << "Finished generating sequences" << endl;
	
	sort(progs.begin(), progs.end());
	cout << "Sorted" << endl;

	FILE* fout;
	fout = fopen("ariprog.out", "w");
	if(progs.size() == 0) {
		fprintf(fout, "NONE\n");
	} else {
		for(int i = 0;i < progs.size();i++) {
			printf("%d %d\n", progs[i].second, progs[i].first);
			fprintf(fout,"%d %d\n", progs[i].second, progs[i].first);
		}
	}
	fclose(fout);

	return 0; 
}