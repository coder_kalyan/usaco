#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;

const string PROG = "mountains";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N;
vii mountains;

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N;
    mountains.resize(N);
    REP(i,0,N)
        fin >> mountains[i].second >> mountains[i].first;

    sort(mountains.begin(),mountains.end());
    // REP(i,0,N)
        // cout << mountains[i].first << " " << mountains[i].second << endl;
    long x1, y1, x2, y2;
    // long i = N;
    vii_i beg = mountains.begin();
    for (auto i=mountains.end()-1;i>=mountains.begin();i--) {
        x1 = i->second;
        y1 = i->first;
        // cout << "i: ";
        // cout << x1 << " " << y1 << endl;
        auto j = i-1;
        while (j >= mountains.begin()) {
            x2 = j->second;
            y2 = j->first;
            // cout << "j: ";
            // cout << x2 << " " << y2 << endl;
            if (abs(x2-x1)<=(y1-y2)) {
                // invisible
                auto j2 = j-1;
                mountains.erase(j);
                j = j2;
                i--;
            } else {
                j--;
            }
        }
        // i--;
    }

    int s = mountains.size();
    cout << s << endl;
    fout << s << endl;

	return 0;
}
