"""
ID: kalyan2
LANG: PYTHON3
TASK: maze1
"""

from heapq import *

INF = 1000000000000000
MAXW = 38
MAXH = 100
NODES = MAXW*MAXH + 10

field = []

with open("maze1.in") as f:
    W, H = map(int, f.readline().split())
    for i in range(2*H+1):
        field.append(list(f.readline().rstrip("\n")))

graph = [[0 for x in range(W)] for y in range(H)]

#  for f in field:
    #  print("".join(f))

for j in range(H):
    for i in range(W):
        if field[2*j][2*i+1] == ' ':
            graph[j][i] |= 8
        if field[2*j+2][2*i+1] == ' ':
            graph[j][i] |= 2
        if field[2*j+1][2*i] == ' ':
            graph[j][i] |= 1
        if field[2*j+1][2*i+2] == ' ':
            graph[j][i] |= 4

#  for g in graph:
    #  print(g)

exits = []
for i in range(W):
    if graph[0][i] & 8:
        exits.append((i,0))
    if graph[H-1][i] & 2:
        exits.append((i,H-1))
for j in range(H):
    if graph[j][0] & 1:
        exits.append((0,j))
    if graph[j][W-1] & 4:
        exits.append((W-1,j))

#  print(exits)
#  print()

def dijkstra(sx, sy):
    dist = [[INF for x in range(W)] for y in range(H)] #[INF] * NODES
    vis = [[False for x in range(W)] for y in range(H)] #[False] * NODES
    dist[sy][sx] = 1
    #  vis[sy][sx] = True
    #  dist[0] = 0
    #  vis[0] = True

    pq = [(1,sx, sy)]
    while len(pq) > 0:
        #  print(dist[2])
        cur = heappop(pq)
        cw, cx, cy = cur
        #  print(cur)
        #  print(vis[cy][cx])
        if vis[cy][cx]:
            continue
        #  print("hi")
        vis[cy][cx] = True
        #  i, j = unind(cv)
        if graph[cy][cx] & 8: # UP
            if (cy > 0) and (not vis[cy-1][cx]) and (cw+1<dist[cy-1][cx]):
                heappush(pq,(cw+1,cx,cy-1))
                dist[cy-1][cx] = cw+1
        if graph[cy][cx] & 2: # DOWN
            if (cy < H-1) and (not vis[cy+1][cx]) and (cw+1<dist[cy+1][cx]):
                heappush(pq,(cw+1,cx,cy+1))
                dist[cy+1][cx] = cw+1
        if graph[cy][cx] & 1: # LEFT
            if (cx > 0) and (not vis[cy][cx-1]) and (cw+1<dist[cy][cx-1]):
                heappush(pq,(cw+1,cx-1,cy))
                dist[cy][cx-1] = cw+1
        #  print(graph[cy][cx])
        if graph[cy][cx] & 4: # RIGHT
            #  print("right", graph[cy][cx])
            if (cx < W-1) and (not vis[cy][cx+1]) and (cw+1<dist[cy][cx+1]):
                heappush(pq,(cw+1,cx+1,cy))
                dist[cy][cx+1] = cw+1

    #  for d in dist:
        #  print(d)
    return dist

dist1 = dijkstra(*exits[0])
dist2 = dijkstra(*exits[1])
#  for d in dist1:
    #  print(d)
#  print()
#  for d in dist2:
    #  print(d)
#  print(dist1)
#  print(dist2)
m = 0
for j in range(H):
    for i in range(W):
        m = max(m, min(dist1[j][i],dist2[j][i]))

print(m)
with open("maze1.out", "w") as f:
    f.write(str(m))
    f.write("\n")
#  print(dist)
