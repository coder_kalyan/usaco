#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;

const string PROG = "planting";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N;
vi gType;
vector<vi> conn;
vector<priority_queue<long, vi, greater<long> > > dis;
long numG = 0;

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N;
    gType.resize(N+1);
    conn.resize(N+1);
    dis.resize(N+1);
    fill(gType.begin(),gType.end(),0);
    long tmp,tmp2;
    // cout << N << endl;
    REP (i,1,N) {
        fin >> tmp >> tmp2;
        // cout << i << " " <<  tmp << " " << tmp2 << endl;
        conn[tmp].push_back(tmp2);
        conn[tmp2].push_back(tmp);
    }

    // REP(i,1,N+1) {
        // cout << i << ": ";
        // REP(j,0,conn[i].size())
            // cout << conn[i][j] << " ";
        // cout << endl;
    // }

    REP(i,1,N+1) {
        long g = 1;
        // cout << "size:" << dis[i].size() << endl;
        // if (dis[i].size() > 0) cout << "top: " << dis[i].top() << endl;
        while ((dis[i].size() > 0) && (dis[i].top() == g)) {
            // cout << "top: "<<dis[i].top() << endl;
            g++;
            dis[i].pop();
        }
        // cout << "node " << i << ", chose: " << g << endl;
        numG = max(numG, g);
        gType[i] = g;
        for (auto it=conn[i].begin();it!=conn[i].end();it++) {
            // cout << "dis " << g << " in " << *it << endl;
            dis[*it].push(g);
            for (auto it2=conn[*it].begin();it2!=conn[*it].end();it2++) {
                // cout << "dis " << g << " in " << *it2 << endl;
                dis[*it2].push(g);
                // cout << dis[*it2].top() << endl;
            }
        }
    }

    cout << numG << endl;
    fout << numG << endl;
	return 0;
}
