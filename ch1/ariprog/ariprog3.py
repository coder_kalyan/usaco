"""
ID: kalyan2
TASK: ariprog
LANG: PYTHON3          
"""

from itertools import combinations
from math import *

with open("ariprog.in") as f:
	N = int(f.readline())
	M = int(f.readline())


bisquares = list({(p * p + q * q) for p in range(M + 1) for q in range(M + 1)})

progs = []

def check(prog):
	diff = prog[1] - prog[0]
	prev = prog[1]
	for element in prog[2:]:
		if element - prev != diff:
			return False
		prev = element

	return True

for comb in combinations(bisquares, N):
	if check(comb):
		progs.append(comb)

progs = sorted(progs, key=lambda x: (x[1] - x[0], x[0]))
if len(progs) == 0:
	print("NONE")
	with open("ariprog.out", "w") as f:
		f.write("NONE\n")
else:
	with open("ariprog.out", "w") as f:
		write = f.write
		for p in progs:
			print(p[0], p[1] - p[0])
			# print(p)
			write("{} {}\n".format(p[0], p[1] - p[0]))
