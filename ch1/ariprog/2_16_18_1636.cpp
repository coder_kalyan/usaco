#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;

short N, M;
vi bisquares;
vii progs;

void gen_combs(int step, int left, vi done, int diff, int prev) {
	if(step == 0) {
		// for(int i = 0;i < done.size();i++) {
		// 	cout << done[i] << " ";
		// }
		// cout << endl;
		progs.push_back(make_pair(diff, done[0]));

		return;
	}

	for(int i = left;i < bisquares.size();i++) {
		int temp = bisquares[i];
		if(diff != -1) {
			if(temp - prev == diff) {
				done.push_back(temp);
				gen_combs(step - 1, i + 1, done, diff, temp);
				done.pop_back();
			}
		} else if(prev != -1) {
			done.push_back(temp);
			gen_combs(step - 1, i + 1, done, temp - prev, temp);
			done.pop_back();
		} else {
			done.push_back(temp);
			gen_combs(step - 1, i + 1, done, -1, temp);
			done.pop_back();
		}
	}
}

int main() {
	ifstream fin;
	fin.open("ariprog.in");
	
	fin >> N;
	fin >> M;

	// cout << N << endl << M << endl;

	for(int i = 0;i <= M;i++) {
		for(int j = i;j <= M;j++) {
			bisquares.push_back(i * i + j * j);
		}
	}
	sort(bisquares.begin(), bisquares.end());

	// for(int i = 0;i < bisquares.size();i++) {
	// 	cout << bisquares[i] << " ";
	// }
	// cout << endl;

	// now we have to find all the valid combinations of N elements from
	// the list of bisquares. Note: this will be at most (M*M) Choose N
	// but we are building the generator recursively to automatically filter
	// out non-sequences while generating, even before it finishes
	// generating a set
	vi done;
	done.reserve(N);
	gen_combs(N, 0, done, -1, -1);
	sort(progs.begin(), progs.end());
	progs.erase(unique(progs.begin(), progs.end()), progs.end());
	
	for(int i = 0;i < progs.size();i++) {
		printf("%d %d\n", progs[i].second, progs[i].first);
	}

	return 0;
}