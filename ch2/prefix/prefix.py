"""
ID: kalyan2
LANG: PYTHON3
TASK: prefix
"""

import sys
sys.setrecursionlimit(210000)

prefixes = []
with open("prefix.in") as f:
    tmp = f.readline().replace("\n","")
    while tmp != ".":
        prefixes.extend(tmp.split())
        tmp = f.readline().replace("\n","")

    seq = f.read().replace("\n","")

    print(prefixes)
    max_len = len(max(prefixes, key=len))
    print(max_len)

max_count = 0

def solve(left,count):
    print(left)
    global max_count

    found = False
    for i in range(1,max_len+1):
        if left[:i] in prefixes:
            found = True
            print(i)
            solve(left[i:],count+i)
    if found == False:
        #  print(left, left[:2])
        # there are no more prefixes in the list
        # on this road down
        # check if count is a new maximum
        max_count = max(max_count,count)

solve(seq,0)

print(max_count)
with open("prefix.out", "w") as f:
    f.write(str(max_count))
    f.write("\n")
