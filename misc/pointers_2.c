#include "arraylib.c"

void main(void) {
    int l[5] = {1,2,3,4,5};

    display(l,5);
    negate(l,5);
    display(l,5);
}

void negate(int *a, int n) {
    int i, j;
    for(i = 0;i < n;i++) {
        j = a[i];
        a[i] = -j;
    }
}
