#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;

const string PROG = "snowboots";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N, B;
vi F;
queue<long> S, D;
long numSwitch = 0;

void getBoot(long curDepth) {
    do {
        // LOG("discarded boot (depth)", D.front());
        D.pop();
        S.pop();
        numSwitch++;
    } while ((D.front()<curDepth));
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N >> B;
    F.resize(N);
    REP(i,0,N) {
        fin >> F[i];
    }
    long stmp, dtmp;
    REP(i,0,B) {
        fin >> dtmp >> stmp;
        S.push(stmp);
        D.push(dtmp);
    }

    // when on first tile, any (first) boot will fit
    long tile = 0;

    // jump as far as possible with current boot
    while (tile < (N-1)) {
        // LOG("on tile",tile);
        // LOG("wearing",S.front());
        // LOG("wearing",D.front());
        long sMax = S.front(), dMax = D.front();
        sMax = min(sMax,N-tile-1);
        long s;
        for(s = sMax;s>0;s--) {
            if (F[tile+s] <= dMax) {
                tile += s;
                break;
            }
        }
        // if u couldn't jump, change boots
        if (s == 0)
            getBoot(F[tile]);
    }

    cout << numSwitch << endl;
    fout << numSwitch << endl;
	return 0;
}
