/*
 ID: kalyan2
 LANG: C++
 TASK: spin
 */

#include <iostream>
#include <fstream>
#include <bitset>

using namespace std;

int hole[360];
int wstart[5][5], wext[5][5], speed[5],offset[5];

int main() {
    ifstream fin("spin.in");
    ofstream fout("spin.out");

    int w;
    for (int i=0;i<5;i++) {
        fin >> speed[i] >> w;
        for (int j=0;j<w;j++) {
            fin >> wstart[i][j] >> wext[i][j];
        }
    }

    for (int t=0;t<360;t++) {
        fill(hole,hole+360,0);
        for (int i=0;i<5;i++) { // for each wheel
            for (int j=0;j<5;j++) { // for each wedge
                if (wext[i][j] == 0) break; // out of wedges for wheel
                for (int ang=0;ang<=wext[i][j];ang++) { // for each angle in wedge
                    hole[(offset[i]+ang+wstart[i][j])%360] |= (1 << i);
                }
            }
        }

        for (int i=0;i<360;i++) {
            if (hole[i] == 31) {
                cout << t << endl;
                fout << t << endl;
                return 0;
            }
        }

        for (int i=0;i<5;i++) offset[i] = (offset[i] + speed[i]) % 360;
    }

    cout << "none" << endl;
    fout << "none" << endl;
    return 0;
}
