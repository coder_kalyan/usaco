#include <iostream>

using namespace std;

int N;
int main() {
    cin >> N;
    int a, b;
    for (int i = 0;i < N;i++) {
        cin >> a >> b;
        if (a < b)
            cout << "<" << endl;
        else if (a == b)
            cout << "=" << endl;
        else
            cout << ">" << endl;
    }
}
