"""
ID: kalyan2
TASK: ariprog
LANG: PYTHON3          
"""

from itertools import combinations as builtin_combs
from math import *

N = 2# int(input("N:"))
M = int(input("M:"))


bisquares = list({(p * p + q * q) for p in range(M + 1) for q in range(M + 1)})
print("Generated bisquares")

def is_prog(l):
	# if sum(l) != (l[0] + l[-1]) // 2:
	# 	return False
	prev = l[1]
	diff = prev - l[0]
	for i in l[2:]:
		if i - prev != diff:
			return False
		prev = i

	return True

def combinations(iterable, r):
    # combinations('ABCD', 2) --> AB AC AD BC BD CD
    # combinations(range(4), 3) --> 012 013 023 123
    pool = tuple(iterable)
    n = len(pool)
    if r > n:
        return
    indices = list(range(r))
    # prev = tuple(pool[i] for i in indices)
    yield tuple(pool[i] for i in indices)
    while True:
        for i in reversed(range(r)):
            if indices[i] != i + n - r:
                break
        else:
            return
        indices[i] += 1
        for j in range(i+1, r):
            indices[j] = indices[j-1] + 1
        comb = tuple(pool[i] for i in indices)
        if is_prog(comb):
        	yield comb
        # prev = tuple(pool[i] for i in indices)


def gen_comb(left, r):
	print(len(left))
	if len(left) <= r:
		yield left
	else:
		first = left[0]
		rest = left[1:]
		for c in gen_comb(rest, r - 1):
			yield [first] + c
		for c in gen_comb(rest, r):
			yield [first] + c

def choosenk(alist, n):
    if len(alist) <= n:
        yield alist
        alist = []

    if alist:
        head = alist[0]
        rest = list(alist[1:])
        for c in choosenk(rest, n-1):
            yield [head] + c
        for c in choosenk(rest, n):
            yield c

def generate_combos(alist, k):
    # com = [i for i in range(k)]
    n = len(alist)
    while alist[k - 1] < n:
        #for (int i = 0; i < k; i++)
            #cout << com[i] << " ";
        # cout << endl;
        yield alist[:k]
        t = k - 1;
        while t != 0 and n - k + t != None:
        	alist[t] == n - k + t
        	t -= 1
        alist[t] += 1
        for i in range(1, k):
        	alist[i] = alist[i - 1] + 1;

combs = sorted(list(combinations(bisquares, N)), key = lambda x: (x[1] - x[0], x[0]))
for comb in builtin_combs:
	# print(comb)
	print(comb[0], comb[1] - comb[0])

print("Finished")