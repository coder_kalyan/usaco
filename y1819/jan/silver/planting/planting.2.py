with open("planting.in") as f:
    N = int(f.readline())
    nb = [0] * N
    for i in range(N - 1):
        a, b = map(int, f.readline().split())
        nb[a - 1] += 1
        nb[b - 1] += 1

ans = max(nb) + 1
print(ans)
with open("planting.out", "w") as f:
    f.write(str(ans))
    f.write("\n")
