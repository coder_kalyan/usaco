/*
  ID: kalyan2
  LANG:C++11
  TASK: ttwo
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>
#include <climits>
#include <cmath>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define RREP(i,a,b) for (long i=b-1;i>=a;i--)
#define LOG(a,b) clog << a << ": " << b << endl;
#define MP(a,b) make_pair(a,b)
#define DIST(x1,y1,x2,y2) hypot(abs(x2-x1),abs(y2-y1))

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;

#define INF INT_MAX

const string PROG = "ttwo";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

char field[10][10];
// bool visf[10][10][4] = {false}, visc[10][10][4] = {false};
// int visd[10][10] = {0};
int nf = 0, nc = 0;
char dirf = 'N';
char dirc = 'N';
int dxf = 0, dyf = -1;
int dxc = 0, dyc = -1;

int dirMap(char dir) {
    switch(dir) {
        case 'N':
            return 0;
        case 'E':
            return 1;
        case 'S':
            return 2;
        case 'W':
            return 3;
        default:
            return -1;
    }
}

void rot(char *dir, int *dx, int *dy) {
    switch(*dir) {
        case 'N':
            *dir = 'E';
            *dx = 1;
            *dy = 0;
            break;
        case 'E':
            *dir = 'S';
            *dx = 0;
            *dy = 1;
            break;
        case 'S':
            *dir = 'W';
            *dx = -1;
            *dy = 0;
            break;
        case 'W':
            *dir = 'N';
            *dx = 0;
            *dy = -1;
            break;
        default:
            break;
    }
}

ii pos(ii cur,int *dx, int *dy) {
    return MP(cur.first+(*dx),cur.second+(*dy));
}

ii move(ii cur, char *dir, int *dx, int *dy) {
    ii m = pos(cur,dx,dy);
    if ((m.first >= 10) || (m.first < 0)) {
        rot(dir,dx,dy);
        return cur;
    }
    if ((m.second >= 10) || (m.second < 0)) {
        rot(dir,dx,dy);
        return cur;
    }
    if (field[m.second][m.first] == '*') {
        rot(dir,dx,dy);
        return cur;
    }
    
    return m;
}

// int dist() {
    // return hypo(abs(fp.first-cp.first),abs(fp.second-cp.second));
// }

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    ii fp;
    ii cp;
    REP(j,0,10)
        REP(i,0,10) {
            fin >> field[j][i];
            if (field[j][i] == 'C') {
                field[j][i] = '.';
                cp.first = i;
                cp.second = j;
            }
            if (field[j][i] == 'F') {
                field[j][i] = '.';
                fp.first = i;
                fp.second = j;
            }
        }

    REP(i,0,10) {
        REP(j,0,10)
            cout << field[i][j];
        cout << endl;
    }

    // LOG("FX",fp.first);
    // LOG("FY",fp.second);
    // LOG("CX",cp.first);
    // LOG("CY",cp.second);
    // cout << endl;

    int count = 0;
    while (fp != cp) {
        fp = move(fp,&dirf,&dxf,&dyf);
        cp = move(cp,&dirc,&dxc,&dyc);

        if (count >= 160000) { // (100*4)^2
            count = 0;
            break;
        }

        // if (visf[fp.second][fp.first][dirMap(dirf)]) {
            // if (visc[cp.second][cp.first][dirMap(dirc)]) {
                // if (visd[fp.second][fp.first]==DIST(fp.first,fp.second,cp.first,cp.second)) {
                    // count = 0;
                    // break;
                // }
            // }
        // }
        // visf[fp.second][fp.first][dirMap(dirf)] = true;
        // visc[cp.second][cp.first][dirMap(dirc)] = true;
        // visd[fp.second][fp.first]=DIST(fp.first,fp.second,cp.first,cp.second);

        // if (visc[cp.second][cp.first][dirMap(dirc)]) {
            // cout << cp.first << " " << cp.second << endl;
            // // cout << count << endl;
            // count = 0;
            // break;
        // }
        // visc[cp.second][cp.first][dirMap(dirc)] = true;

        count++;
    }
    
    cout << count << endl;
    fout << count << endl;
	return 0;
}
