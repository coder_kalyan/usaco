"""
ID: kalyan2
LANG: PYTHON3
TASK: sort3
"""

record = []
count1 = 0
count2 = 0
count3 = 0

def should_move(ind):
    val = record[ind]
    if ind < count1:
        return val != 1
    if ind >= count1 and ind < count2:
        return val != 2
    if ind >= count2:
        return val != 3


def count_sm(l):
    return len([x for x in range(len(l)) if should_move(x)])

def find_swap1(l):
    set1 = l[:count1]
    set2 = l[count1:count1 + count2]
    set3 = l[count1 + count2:]
    # print(set1, set2, set3)
    if 2 in set1 and 1 in set2:
        return set1.index(2), count1 + set2.index(1)
    if 3 in set1 and 1 in set3:
        return set1.index(3), count1 + count2+ set3.index(1)
    if 3 in set2 and 2 in set3:
        return count1+ set2.index(3), count1 + count2 + set3.index(2)
    if 2 in set1 and 3 in set2 and 1 in set3:
        return count1+set2.index(3), count1+count2+set3.index(1)
    if 3 in set1 and 1 in set2 and 2 in set3:
        return set1.index(3), count1 + set2.index(1)
    return -1
with open("sort3.in") as f:
    n = int(f.readline())
    for i in range(n):
        record.append(int(f.readline()))
    count1 = record.count(1)
    count2 = record.count(2)
    count3 = record.count(3)

#print(record)
#print(count1, count2, count3)
set1 = record[:count1]
set2 = record[count1:count1 + count2]
set3 = record[count1 + count2:]
print(set1)
print(set2)
print(set3)
print()
count = 0
ans = find_swap1(record)
while ans != -1:
    p, q = ans
    record[p], record[q] = record[q], record[p]
    ans = find_swap1(record)
    count += 1
set1 = record[:count1]
set2 = record[count1:count1 + count2]
set3 = record[count1 + count2:]
print(set1)
print(set2)
print(set3)
print()
# print(record)
print(count)
##mins = len(record) // 2
##def permute(rec, tier):
##    if count_sm(rec) == 0 and tier < mins:
##        mins = tier
##        return
##    for i in range(len(rec)):
##        for j in range(len(rec)):
##            if j == i:
##                pass
##            rec2 = rec[:]
##            rec2[i], rec2[j] = rec2[j], rec2[i]
##            permute(rec2, tier + 1)

#print(mins)


with open("sort3.out", "w") as f:
    f.write(str(count))
    f.write("\n")
# print(mini, minj)
