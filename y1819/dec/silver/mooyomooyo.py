board = []
brd = []

with open("mooyomooyo.in") as f:
    N, K = map(int, f.readline().split())
    for i in range(N):
        tmp = list(map(int, list(f.readline().replace("\n",""))))
        board.append(tmp)

for row in board:
    print("".join(map(str,row)))
print()

def gravity():
    for coli in range(10):
        for rowi in range(N-2,-1,-1):
            if board[rowi+1][coli] == 0:
                board[rowi+1][coli] = board[rowi][coli]
                board[rowi][coli] = 0

def cluster(i, j, v, found):
    global brd
    #if brd[i][j] == 0:
    #    return
    if (i, j) in found:
        return
    #brd[i][j] = 0
    if board[i][j] == v:
        found.append((i,j))
        if (i > 0):
            cluster(i-1,j,v,found)
        if (i < N-1):
            cluster(i+1,j,v,found)
        if (j < 9):
            cluster(i,j+1,v,found)
        if (j > 0):
            cluster(i,j-1,v,found)
    else:
        # no longer part of a cluster
        #if len(found) >= K:
        #    for f,g in found:
        #        board[f][g] = 0
        return


def run_cluster(v):
    global brd
    for rowi in range(N-1,-1,-1):
        for coli in range(10):
            brd = [[1,1,1,1,1,1,1,1,1,1] for x in range(N)]
            found = []
            cluster(rowi,coli,v,found)
            # no longer part of a cluster
            if len(found) >= K:
                for f,g in found:
                    board[f][g] = 0

gravity()
while True:
    for v in range(1,10):
        run_cluster(v)
    print("ran cluster")
    for row in board:
        print("".join(map(str,row)))
    print("ran gravity")
    i = 0
    while True:
        old = [x[:] for x in board]
        gravity()
        if board == old:
            break
        i += 1
    if i == 0:
        break
    for row in board:
        print("".join(map(str,row)))

with open("mooyomooyo.out", "w") as f:
    for row in board:
        print("".join(map(str,row)))
        f.write("".join(map(str,row)))
        f.write("\n")
