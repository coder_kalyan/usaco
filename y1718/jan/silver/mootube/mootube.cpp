#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <queue>

using namespace std;

typedef struct {
    int video;
    int relevance;
} Edge;

#define MAXN 5000
int N, Q;
vector<Edge> adj[MAXN];

int main() {
    ifstream fin("mootube.in");
    ofstream fout("mootube.out");

    fin >> N >> Q;
    int a, b, c;
    Edge tmp;
    for (int i = 0;i < N - 1;i++) {
        fin >> a >> b >> c;
        //cout << a << " " << b << " " << c << endl;
        tmp.video = b - 1;
        tmp.relevance = c;
        adj[a - 1].push_back(tmp);
        tmp.video = a - 1;
        adj[b - 1].push_back(tmp);
    }
    //cout << endl;

    queue<int> q;
    bool vis[MAXN];
    int ans;
    for (int i = 0;i < Q;i++) {
        fin >> a >> b;
        //cout << a << b << endl;
        b--;
        ans = 0;
        fill(vis, vis + N, 0);
        q.push(b);
        vis[b] = 1;
        while (!q.empty()) {
            c = q.front();
            q.pop();
            for (Edge e : adj[c]) {
                if (!vis[e.video] && e.relevance >= a) {
                    //cout << e.video << " " << e.relevance << " " << a << endl;
                    q.push(e.video);
                    vis[e.video] = 1;
                    ans++;
                }
            }
        }
        //cout << ans << endl << endl;
        fout << ans << endl;
    }
        
    return 0;
}
