with open("square.in") as f:
    tmp = list(map(int, f.readline().split()))
    l1 = (tmp[0], tmp[1])
    r1 = (tmp[2], tmp[3])
    tmp = list(map(int, f.readline().split()))
    l2 = (tmp[0], tmp[1])
    r2 = (tmp[2], tmp[3])

print(l1, r1, l2, r2)
l = (min(l1[0], l2[0]),min(l1[1],l2[1]))
r = (max(r1[0], r2[0]),max(r1[1],r2[1]))
print(l, r)

horg = abs(r[0] - l[0])
vorg = abs(r[1] - l[1])
size = max(horg, vorg)
print(size * size)
with open("square.out", "w") as f:
    f.write(str(size * size))
    f.write("\n")
