#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>
#include <climits>
#include <cmath>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define RREP(i,a,b) for (long i=b-1;i>=a;i--)
#define LOG(a,b) clog << a << ": " << b << endl;
#define MP(a,b) make_pair(a,b)
#define DIST(x1,y1,x2,y2) hypot(abs(x2-x1),abs(y2-y1))

#define DEBUG 1
#ifdef DEBUG
#define COUT(str) cout << str;
#else
#define COUT(str) do {} while(0);
#endif

class cmp;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;
typedef priority_queue<ii> pqiif;
typedef priority_queue<ii,vii,cmp> pqiis;
typedef vector<vi> vvi;
typedef vvi::iterator vvi_i;
typedef vector<vii> vvii;
typedef vvii::iterator vvii_i;

class cmp {
    bool operator() (const ii &a, const ii &b) {
        return a.second > b.second;
    }
};

#define INF INT_MAX
#define MAXN 100000

const string PROG = "hayfeast";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

ll N, M;
ll F[MAXN], S[MAXN];

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N >> M;
    REP(i,0,N) {
        fin >> F[i] >> S[i];
        // cout << i << " " << S[i] << endl;
    }

    ll currSp = 0;
    ll minSp;
    ll currFl = 0;
    ll i = 0;
    ll j = i;
    // cout << "test" << endl;
    while (currFl < M && j < N) {
        currFl += F[j];
        currSp = max(currSp, S[j]);
        j++;
        // cout << j << S[j] << endl;
    }
    minSp = currSp;
    // cout << currSp << endl;

    while (j < N) {
        // drop haybale i and increment i
        currFl -= F[i];
        if (currSp == S[i]) {
            // recompute max
            currSp = 0;
            REP(x,i+1,j)
                currSp = max(currSp, S[x]);
        }
        i++;
        // now add haybales as necessary
        while ((currFl < M) && j < N) {
            currFl += F[j];
            currSp = max(currSp, S[j]);
            j++;
        }
        // cout << currSp << " ";
        minSp = min(minSp, currSp);
    }
    
    cout << minSp << endl;
    fout << minSp << endl;

	return 0;
}
