with open("cowtip.in") as f:
    N = int(f.readline())
    grid = []
    for i in range(N):
        grid.append(list(map(int, list(f.readline().replace("\n","")))))

print(grid)
unused_x = list(range(1, N + 1))
unused_y = list(range(1, N + 1))

def flip(gr, size_x, size_y):
    new = [[j for j in x] for x in gr]
    for i in range(size_x):
        for j in range(size_y):
            new[i][j] = int(not new[i][j])
    #  new = [[int(not j) if i2 < size_x else j for i2, j in enumerate(x) ] for x in grid]
    #  for i in range(size_y):
        #  for j in range(size_x):
            #  new[j][i] = 1 if grid[j][i] == 0 else 0 #not new[i][j]

    return new

def count(n_grid):
    c = 0
    for row in n_grid:
        for cell in row:
            if cell == 0:
                c += 1
    
    return c

def find():
    max_c = 0
    max_x = -1
    max_y = -1
    for size_x in unused_x: #range(1, N + 1):
        for size_y in unused_y: #range(1, N + 1):
            tmp = flip(size_x, size_y)
            c = count(tmp)
            if c > max_c:
                max_c = c
                max_x = size_x
                max_y = size_y

    unused_x.remove(max_x)
    unused_y.remove(max_y)
    return max_x, max_y, max_c

min_cn = N * N
def perm(gr, unx, uny, cn):
    global min_cn
    #  if gr == [[1, 1, 0], [0, 0, 0], [0, 0, 0]]:
        #  print(unx, uny)
    if count(gr) == (N * N):
        print(count(gr))
        if cn < min_cn:
            min_cn = cn
        return
    elif len(unx) == 0 or len(uny) == 0:
        return

    for size_x in unx: #range(1, N + 1):
        for size_y in uny: #range(1, N + 1):
            tmp = flip(gr, size_x, size_y)
            #  if gr == [[1, 1, 0], [0, 0, 0], [0, 0, 0]]:
                #  print(gr, size_x, size_y, tmp)
            x2 = unx[:]
            x2.remove(size_x)
            y2 = uny[:]
            y2.remove(size_y)
            perm(tmp, x2, y2, cn + 1)

def disp():
    for g in grid:
        print(g)
    print()

#  grid = [[1, 1, 0], [0, 0, 0], [0, 0, 0]]
#  disp()
#  f = find()
#  grid = flip(f[0],f[1])
#  disp()
#  f = find()
#  grid = flip(f[0],f[1])
#  disp()

perm(grid, unused_x, unused_y, 0)
#  f = find()
#  ans = 0
#  while f[2] < (N * N):
    #  grid = flip(f[0], f[1])
    #  ans += 1
    #  f = find()
#  grid = flip(f[0], f[1])
#  ans += 1

#  for g in grid:
    #  print(g)

ans = min_cn
print(ans)
with open("cowtip.out", "w") as f:
    f.write(str(ans))
    f.write("\n")
