#include <iostream>
#include <cstdlib>
using namespace std;

int main() {
	int actual = 0;
	int guess = 0;
	char command = 0;
	do {
		actual = rand() % 100 + 1;
		do {
			cout << "Guess a number between 1 and 100:";
			if(!(cin >> guess)) {
				// non-numeric value entered - retry
				cin.clear();
				cin.ignore(10000,'\n');
				cout << "Please input numeric values only." << endl;
				continue;
			}

			if(guess > actual) {
				cout << "Too high!" << endl;
			} else if(guess < actual) {
				cout << "Too low!" << endl;
			}
		} while(guess != actual);
		cout << "Correct! The number was indeed " << actual << "!" << endl;
		cout << "Press 'q' to quit, or anything else to play again." << endl;
		cin >> command;
	} while(command != 'q');
	return 0;
}