#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;

const string PROG = "cowdance";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N, TM, minK = 0;
vi D;

bool sim(long k) {
    // simulate the whole shebang, and see
    // if t <= TM
    // note we use a priority queue, which is 
    // logN push and pop
    // we 2N operations, so this runs in NlogN time
    priority_queue<long, std::vector<long>, std::greater<long> > q;
    long i = 0, s = k, t;
    for(i=0;i<k;i++) {
        q.push(D[i]);
        // clog << D[i] << endl;
    }
    // clog << endl;
    while (s > 0) {
        t = q.top();
        q.pop();
        // clog << t << endl;
        if (i < N) {
            q.push(t + D[i]);
            i++;
        } else {
            // out of cows, so the queue should shorten
            s--;
        }
    }
    // clog << endl << t << endl;

    return t <= TM;
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N >> TM;
    // cout << N << " " << TM << endl;
    D.resize(N);
    REP(i,0,N)
        fin >> D[i];

    // now that we have sim (NlogN time), we need to
    // run it to find the lowest that works
    // this will be N^2logN, which works - but we
    // can do binary search (Nlog^2N) since the
    // relationship is monotonic
	long lowK = 1;
    long highK = N;

    long testK;
    while ((lowK+1) < highK) {
            testK = (highK + lowK) / 2;
            if (sim(testK)) {
                    highK = testK;
            }
            else {
                    lowK = testK;
            }
    }
    if (sim(lowK))
        minK = lowK;
    else
        minK = highK;

    cout << minK << endl;
    fout << minK << endl;
	return 0;
}
