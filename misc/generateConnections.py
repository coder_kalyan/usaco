def permute(paired,nums_left):
	f = nums_left[0]
	for i in nums_left[1:]:
		p2 = paired[:] + [(f, i)]
		n2 = [x for x in nums_left if x != f and x != i]
		if len(n2) > 0:
			permute(p2, n2)
		else:
			print(p2)

permute([], list(range(1,9)))
