/*
  ID: kalyan2
  LANG:C++11
  TASK: runround
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>

using namespace std;

typedef long long ll;
typedef unsigned long ul;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const string PROG = "runround";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

ul M;

char digits[9];     // ans always fits into unsigned long, 2^32 is 9 decimal digits
short nDig;
bool seen[10];  // seen only needs 9 dig but array is reused for unique checker
bool isRun(ul n) {
    nDig = sprintf(digits, "%lu", n);
    fill(begin(seen), end(seen), false);
    short pos = 0, conv, unused = nDig;
    short keepAlive = 1;
    // checks that all digits are unique
    for (int i = 0;i < nDig;i++) {
        conv = (short) digits[i] - 48;
        if (conv == 0)
            return false;
        else if (!seen[conv])
            seen[conv] = true;
        else
            return false;
    }
    fill(begin(seen), end(seen), false);
    seen[0] = true;

    short start = (short) digits[pos] - 48;
    while (true) {
        conv = (short) digits[pos] - 48;
        if (conv == 0)
            return false;
        pos = (pos + conv) % (nDig);
        if (keepAlive == -1)
            return start == conv;
        unused--;
        if (!seen[pos])
            seen[pos] = true;
        else if (unused > 0)
            return false;
        else
            keepAlive = -1;
    }
}

int main() {
	ifstream fin (FIN);
    fin >> M;

    bool keepAlive = true;
    while (!isRun(++M)) {}
    cout << M << endl;

	ofstream fout (FOUT);
    fout << M << endl;

	return 0;
}
