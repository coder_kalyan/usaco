/* USACO Program
ID: kalyan2
LANG: C
TASK: friday
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// defines the # of days in a month for every month EXCEPT FEBRUARY
// speeds up calculations, only calculate for february, because it could be 28 or 29,
// depending on leap year
const unsigned char daysInMonth[] = {31,0,31,30,31,30,31,31,30,31,30,31};

// calculates whether or not this is a leap year
unsigned char isLeapYear(unsigned short currentYear) {
	unsigned char isLeap;
	// for non-century years
	if(currentYear % 100 != 0) {
		if(currentYear % 4 == 0)
			isLeap = 1;
		else
			isLeap = 0;
	} else {
		if(currentYear % 400 == 0)
			isLeap = 1;
		else
			isLeap = 0;
	}

	return isLeap;
}

// calculates date for tomorrow
unsigned char nextDate(unsigned char currentDate,unsigned char currentMonth,unsigned short currentYear) {
	unsigned char next;
	// for regular non-february months
	if(currentMonth != 1) {
		if(currentDate >= daysInMonth[currentMonth])
			next = 1;
		else
			next = currentDate + 1;
	} else {
		if(isLeapYear(currentYear) == 1) {
			if(currentDate >= 29)
				next = 1;
			else
				next = currentDate + 1;
		} else {
			if(currentDate >= 28)
				next = 1;
			else
				next = currentDate + 1;
		}
	}

	return next;
}

unsigned char nextDay(unsigned char currentDay) {
	if(currentDay == 6)
		return 0;
	else
		return currentDay + 1;
}

unsigned char nextMonth(unsigned char currentMonth,unsigned char currentDate,unsigned char currentYear) {
	if(currentMonth >= 11)
		return 0;
	else
		return currentMonth + 1;
}

void main(void) {
	FILE *fin, *fout;
	unsigned short N;

	/*
	ORDER:
	0 = SATURDAY
	1 = SUNDAY
	2 = MONDAY
	3 = TUESDAY
	4 = WEDNESDAY
	5 = THURSDAY
	6 = FRIDAY
	*/
	// NOTE: day and month are 0-indexed, while date is 1-indexed
	unsigned char day;	// current day
	unsigned char date;	// current date
	unsigned char tempDate;
	// current month, 0 = JANUARY
	unsigned char month;
	unsigned short year;	// current year
	// count of 13ths on different days
	// uses same order as above
	unsigned short count13ths[7] = {0,0,0,0,0,0,0};

	unsigned short loop;

	// open the files
	fin = fopen("friday.in","r");
	fout = fopen("friday.out", "w");

	// read in the value of N
	fscanf(fin, "%hu\n", &N);

	// JANUARY 1st, 1900 was a MONDAY
	day = 2;
	date = 1;
	month = 0;
	for(year = 1900;year < 1900 + N;year++) {
		if(isLeapYear(year) == 1) {
			for(loop = 0;loop < 366;loop++) {
				if(date == 13) {
					count13ths[day]++;
				}

				// TODO: we need to implement a smart increment for month
				day = nextDay(day);
				tempDate = nextDate(date,month,year);
				if(tempDate < date)
					month = nextMonth(month,date,year);
				date = tempDate;
			}
		} else {
			for(loop = 0;loop < 365;loop++) {
				if(date == 13) {
					count13ths[day]++;
				}

				day = nextDay(day);
				tempDate = nextDate(date,month,year);
				if(tempDate < date)
					month = nextMonth(month,date,year);
				date = tempDate;
			}
		}
	}

	for(loop = 0;loop < 7;loop++)
		if(loop == 6)
			fprintf(fout,"%d\n",count13ths[loop]);
		else
			fprintf(fout,"%d ",count13ths[loop]);
	exit(0);
}
