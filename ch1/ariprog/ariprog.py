"""
ID: kalyan2
TASK: ariprog
LANG: PYTHON3          
"""

from itertools import combinations
from math import *

with open("ariprog.in") as f:
	N = int(f.readline())
	M = int(f.readline())

bisquares = {(p * p + q * q) for p in range(M + 1) for q in range(p, M + 1)}
twom2 = 2*M*M

def search(n1, n2):
	d = n2 - n1
	# find the k-th element of this series that will be bigger than 2M*M
	# k = ceil( (twom2 - n1)/d + 1) + 1
	k = (twom2 - n1) // d + 2 # same result as line above, faster
	if k <= N:
		return False
	for check in range(N-1,1,-1):
		if (n1 + check * d) not in bisquares:
		 	return False
	return True

print("Finished generating bisquares")
combs = combinations(bisquares, 2)

progs = []
append = progs.append


for comb in combs:
	if search(comb[0], comb[1]):
		append((comb[0], comb[1] - comb[0]))

progs = sorted(progs, key = lambda x: (x[1], x[0]))

if len(progs) == 0:
	print("NONE")
	with open("ariprog.out", "w") as f:
		f.write("NONE\n")
else:
	with open("ariprog.out", "w") as f:
		write = f.write
		for (a, b) in progs:
			print(a, b)
			write("{} {}\n".format(a, b))

