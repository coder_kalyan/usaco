tank1 = 1000
tank2 = 1000
with open("backforth.in") as f:
    bc1 = list(map(int, f.readline().split()))
    bc2 = list(map(int, f.readline().split()))

print(bc1)
print(bc2)

possible = set()
def back(c,l1, l2, t1, t2):
    if c == 0:
        # we are done running
        possible.add(t1)
    
    elif c % 2 == 0:
        for bucket in l1:
            tmp1 = l1[:]
            tmp1.remove(bucket)
            tmp2 = l2[:]
            tmp2.append(bucket)
            back(c - 1, tmp1[:], tmp2[:], t1 - bucket, t2 + bucket)
    else:
        for bucket in l2:
            tmp1 = l1[:]
            tmp1.append(bucket)
            tmp2 = l2[:]
            tmp2.remove(bucket)
            back(c - 1, tmp1[:], tmp2[:], t1 + bucket, t2 - bucket)

back(4, bc1[:], bc2[:], tank1, tank2)
ans = len(possible)
print(ans)
with open("backforth.out", "w") as f:
    f.write(str(ans))
    f.write("\n")

