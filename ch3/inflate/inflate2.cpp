/*
ID: kalyan2
LANG: C++11
TASK: inflate
*/

// #include <bits/stdc++.h>
#include <iostream>
#include <fstream>

int dp[10001];

using namespace std;

int main() {
    ifstream fin("inflate.in");
    int M, N;
    fin >> M >> N;
    int pt, mi;
    for (int i = 0;i < N;i++) {
        cout << i << endl;
        fin >> pt >> mi;
        for (int j = mi;j <= M;j++) {
            dp[j] = max(dp[j], dp[j - mi] + pt);
        }
    }

    cout << dp[M] << endl;
    ofstream fout("inflate.out");
    fout << dp[M] << endl;
}
