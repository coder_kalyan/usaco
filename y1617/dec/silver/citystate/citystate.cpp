#include <iostream>
#include <fstream>
#include <string>
#include <map>

using namespace std;

int N;
map<string, int> M;
int main() {
    ifstream fin("citystate.in");
    ofstream fout("citystate.out");

    fin >> N;
    string a, b, c;
    for (int i = 0;i < N;i++) {
        fin >> a >> b;
        if (a.substr(0, 2) == b)
            continue;
        c = a.substr(0, 2) + b;
        auto loc = M.find(c);
        if (loc != M.end())
            loc->second++;
        else
            M.insert(make_pair(c, 1));
    }

    long ans = 0;
    for (auto key : M) {
        a = key.first.substr(0, 2);
        b = key.first.substr(2, 2);
        c = b + a;
        //cout << a << " " << b << endl;
        auto loc = M.find(c);
        if (loc != M.end())
            ans += key.second * loc->second;
    }

    ans /= 2;
    cout << ans << endl;
    fout << ans << endl;

    return 0;
}
