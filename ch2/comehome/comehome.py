"""
ID: kalyan2
LANG: PYTHON3
TASK: comehome
"""

from heapq import *

INF = 10000000000000000

adj = dict()
with_cow = set()
vis = dict()

def dijkstra(source):
    dis[source] = 0
    #  vis[source] = True
    pq = []
    heappush(pq, (0, source))
    while len(pq) > 0:
        cur = heappop(pq)
        cv = cur[1]
        cw = cur[0]
        if vis[cv]:
            continue
        #  print("cv",cur,cv,adj[cv])
        vis[cv] = True
        for vert in adj[cv]:
            #  print(vert,vis[vert[0]],vert[1]+cw,dis[vert[0]])
            if (not vis[vert[0]]) and vert[1]+cw<dis[vert[0]]:
                heappush(pq,(vert[1]+cw,vert[0]))
                dis[vert[0]] = vert[1]+cw

with open("comehome.in") as f:
    P = int(f.readline())
    for i in range(P):
        a, b, c = f.readline().split(" ")
        a = a[0]
        b = b[0]
        c = int(c)
        if a not in adj:
            adj[a] = []
        if b not in adj:
            adj[b] = []
        if a not in vis:
            vis[a] = False
        if b not in vis:
            vis[b] = False
        adj[a].append((b, c))
        adj[b].append((a, c))
        if a != 'Z' and a.isupper():
            with_cow.add(a)
        if b != 'Z' and b.isupper():
            with_cow.add(b)

dis = {x:INF for x in adj.keys()}
#  print(adj)
#  for b in adj.items():
    #  print(b)
#  print(with_cow)
#  print(vis)
dijkstra('Z')
print(dis)
ans = min([(b,a) for a,b in dis.items() if a in with_cow])
print(ans[1], ans[0])
with open("comehome.out", "w") as f:
    f.write(ans[1])
    f.write(" ")
    f.write(str(ans[0]))
    f.write("\n")
