#include <fstream>
#include <algorithm>

using namespace std;

int N;
int W[100000];

int main() {
    ifstream fin("lemonade.in");
    ofstream fout("lemonade.out");

    fin >> N;
    for (int i = 0;i < N;i++) fin >> W[i];
    sort(W, W + N, greater<int>());

    int ans = 0;
    for (int i = 0;i < N;i++) {
        if (W[i] < ans) break;
        ans++;
    }

    fout << ans << endl;
     
    return 0;
}
