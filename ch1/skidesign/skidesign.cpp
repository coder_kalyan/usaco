/*
ID: kalyan2
TASK: skidesign
LANG: C++                 
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

short N;
vector<short> hills;

pair<short, short> getMaxIndex() {
	short largestI = 0;
	short largest = hills[largestI];
	for(int i = 1;i < hills.size();i++ )
		if(hills[i] > largest) {
			largestI = i;
			largest = hills[largestI];
		}
	return make_pair(largest, largestI);
}

pair<short, short> getMinIndex() {
	short smallestI = 0;
	short smallest = hills[smallestI];
	for(int i = 1;i < hills.size();i++ )
		if(hills[i] < smallest) {
			smallestI = i;
			smallestI = hills[smallestI];
		}
	return make_pair(smallest, smallestI);
}

int main() {
	// generatePairs();
	ifstream fin;
	fin.open("skidesign.in");

	fin >> N;
	// cout << N;

	// read in the wormholes
	// even though our main algorithm doesn't use wormhole position, we need it
	// for checking for wormholes on the same line

	// first just read in the wormholes
	for(long i = 0;i < N;i++) {
		short temp;
		fin >> temp;
		hills.push_back(temp);
	}

	fin.close();
	
	short money = 0;//, x, y;
	sort(hills.begin(), hills.end());
	for(int i = 0;i < hills.size();i++)
		cout << hills[i] << " ";
	cout << endl;
	// cout << hills.front() << " " << hills.back() << endl;
	/*int count = 1;
	while(hills.back() - hills.front() > 17) {
	//for(int i = 0;i < 10;i++) {
		x = hills.back() - hills.front();
		// cout << hills.front() << " " << hills.back() << endl;
		y = x  - 16;
		// cout << floor(y / 2) << endl;
		hills.front() += (short) floor(y / 2);
		hills.back() -= (short) ceil(y / 2);
		// cout << hills.front() << " " << hills.back() << endl;
		sort(hills.begin(), hills.end());
		money += 2 * floor(y / 2) * ceil(y / 2);
		// cout << money << endl;
		if(count % 10 == 0) {
			// cout << hills.size() << " ";
			
		}
		count++;
	}*/
	int y = hills.back() - hills.front() - 18;
	int i = 0;
	int base = hills.front() + floor(y / 2);
	printf("Base: %d\n", base);
	while(i < hills.size()) {
		if(hills[i] < base) {
			int delta = base - hills[i];
			hills[i] += base;
			money += delta * delta;
		}
		i++;
	}
	i = 0;
	base = hills.back() - ceil(y / 2);

	printf("Base: %d\n", base);
	while(i < hills.size()) {
		if(hills[i] > base) {
			int delta = hills[i] - base;
			hills[i] -= base;
			money += delta * delta;
		}
		i++;
	}

	sort(hills.begin(), hills.end());
	for(int i = 0;i < hills.size();i++)
		cout << hills[i] << " ";
	cout << endl;
	/*pair<short, short> max, min;
	do {
		max = getMaxIndex();
		min = getMinIndex();
		// cout << max.first << " " << min.first << endl;

		short diff = max.first - min.first - 17;
		//cout << diff << endl;
		if(diff > 0) {
			money += 2 * ((diff / 2) * (diff / 2));
			// cout << diff / 2;
			// cout << hills[max.second] << endl;
			hills[max.second] -= diff / 2;
			cout << hills[max.second] << endl;
			cout << hills[max.second] << endl;
			hills[min.second] += diff / 2;
			cout << hills[max.second] << endl;
			
		}
		cout << hills[max.second] << endl;
			

	} while(max.first - min.first > 17);*/
	cout << money << endl;
	ofstream fout;
	fout.open("skidesign.out");
	fout << money << endl;
	fout.close();
	return 0;
}
