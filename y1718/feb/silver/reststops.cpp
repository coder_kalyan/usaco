#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;

const string PROG = "reststops";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long L, N, rF, rB;
vii xc;

bool cmpRest(ii a, ii b) { return a.second < b.second; }

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> L >> N >> rF >> rB;
    xc.resize(N);
    long xi, ci;
    long xEnd = 0;
    REP(i,0,N) {
        fin >> xi >> ci;
        xEnd = max(xEnd, xi);
        xc[i] = make_pair(xi,ci);
        clog << xi << " " << ci << endl;
    }
    clog << endl;

    long tot = (rF - rB) * L;
    long uTot = 0;
    if (tot <= 0) {
        cout << "0" << endl;
        fout << "0" << endl;
        return 0;
    }

    clog << "TOT: " << tot << endl;
    long tRem = tot;
    long S, U, xStart;
    vii_i start = xc.begin();
    xStart = 0;

    clog << "Rem: " << tRem << endl;
    while ((tRem > 0) && (xStart < xEnd)) {
        vii_i max_pos = max_element(start,xc.end(),cmpRest);
        // clog << max_pos->first << " ";
        // clog << max_pos->second << endl;
        LOG("xStart",xStart);
        S = (rF - rB) * ((max_pos->first)-xStart);
        S = min(S,tRem);
        LOG("S",S);
        U = S * max_pos->second;
        uTot += U;
        tRem -= S;
        xStart = max_pos->first;
        clog << "Rem: " << tRem << endl;
    
        start = max_pos + 1;
    }

    cout << uTot << endl;
    fout << uTot << endl;
	return 0;
}
