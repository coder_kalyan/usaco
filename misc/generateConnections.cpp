#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;

void permute(vector<pair<long, long> >& paired, vector<long>& nums_left);

long main() {
	vector<pair<long, long> > pairs;
	vector<long> nums_left;
	for(long i = 1; i < 9;i++) {
		nums_left.push_back(i);
	}
	permute(pairs, nums_left);
	return 0;
}

void permute(vector<pair<long, long> >& paired, vector<long>& nums_left) {
	long f = nums_left[0];
	for(long i = 1; i < nums_left.size(); i++) {
		long current = nums_left[i];
		vector<pair<long, long> > p2;
		vector<long> n2;
		for(long j = 0;j < paired.size(); j++) {
			p2.push_back(paired[j]);
		}
		pair<long, long> newPair = make_pair(f, current);
		p2.push_back(newPair);
		for(long j = 0;j < nums_left.size(); j++) {
			if(nums_left[j] != f && nums_left[j] != current)
				n2.push_back(nums_left[j]);
		}
		if(n2.size() > 0) {
			permute(p2, n2);
		} else {
			//for(auto p: p2) {
			//	cout << p;
			//}
			for(long i = 0;i < p2.size(); i++) {
				cout << "(" << p2[i].first << ", " << p2[i].second << "), ";
			}
			cout << endl;
		}
	}
}