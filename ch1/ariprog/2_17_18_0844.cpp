/*
ID: kalyan2
TASK: ariprog
LANG: C++
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;

short N, M;
int bisquares[63002];
int progsarray[   ]
vii progs;
vector<bool> mask;
int myind = 0;

int main() {
	ifstream fin;
	fin.open("ariprog.in");
	
	fin >> N;
	fin >> M;

	// int myind = 0;
	for(int i = M;i >=0;i--) {
		for(int j = i;j >=0;j--) {
			int mytemp = (i*i) + (j*j);
			// add to array only if not duplicate
			int inarray = 0;
			for (int k =0; k < myind; k++) {
				if (bisquares[k] == mytemp) {
					inarray = 1;
					break;
				}
			}
			if (!inarray)
				bisquares[myind++] = mytemp;				
		}
	}

	sort(&bisquares[0], &bisquares[myind]);
	// remove duplicates now


	//bisquares.erase(unique(bisquares.begin(), bisquares.end()), bisquares.end());
	
	cout << "Finished generating bisquares" << endl;

	//for(int i = 0;i < bisquares.size();i++) {
	// for(int i = 0;i < myind;i++) {
	// 	cout << bisquares[i] << " ";
	// }
	// cout << endl;

	// generate all sequences of length 2
	for(int i = 0;i < myind-1;i++) {
		for(int j = i+1;j < myind;j++) {
			progs.push_back(make_pair(bisquares[j]-bisquares[i], bisquares[i]));
			mask.push_back(true);
		}
	}

	cout << "Finished seq length 2" << endl;
	// for(int i = 0;i < progs.size();i++) {
	// 	printf("%d %d\n", progs[i].second, progs[i].first);
	// }

	// now for each of these sequences, see if the rest of the sequence exists
	// starting from the last element
	for (int i = 0; i < progs.size(); i++) {
		int seqlen = N - 1;
		while ( (seqlen >= 2) && mask[i] ) {
			int nextelem = progs[i].second + seqlen*progs[i].first;
			if(!binary_search(&bisquares[0],&bisquares[myind],nextelem))
				mask[i] = false;
			else
				seqlen--;
		}
	}

	cout << "Finished generating sequences" << endl;
	vii filtered;
	for(int i = 0;i < progs.size();i++) {
		if(mask[i])
			filtered.push_back(progs[i]);
	}
	
	sort(filtered.begin(), filtered.end());
	cout << "Sorted" << endl;
	FILE* fout;
	fout = fopen("ariprog.out", "w");
	if(filtered.size() == 0) {
		fprintf(fout, "NONE\n");
	} else {
		for(int i = 0;i < filtered.size();i++) {
			printf("%d %d\n", filtered[i].second, filtered[i].first);
			fprintf(fout,"%d %d\n", filtered[i].second, filtered[i].first);
		}
	}


	fclose(fout);
	return 0; 
}