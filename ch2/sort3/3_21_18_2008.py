"""
ID: kalyan2
LANG: PYTHON3
TASK: sort3
"""

record = []

def should_move(ind):
    val = record[ind]
    if ind > 0:
        p1 = any([x > val for x in record[:ind - 1]])
    else:
        p1 = False
    if ind < len(record):
        p2 = any([x < val for x in record[ind + 1:]])
    else:
        p2 = False
    return p1 or p2

    #if val == 1:
    #    return all([x == 1 for x in record[0:ind-1]])
    #elif val == 2:
    #    return all[x < 

def count_sm():
    return len([x for x in range(len(record)) if should_move(x)])

with open("sort3.in") as f:
    n = int(f.readline())
    for i in range(n):
        record.append(int(f.readline()))

print(record)
#for i in range(len(record)):
    #print(should_move(i))
min_sm = count_sm()
# mini, minj = -1, -1
mins = [(-1, -1)]
count = 0
while min_sm > 0:
    for i in range(len(record)):
        for j in range(len(record)):
            if j == i:
                pass
            record[i], record[j] = record[j], record[i]
            if count_sm() < min_sm:
                min_sm = count_sm()
                mins[-1] = (i, j)
            record[i], record[j] = record[j], record[i]
    i, j = mins[-1]
    record[i], record[j] = record[j], record[i]
    mins.append((-1, -1))
    count += 1

del mins[-1]
print(min_sm, mins)
print(count)

with open("sort3.out", "w") as f:
    f.write(str(count))
    f.write("\n")
# print(mini, minj)
