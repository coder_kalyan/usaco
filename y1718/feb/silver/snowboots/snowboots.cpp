#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

#define MAXN 250
int N, B, F[MAXN], S[MAXN], D[MAXN], best = 1000000000, vis[MAXN][MAXN];

void sim(int i, int b) {
    if (vis[i][b]) return;
    vis[i][b] = 1;
    if (i == N-1) {
        best = min(best, b);
        return;
    }
    for (int ti = i+1;ti < N && ti-i <= D[b];ti++)
        if (F[ti] <= S[b])
            sim(ti, b);
    for (int tb = b+1;tb < B;tb++)
        if (F[i] <= S[tb])
            sim(i, tb);
}

int main() {
    ifstream fin("snowboots.in");
    ofstream fout("snowboots.out");

    fin >> N >> B;
    for (int i = 0;i < N;i++) fin >> F[i];
    for (int i = 0;i < B;i++) fin >> S[i] >> D[i];

    sim(0, 0);
    cout << best << endl;
    fout << best << endl;

    return 0;
}
