"""
ID: kalyan2
LANG: PYTHON3
TASK: subset
"""

with open("subset.in", "r") as f:
    N = int(f.readline())

fullset = [x for x in range(1, N + 1)]
print(fullset)

sa = []
sb = fullset[:]

ans = set()

class Answer:
    def __init__(self, a, b):
        self.a = a
        self.b = b

def splice(a, b):
    if sum(a) == sum(b):
        ans.add((tuple(a), tuple(b)))
    elif len(b) == 0:
        return

    for i in range(0, len(b)):
        temp = b[i]
        a.append(temp)
        b.remove(temp)

        splice(a, b)

        a.remove(temp)
        b.insert(i, temp)

splice(sa, sb)
for answer in ans:
    print(answer)#.a, answer.b)
