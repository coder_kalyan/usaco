#include <bits/stdc++.h>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define RREP(i,a,b) for (long i=b-1;i>=a;i--)
#define LOG(a,b) clog << a << ": " << b << endl;
#define MP(a,b) make_pair(a,b)
#define DIST(x1,y1,x2,y2) hypot(abs(x2-x1),abs(y2-y1))

#define DEBUG 1

#ifdef DEBUG
#define COUT(str) cout str;
#else
#define COUT(str) {}
#endif


class cmp;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;
typedef priority_queue<ii> pqiif;
typedef priority_queue<ii,vii,cmp> pqiis;
typedef vector<vi> vvi;
typedef vvi::iterator vvi_i;
typedef vector<vii> vvii;
typedef vvii::iterator vvii_i;

class cmp {
    bool operator() (const ii &a, const ii &b) {
        return a.second > b.second;
    }
};

#define INF INT_MAX

const string PROG = "snakes";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N, K;
long GR[400];

long dfs(long nth, long size, long k, long waste) {
    // cout << GR[nth] << size << endl;
    if (nth >= N) return waste; // done with all groups
    if (k > K) return INF; // made too many switches
    if (size < GR[nth]) return INF; // size doesn't fit
    long ret = dfs(nth+1,size,k,waste + (size-GR[nth]));
    // if (waste == 0) cout << ret << endl;
    // REP(i,0,N) {
    // long choice = 0;
    for (long xyz = 0;xyz < N;xyz++) {
        // cout << xyz << " ";
        // if (GR[xyz] >= size) cout << xyz << " ";
         ret = min(ret, dfs(nth+1,GR[xyz],k+1,waste + (size-GR[nth])));
             // if (tmp < ret) {
                 // ret = tmp;
                 // choice = xyz;
             // }
        // cout << dfs(nth+1,GR[i],k+1,waste + (size-GR[nth])) << endl;
    }
    // cout << "CHOICE:" << choice << endl;
    // cout << endl;
    // cout << ret << endl;
    return ret;
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N >> K;
    REP(i,0,N) fin >> GR[i];
    // REP(i,0,N) cout << GR[i] << " ";
    // cout << endl;
    long ans = INF;
    REP(i,0,N) ans = min(ans, dfs(0,GR[i],0,0));
    cout << ans << endl;

	return 0;
}
