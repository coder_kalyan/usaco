#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <queue>

using namespace std;

#define MAXN 200
int N, X[MAXN], Y[MAXN], POW[MAXN], vis[MAXN];
vector<int> adj[MAXN];

int main() {
    ifstream fin("moocast.in");
    ofstream fout("moocast.out");

    fin >> N;
    for (int i = 0;i < N;i++)
        fin >> X[i] >> Y[i] >> POW[i];

    for (int i = 0;i < N;i++) {
        for (int j = 0;j < N;j++) {
            //if (i == j)
                //continue;
            if (hypot(X[j] - X[i], Y[j] - Y[i]) <= POW[i])
                adj[i].push_back(j);
        }
    }

    int ans = 0, count, cur;
    queue<int> q;
    for (int i = 0;i < N;i++) {
        fill(vis, vis + N,0);
        //vis[i] = 1;
        count = 0;
        q.push(i);
        while (!q.empty()) {
            cur = q.front();
            q.pop();
            if (vis[cur])
                continue;
            vis[cur] = 1;
            count++;
            //cout << "#nb: " << adj[cur].size() << endl;
            for (int nb : adj[cur])
                q.push(nb);
        }
        //cout << count << endl;
        ans = max(ans, count);
    }

    cout << ans << endl;
    fout << ans << endl;
    
    return 0;
}
