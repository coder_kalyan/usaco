from queue import *

def max_elem(m):
    mi = 0
    mv = m[mi]

    for i, v in enumerate(m):
        if v > mv:
            mv = v
            mi = i

    return mi

def sim(r):
    b = bales[:]

    for i in range(K):
        q = []
        m = []
        #  print(q)
        for i in range(N):
            while len(q) > 0 and b[i] - q[0] < r:
                del q[0]
            q.append(b[i])
            m.append(len(q))

        for i in range(N-1,-1,-1):
            while len(q) > 0 and q[0] - b[i] < r:
                del q[0]
            q.append(b[i])
            m[i] += (len(q))
            c = max_elem(m)
        #  print("before:",len(b))
        #  b[c-r:c+r] = [0] * r
        for i in range(len(b)):
            if i >= 0 and i < N and abs(b[c]-b[i]) <= r:
                print("zeroing",i,b[i])
                b[i] = 0
            #  b[i] = 0

        #  print("after:",len(b))
        m[c] = 0

    return all([x == 0 for x in b])

with open("angry.in") as f:
    N, K = map(int, f.readline().split())
    bales = []
    for i in range(N):
        bales.append(int(f.readline()))

    lowK = 1
    highK = N
    testK = -1
    ans = -1

    while lowK+1 < highK:
        testK = (highK + lowK) // 2
        if sim(testK):
            highK = testK
        else:
            lowK = testK

    if sim(testK):
        ans = lowK
    else:
        ans = highK

    print(ans)
    
