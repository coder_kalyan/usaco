N = int(input())

points = []

for i in range(N):
    a, b = map(int, input().split())
    points.append((a, 1))
    points.append((b + 1, -1))

points = sorted(points, key = lambda x:x[0])

count = [0 for i in range(N + 1)]

overlap = 0
prev = 0
for point in points:
    count[overlap] += point[0] - prev
    overlap += point[1]
    prev = point[0]

print(" ".join(map(str, count[1:])))
