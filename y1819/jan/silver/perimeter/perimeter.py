with open("perimeter.in") as f:
    N = int(f.readline())
    grid = []
    for i in range(N):
        grid.append(list(map(lambda x: 1 if x == '#' else 0, list(f.readline()))))

# print(grid)
# print()
# print()
fill = [[-1 for i in range(N)] for j in range(N)]

def flood_fill(x, y, n):
    if x < 0 or x >= N or y < 0 or y >= N:
        return
    if grid[y][x] == 0:
        return
    if fill[y][x] != -1:
        return
    # print(x, y)
    fill[y][x] = n
    flood_fill(x + 1, y, n)
    flood_fill(x - 1, y, n)
    flood_fill(x, y + 1, n)
    flood_fill(x, y - 1, n)

island = 0
for y in range(N):
    for x in range(N):
        if grid[y][x] == 0:
            continue
        if fill[y][x] != -1:
            continue
        flood_fill(x, y, island)
        island += 1
        # for i in range(N):
            # print(fill[i])
        # print()

def is_nb(x, y):
    if x < 0 or x >= N or y < 0 or y >= N:
        return False
    if grid[y][x] == 0:
        return False
    return True

def count_nb(x, y):
    ret = is_nb(x + 1, y)
    ret += is_nb(x - 1, y)
    ret += is_nb(x, y + 1)
    ret += is_nb(x, y - 1)
    
    return ret

# for i in range(N):
    # print(fill[i])

area = [0] * island
peri = [0] * island

for y in range(N):
    for x in range(N):
        if fill[y][x] != -1:
            area[fill[y][x]] += 1
            peri[fill[y][x]] += (4 - count_nb(x, y))

mxa = 0
mnp = 1000000000000000
mxi = 0
for i in range(island):
    if area[i] > mxa:
        mxa = area[i]
        mxi = i
        mnp = peri[i]
    elif area[i] == mxa:
        if peri[i] < mnp:
            mnp = peri[i]
            mxi = i

print(area[mxi], peri[mxi])

with open("perimeter.out", "w") as f:
    f.write(str(area[mxi]))
    f.write(" ")
    f.write(str(peri[mxi]))
    f.write("\n")
