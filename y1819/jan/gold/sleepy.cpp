#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>
#include <climits>
#include <cmath>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define RREP(i,a,b) for (long i=b-1;i>=a;i--)
#define LOG(a,b) clog << a << ": " << b << endl;
#define MP(a,b) make_pair(a,b)
#define DIST(x1,y1,x2,y2) hypot(abs(x2-x1),abs(y2-y1))

class cmp;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;
typedef priority_queue<ii> pqiif;
typedef priority_queue<ii,vii,cmp> pqiis;
typedef vector<vi> vvi;
typedef vvi::iterator vvi_i;
typedef vector<vii> vvii;
typedef vvii::iterator vvii_i;

class cmp {
    bool operator() (const ii &a, const ii &b) {
        return a.second > b.second;
    }
};

#define INF INT_MAX

const string PROG = "sleepy";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

struct Node {
    int l, r;
    int num;
    int small;
};

int N;
vector<Node> cows;
int cnt = 0;

int insert(Node node, int ins) {
    if (cows[ins].num < node.num) {
        node.small++;
        if (node.l == -1) {
            node.l = ins;
            return cnt;
        }
        return insert(cows[node.l], ins);
    } else {
        cnt += node.small + 1;
        if (node.r == -1) {
            node.r = ins;
            return cnt;
        }
        return insert(cows[node.r], ins);
    }
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N;
    cows.resize(N);
    REP(i,0,N) {
        fin >> cows[i].num;
        cows[i].l = -1;
        cows[i].r = -1;
    }

    int rind = N - 2;
    int nsort = 1;
    int m = cows[N-1];
    while (cows[rind] < min) {
        min = cows[rind];
        rind--;
        nsort++;
    }

    int befval = rind;
    Node root;
    root.num = cows[rind];
    nsort++;
    rind--;

	return 0;
}
