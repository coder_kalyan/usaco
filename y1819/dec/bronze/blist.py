## TRY 2:
class Cow:
    def __init__(self, i, s, t, b):
        self.i = i
        self.s = s
        self.t = t
        self.b = b

cows = []
with open("blist.in") as f:
    N = int(f.readline())
    idx = 1
    for i in range(N):
        s, t, b = map(int, f.readline().split())
        cow = Cow(idx, s, t, b)
        print(cow)
        cows.append(cow)
        idx += 1

free = []
def alloc(b):
    used = []
    i = 0
    for _ in range(b):
        while i < len(free) and free[i] == False:
            i += 1
            print(i)
        if i < len(free):
            print(free[i])
            # we have found an unallocated bucket
            used.append(i)
            free[i] = False
        else:
            # we have run out of buckets, create more
            free.append(False)
            used.append(i)
        i += 1

    return used

def dealloc(blist):
    for b in blist:
        free[b] = True

max_time = 0
for cow in cows:
    max_time = max(max_time, cow.t)

cur_time = 1

milking = {}

while cur_time <= max_time:
    print(milking, free)
    for cow in cows:
        if cow.t == cur_time:
            dealloc(milking[cow.i])
            del milking[cow.i]
    for cow in cows:
        if cow.s == cur_time:
            tmp = alloc(cow.b)
            milking[cow.i] = tmp

    cur_time += 1

print(len(free))
with open("blist.out", "w") as f:
    f.write(str(len(free)))
    f.write("\n")

