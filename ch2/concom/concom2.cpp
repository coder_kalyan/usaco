/*
  ID: kalyan2
  LANG:C++11
  TASK: concom
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;

#define MAXCOM 100

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;

const string PROG = "concom";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

int N = 0;      // number of companies - we have to determine that ourselves
int own[MAXCOM][MAXCOM];
int ind[MAXCOM][MAXCOM];

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    int n, i, j;
    fin >> n;
    REP(x,0,n) {
        cin >> i >> j;
        cin >> own[i][j];
    REP(x,0,N) {
        REP(y,0,N) {
            if (own[x][y] > 50) {
                cout << x << " " << y << endl;
                fout << x << " " << y << endl;
            } else {
                int sum = 0;
                REP(z,0,N) {
                    if ((z==x)||(z==y))
                        continue;
                    if (own[x][z] > 50)
                // company x controls company y, so....
                // every company z's ownership percentage gets added to ind[x][z]
                ind[x][y] 
	return 0;
}
