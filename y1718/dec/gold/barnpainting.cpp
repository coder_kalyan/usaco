#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>
#include <climits>
#include <cmath>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define RREP(i,a,b) for (long i=b-1;i>=a;i--)
#define LOG(a,b) clog << a << ": " << b << endl;
#define MP(a,b) make_pair(a,b)
#define DIST(x1,y1,x2,y2) hypot(abs(x2-x1),abs(y2-y1))

class cmp;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;
typedef priority_queue<ii> pqiif;
typedef priority_queue<ii,vii,cmp> pqiis;
typedef vector<vi> vvi;
typedef vvi::iterator vvi_i;
typedef vector<vii> vvii;
typedef vvii::iterator vvii_i;

class cmp {
    bool operator() (const ii &a, const ii &b) {
        return a.second > b.second;
    }
};

#define INF INT_MAX
#define MAXN 10000
const long MOD = 10E9 + 7; //(long)pow(10,9) + 7.0;

// #define MOD (pow(10,9) + 7.0)

const string PROG = "barnpainting";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

int N, K;
int color[MAXN];
vi adj[MAXN];
int dp[MAXN][3];

long solve(int currV, int currC, int parV, int parC) {
    if (currC == parC || (color[currV] >= 0 && currC != color[currV]))
        return 0;
    if (dp[currV][currC] >= 0)
        return dp[currV][currC];

    dp[currV][currC] = 1;
    for(int nb: adj[currV]) {
        // cout << nb << endl;
        if (nb == parV) continue;
        int tmp = 0;
        REP(c,0,3) {
            tmp += solve(nb, c, currV, currC);
            tmp %= MOD;
        }
        dp[currV][currC] *= tmp;
        dp[currV][currC] %= MOD;
    }

    return dp[currV][currC];
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N >> K;
    // cout << N << K;
    REP(i,0,N)
        REP(j,0,3)
            dp[i][j] = -1;
    fill(color,color+N,-1);
    int a, b;
    REP(i,0,N-1) {
        fin >> a >> b;
        a--;
        b--;
        // cout << a << " " << b << endl;
        adj[a].push_back(b);
        adj[b].push_back(a);
    }
    REP(i,0,K) {
        fin >> a >> b;
        a--;
        b--;
        color[a] = b;
    }

    long ans = 0;
    REP(c,0,3) {
        ans += solve(0,c,-1,-1);
        // cout << solve(0,c,-1,-1) << endl;
    }
    ans %= MOD;

    cout << ans << endl;
    fout << ans << endl;
	return 0;
}
