/*
  ID: kalyan2
  LANG:C++11
  TASK: inflate
*/

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>
#include <climits>
#include <cmath>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define RREP(i,a,b) for (long i=b-1;i>=a;i--)
#define LOG(a,b) clog << a << ": " << b << endl;
#define MP(a,b) make_pair(a,b)
#define DIST(x1,y1,x2,y2) hypot(abs(x2-x1),abs(y2-y1))

#define DEBUG 1

#ifdef DEBUG
#define COUT(str) cout str;
#else
#define COUT(str) {}
#endif


class cmp;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;
typedef priority_queue<ii> pqiif;
typedef priority_queue<ii,vii,cmp> pqiis;
typedef vector<vi> vvi;
typedef vvi::iterator vvi_i;
typedef vector<vii> vvii;
typedef vvii::iterator vvii_i;

class cmp {
    bool operator() (const ii &a, const ii &b) {
        return a.second > b.second;
    }
};

#define INF INT_MAX
#define MAXN 10000

const string PROG = "inflate";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

int M, N;
int pt[MAXN+10], mi[MAXN+10];
map<long, long> dp;

long solve(long p, long r) {
    if (r == 0)
        return p;
    // ii lb = *(dp.lower_bound(r));
    // if (lb.first == r)
        // return p + lb.second;
    // if (dp.count(r) == 1)
        // return dp[r];

    // take the max of pulling any question
    long q = 0;
    REP(i, 0, N) {
        if (r - mi[i] < 0)
            continue;
        q = max(q, solve(p + pt[i], r - mi[i]));
    }

    // dp.insert(MP(r, q));
    // lb = (dp.lower_bound(r));
    // cout << q << " " << lb.second << endl;

    return q;
}

long solve2() {
    long m = 0, q, qi;
    dp.insert(MP(0, 0));
    while (m < M) {
        q = 0;
        REP(i, 0, N) {
            if (m + mi[i] > M)
                continue;
            if (dp[m] + pt[i] > q) {
                q = dp[m] + pt[i];
                qi = i;
            }
            // q = max(q, dp[m] + pt[i]);
        }
        dp.insert(MP(m + mi[qi], q));
        m += mi[qi];
        cout << m + mi[qi] << endl;
    }

    return dp[M];
}
// long KS(long *v, long *w, long n, long C) {
    // long res;
    // if (n == 0 || C == 0)
        // res = 0;
    // else if (w[n] > C)
        // res = KS(v, w, n - 1, C);
    // else {
        // long tmp1 = KS(v, w, n - 1, C);
        // long tmp2 = v[n] + KS(v, w, n - 1, C - w[n]);
        // res = max(tmp1, tmp2);
    // }
//
    // return res;
// }

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> M >> N;
    REP(i,0,N)
        fin >> pt[i] >> mi[i];

    // cout << N << M << endl;
    dp.clear();
    // fout << solve(0, M) << endl;
    cout << solve2() << endl;
    // cout << endl;
    // for (ii v : dp)
        // cout << v.first << " " << v.second << endl;
	return 0;
}
