"""
ID: kalyan2
LANG: PYTHON3
TASK: holstein
"""

with open("holstein.in") as f:
    V = int(f.readline())
    required = list(map(int, f.readline().split(" ")))
    G = int(f.readline())
    amounts = []
    for i in range(G):
        temp = f.readline().split(" ")
        amounts.append([])
        for n in temp:
            if n != "":
                amounts[-1].append(int(n))
    
print(V)
print(required)
print(G)
print(amounts)
