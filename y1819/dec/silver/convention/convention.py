with open("convention.in") as f:
    N, M, C = map(int, f.readline().split())
    TIMES = list(map(int, f.readline().split()))

TIMES = sorted(TIMES)

# print(f"N: {N}, M: {M}, C: {C}")
# print("TIMES:", TIMES)

def test(split_n):
    # print("->", TIMES[0])
    bus_s = TIMES[0]
    bus_si = 0
    bus_n = 1
    # bus_f = 1
    for i in range(1, len(TIMES)):
        # bus_f += 1
        # if bus_f >= C or TIMES[i] - bus_s > split_n:
        if (i +1 - bus_si > C) or TIMES[i] - bus_s > split_n:
            # print("=>", TIMES[i], "-", bus_s, "=", TIMES[i] - bus_s, "<>", split_n)
            # print("->", bus_f, C)
            bus_f = 0
            bus_n += 1
            # ret = max(ret, TIMES[i - 1] - bus_s)
            # print("->", TIMES[i], TIMES[i - 1] - bus_s)
            bus_s = TIMES[i]
            bus_si = i
            # print(bus_f)
        # else:
            # print(TIMES[i], bus_f)
        # if bus_n > M:
            # return -1

    # ret = max(ret, TIMES[i] - bus_s)
    # return ret
    return bus_n <= M

# ans = N
# def search():
    # # global ans
    # ans = N
    # for i in range(TIMES[-1] - TIMES[0], 0, -1):
        # ret = test(i)
        # # print(f"SPLIT_N: {i} -> TEST: {ret}")
        # # if ret != -1:
        # if not ret:
            # ans = i #min(ans, ret)

    # return ans

def bin_search(l, r):
    if l == r:
        return l
    if l + 1 == r:
        if test(l):
            return l
        return r
    mid = (l + r) // 2
    if test(mid):
        return bin_search(l, mid)
    else:
        return bin_search(mid + 1, r)
    # if r >= l:
        # mid = l + (r + l) // 2
        # ret = test(mid)
        # # print(f"SPLIT_N: {mid} -> TEST: {ret}")
        # if ret == -1:
            # # too low, try the upper half
            # bin_search(mid + 1, r)
        # else:
            # ans = min(ans, ret)
            # bin_search(l, mid - 1)

ans = bin_search(0, TIMES[-1] - TIMES[0])
print(ans)
with open("convention.out", "w") as f:
    f.write(str(ans))
    f.write("\n")
