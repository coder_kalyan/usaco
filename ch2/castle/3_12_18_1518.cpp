#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <cmath>

using namespace std;

int M, N;		// M is the number of columns in in the castle matrix, N is the number of rows

void floodFill(short *compArr, vector<short> *nodes, short compN)
{
	short numVisited = 0;
	while (true)
	{
		numVisited = 0;
		for (int i = 0;i < M * N;i++)
		{
			if (compArr[i] == -2)
			{
				numVisited++;
				compArr[i] = compN;
				for (int n = 0;n < nodes[i].size();n++)
				{
					if (compArr[nodes[i][n]]  == -100)
						compArr[nodes[i][n]] = -2;
				}
			}
		}

		// cout << numVisited << endl;
		if (numVisited == 0) {
			return;
		}
	}
}

short largestRoom(short *component, int ind, int numComp)
{
	short compSize[numComp];
	fill(compSize, compSize + numComp, 0);

	for (int i = 0;i < ind;i++)
		compSize[component[i]]++;

	short larRoom = *max_element(compSize, compSize + numComp);
	return larRoom;
}
short sepRooms(short *component, int ind, vector<short> *nodes)
{
	short numComp = 0;
	for (int i = 0;i < (ind);i++)
	{
		if (component[i] == -100)
		{
			numComp++;
			component[i] = -2;
			// cout << numComp << endl;
			floodFill(component, nodes, numComp);
		}
	}
	return numComp;
}
int main()
{
	ifstream fin;
	fin.open("castle.in");

	fin >> N >> M;

	// read in the matrix of cells
	string line;
	short cell;
	getline(fin, line);		// the first line contains N/M, which have already been recorded
	short cells[M][N]; 		// for now, just read into a matrix
	for (int j = 0;j < M;j++)
	{
		getline(fin, line);
		istringstream iss(line);
		for (int i = 0;i < N;i++)
		{
			iss >> cell;
			cells[j][i] = cell;
			// cout << cells[j][i] << " ";
		}
		// cout << endl;
	}

	// ok so now that we have read the file into a matrix,
	// we need to parse that matrix into our graph
	// let's try to use an adjacency list -> each node keeps track of its adjacent nodes
	vector<short> nodes[M * N];
	int ind = 0;
	for (int j = 0;j < M;j++)
	{
		for (int i = 0;i < N;i++)
		{	
			// neighbors are in order south, east, north, west

			// south
			if (!(cells[j][i] & (1 << 3)))
			{
				nodes[ind].push_back(ind + N);
			}
			// east
			if (!(cells[j][i] & (1 << 2)))
			{
				nodes[ind].push_back(ind + 1);
			}
			// north
			if (!(cells[j][i] & (1 << 1)))
			{
				nodes[ind].push_back(ind - N);
			}
			// west
			if (!(cells[j][i] & (1 << 0)))
			{
				nodes[ind].push_back(ind - 1);
			}
			
			ind++;
		}
		// cout << endl;
	}

	// for (int j = 0;j < N * M;j++)
	// {
	// 	cout << j << ": ";
	// 	for(int i = 0;i < nodes[j].size();i++)
	// 	{
	// 		cout << nodes[j][i] << " ";
	// 	}
	// 	cout << endl;
	// }

	short component[ind];
	fill(component, component + ind + 1, -100);

	// first we need to seperate the rooms
	/*short numComp = 0;
	for (int i = 0;i < (ind);i++)
	{
		if (component[i] == -100)
		{
			numComp++;
			component[i] = -2;
			// cout << numComp << endl;
			floodFill(component, nodes, numComp);
		}
	}*/
	short numComp = sepRooms(component, ind, nodes);
	// now to find the largest room
	// short compSize[numComp];
	// fill(compSize, compSize + numComp, 0);

	// for (int i = 0;i < ind;i++)
	// 	compSize[component[i]]++;

	// short larRoom = *max_element(compSize, compSize + numComp);
	short larRoom = largestRoom(component, ind, numComp);
	// now we need to figure out the best ONE wall to break
	// v1: let's try to do this the brute force way - test every wall that exists
	short larBreak = 0;
	short larX, larY;
	char larDir;
	int ind2 = 0;
	for (int j = 0;j < M;j++)
	{
		for (int i = 0;i < N;i++)
		{	
			// neighbors are in order south, east, north, west

			// south
			if (!(cells[j][i] & (1 << 3)))
			{
				// remove the wall
				cells[j][i] &= ~(1 << 3);
				// test...
				fill(component, component + ind + 1, -100);
				short tempNumComp = sepRooms(component, ind, nodes);
				short tempLar = largestRoom(component, ind, tempNumComp);
				if(tempLar > larBreak)
				{
					larBreak = tempLar;
					larX = i;
					larY = j;
					larDir = 'S';
				}
				// and reset
				cells[j][i] |= (1 << 3);
			}
			// east
			if (!(cells[j][i] & (1 << 2)))
			{
				// remove the wall
				cells[j][i] &= ~(1 << 2);
				// test...
				fill(component, component + ind + 1, -100);
				short tempNumComp = sepRooms(component, ind, nodes);
				short tempLar = largestRoom(component, ind, tempNumComp);
				if(tempLar > larBreak)
				{
					larBreak = tempLar;
					larX = i;
					larY = j;
					larDir = 'E';
				}
				// and rese
				cells[j][i] |= (1 << 2);
			}
			// north
			if (!(cells[j][i] & (1 << 1)))
			{
				// cout << "Found north wall!" << endl;
				// remove the wall
				cells[j][i] &= ~(1 << 1);
				// test...
				fill(component, component + ind + 1, -100);
				short tempNumComp = sepRooms(component, ind, nodes);
				short tempLar = largestRoom(component, ind, tempNumComp);
				if(tempLar > larBreak)
				{
					larBreak = tempLar;
					larX = i;
					larY = j;
					larDir = 'N';
				}
				// and reset
				cells[j][i] |= (1 << 1);
			}
			// west
			if (!(cells[j][i] & (1 << 0)))
			{
				// cout << "Found west wall!" << endl;
				// remove the wall
				cells[j][i] &= ~(1 << 0);
				// test...
				fill(component, component + ind + 1, -100);
				short tempNumComp = sepRooms(component, ind, nodes);
				short tempLar = largestRoom(component, ind, tempNumComp);
				if(tempLar > larBreak)
				{
					larBreak = tempLar;
					larX = i;
					larY = j;
					larDir = 'W';
				}
				// and reset
				cells[j][i] |= (1 << 0);
			}
			
			ind2++;
			cout << ind2 << endl;
		}
		// cout << endl;
	}


	ofstream fout;
	fout.open("castle.out");

	cout << numComp << endl;
	fout << numComp << endl;

	cout << larRoom << endl;
	fout << larRoom << endl;
	cout << larBreak << endl;
	cout << larX << " " << larY << " " << larDir << endl;
	// for (int i = 0;i < (ind);i++)
	// {
	// 	cout << i << ": " << component[i] << endl;
	// }
	return 0;
}