#include <stdio.h>

void change(int *i);

void main(void) {
    int k;

    k = 5;
    printf("%d\n",k);
    change(&k);
    printf("%d\n",k);
}

void change(int *i) {
    int j;
    j = *i;
    *i = -j;
}
