import timeit

p = [0, 1, 5, 8, 9, 10, 17, 17, 20, 24, 30]

def cut_rod_1(n):
    if n == 0:
        return 0

    q = 0
    for i in range(1, n + 1):
        q = max(q, p[i] + cut_rod_1(n - i))

    return q

r = [0] * 10
def cut_rod_2(n):
    if n == 0:
        return 0
    if r[n] > 0:
        return r[n]
    q = 0
    for i in range(1, n + 1):
        q = max(q, p[i] + cut_rod_1(n - i))

    r[n] = q
    return q

print(timeit.timeit(lambda: cut_rod_1(6), number=100))
print(timeit.timeit(lambda: cut_rod_2(6), number=100))
