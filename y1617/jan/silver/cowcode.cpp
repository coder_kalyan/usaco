#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;
#define ROT(i,l) (i-1+l)%(l)

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;

const string PROG = "cowcode";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

ll N;
string S;
int lenS;

char solve(ll pos, ll power) {
    if (power == lenS) {
        // return the corresponding character
        // from base string
        return S[pos];
    }

    ll halfpow = power / 2;
    if (pos < halfpow) {
        // we don't have to rotate at this step
        // just return specified character of
        // the previous substr
        return solve(pos,halfpow);
    } else {
        // we will have to rotate at this step
        // request the rotated index of the
        // previous substr
        return solve(ROT(pos-halfpow,halfpow),halfpow);
    }
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> S >> N;
    lenS = S.size();

    ll power = lenS;
    while (power < N) power *= 2;
    clog << power << endl;

    char ans = solve(N-1, power);
    cout << ans << endl;
    fout << ans << endl;
	return 0;
}
