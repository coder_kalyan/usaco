#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)

#define MAXQ 5000
#define MAXN 5000
#define MAXK 1000000000

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;

const string PROG = "mootube";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N, Q;
vii edges[MAXN];

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N >> Q;
    long pi, qi, ri;
    REP(i,0,N-1) {
        fin >> pi >> qi >> ri;
        clog << pi << " " << qi << " " << ri << endl;
        edges[pi-1].push_back(make_pair(qi, ri));
        edges[qi-1].push_back(make_pair(pi, ri));
    };
    clog << endl;

    long ki, vi, ans;
    queue<long> todo;
    bitset<MAXN> seen;
    
    REP(q,0,Q) {
        fin >> ki >> vi;
        seen.reset();
        seen[vi] = true;
        ans = 0;
        todo.push(vi);
        while(todo.size()>0) {
            long cur = todo.front();
            todo.pop();
            for (auto& edge : edges[cur]) {
                // clog << edge.first << endl;
                if (!seen[edge.first] && edge.second>=ki) {
                    seen[edge.first] = true;
                    todo.push(edge.first);
                    clog << edge.first << endl;
                    ans++;
                }
            }
        }
        cout << ans << endl;
        fout << ans << endl;
    }

	return 0;
}
