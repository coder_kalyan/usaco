#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;

/**
	Generates combinations recursively of given size from list of bisquares
	@param step counts down from combinations of size N to 0
	@param firstAvail (N - step)
	@first the first element in a found sequence, only necessary for cataloging progs
	@diff the difference of the currently built progression, to check against
	@prev the parent bisquare, to check/calculate diff
**/
void gen_combs(int step, int firstAvail, int first, int diff, int prev);

short N, M;
vi bisquares;
vii progs;
vector<bool> mask;

// find all sequences of length 2 - this is simply (M+1)^2 choose 2, since any
// two numbers will be a sequence
// then for each of those, see if a third number is available
// continue this process until 

int main() {
	ifstream fin;
	fin.open("ariprog.in");
	
	fin >> N;
	fin >> M;

	// cout << N << endl << M << endl;

	for(int i = 0;i <= M;i++) {
		for(int j = i;j <= M;j++) {
			bisquares.push_back(i * i + j * j);
		}
	}

	sort(bisquares.begin(), bisquares.end());
	bisquares.erase(unique(bisquares.begin(), bisquares.end()), bisquares.end());
	
	cout << "Finished generating bisquares" << endl;

	// generate and store an exclusive OR at this point
	int xorval = bisquares[0];
	for (int i = 1;i < bisquares.size(); i++)
		xorval = xorval ^ bisquares[i];

	cout << "my xorval is " << xorval << endl;
	// for(int i = 0;i < bisquares.size();i++) {
	// 	cout << bisquares[i] << " ";
	// }
	// cout << endl;

	// now we have to find all the valid combinations of N elements from
	// the list of bisquares. Note: this will be at most (M*M) Choose N
	// but we are building the generator recursively to automatically filter
	// out non-sequences while generating, even before it finishes
	// generating a set

	// for(int i = 0;i < bisquares.size()-1;i++) {
	// 	gen_combs(N-1, i+1, bisquares[i], -1, bisquares[i]);
	// }

	// gen_combs(N, 0, -1, -1, -1);

	// generate all sequences of length 2
	for(int i = 0;i < bisquares.size()-2;i++) {
		for(int j = i+1;j < bisquares.size()-1;j++) {
			// i, j here is the choose 2 set
			// progs.push_back(make_pair(diff, first));
			// we will save in the format difference, first
			progs.push_back(make_pair(bisquares[j]-bisquares[i], bisquares[i]));
			mask.push_back(true);
		}
	}

	// for(int i = 0;i < progs.size();i++) {
	// 	printf("%d %d\n", progs[i].second, progs[i].first);
	// }
	// now for each of these sequences, see if the next element exists in the set
	int seqlen = 2;
	while (seqlen < N) {
		int foundone = 0;
		for (int i = 0; i < progs.size(); i++) {
			if(mask[i]) {
				int thisFound = 0;
				int nextelem = progs[i].second + seqlen*progs[i].first;
				// bisquares is a sorted array, remember

				//try the magic here
				// if ( (xorval ^ nextelem) == 0) {
				// 		thisFound = 1;
				// 		foundone++;

				// }

				// int k = 0;
				// while (bisquares[k] <= nextelem) {
				// 	if (bisquares[k] == nextelem){
				// 		thisFound = 1;
				// 		foundone++;
				// 	}
				// 	k++;
				// }
				for (int k = 0; k < bisquares.size(); k++) {
					if (nextelem == bisquares[k]) {
						thisFound = 1;
						foundone++;
						break;
					}
					/*else if (bisquares[k] > nextelem) {
						break;
					}*/
				}
				/*if(find(bisquares.begin(),bisquares.end(),nextelem) != bisquares.end()) {
					thisFound = 1;
					foundone++;
				}*/
				if(thisFound == 0) {
					// this sequence is not valid, remove it
	    			// progs.erase(progs.begin() + i);
	    			mask[i] = false;
				}
			}
		}
		// if we found at least one sequence still in the running, we continue
		if (foundone > 0)
			seqlen++;
		else
			break;
	}

	// at this point, if we have at least one valid sequence, seqlen should be N
	if (seqlen == N) {
		// cout << "Generated combinations" << endl;
		// sort(progs.begin(), progs.end());
		// cout << "Sorted combinations" << endl;
		// progs.erase(unique(progs.begin(), progs.end()), progs.end());
		
		vii filtered;
		for(int i = 0;i < progs.size();i++) {
			if(mask[i])
				filtered.push_back(progs[i]);
		}
		sort(filtered.begin(), filtered.end());
		for(int i = 0;i < filtered.size();i++) {
			printf("%d %d\n", filtered[i].second, filtered[i].first);
		}
	}

	return 0; 
}