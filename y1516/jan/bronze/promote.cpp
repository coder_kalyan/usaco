#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>

using namespace std;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const string PROG = "promote";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

int main() {
	ifstream fin (FIN.c_str());
    long bb,ba,sb,sa,gb,ga,pb,pa;
    fin >> bb >> ba >> sb >> sa >> gb >> ga >> pb >> pa;
    long bs,sg,gp;
    gp = pa - pb;
    sg = ga + pa - gb - pb;
    bs = sa + ga + pa - sb - gb - pb;

	ofstream fout (FOUT.c_str());
    fout << bs << endl << sg << endl << gp << endl;
    cout << bs << endl << sg << endl << gp << endl;

	return 0;
}
