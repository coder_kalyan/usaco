"""
ID: kalyan2
LANG: PYTHON3
TASK: dualpal
"""

def is_palindrome(s):
	return s == s[::-1]

chars = [str(x) for x in range(0, 10)]
chars.extend([chr(x) for x in range(65, 75)])
def convert_base(n, base):
	left = n
	converted = ""
	while left >= base:
		converted  = chars[left % base] + converted
		left //= base
	converted = chars[left] + converted
	return converted

def pal_2_bases(n):
	# returns true if number n is a palindrome in at least 2 bases from 2 to 10
	numPalindromic = 0
	for i in range(2, 11):
		if is_palindrome(convert_base(n, i)):
			numPalindromic += 1
		if numPalindromic >= 2:
			return True
	return False


with open("dualpal.in", "r") as f:
	N, S = f.readline().replace("\n", "").split(" ")
	N = int(N)
	S = int(S)

S += 1
numFound = 0
nums = []
while numFound < N:
	if pal_2_bases(S):
		numFound += 1
		nums.append(S)
	S += 1

nums = map(str, nums)
print(nums)

# l = [[x,x**2] for x in range(1, 301) if is_palindrome(convert_base(x**2, base))]
with open("dualpal.out", "w") as f:
	f.write("\n".join(nums))
	f.write("\n")