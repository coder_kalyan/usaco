/*
  ID: kalyan2
  LANG:C++11
  TASK: humble
*/

#include <bits/stdc++.h>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define RREP(i,a,b) for (long i=b-1;i>=a;i--)
#define LOG(a,b) clog << a << ": " << b << endl;
#define MP(a,b) make_pair(a,b)
#define DIST(x1,y1,x2,y2) hypot(abs(x2-x1),abs(y2-y1))

#define DEBUG 1

#ifdef DEBUG
#define COUT(str) cout str;
#else
#define COUT(str) {}
#endif


class cmp;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;
typedef priority_queue<ii> pqiif;
typedef priority_queue<ii,vii,cmp> pqiis;
typedef vector<vi> vvi;
typedef vvi::iterator vvi_i;
typedef vector<vii> vvii;
typedef vvii::iterator vvii_i;

class cmp {
    bool operator() (const ii &a, const ii &b) {
        return a.second > b.second;
    }
};

#define INF INT_MAX

const string PROG = "humble";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long K, N;
long S[100];
long prev = 0;
pqi humble;

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> K >> N;
    REP(i,0,K) fin >> S[i];
    REP(i,0,K) cout << S[i] << " ";
    cout << endl;
    REP(i,0,N/K + 1) {
        REP(j,0,K) {
            humble.push(
        }
    }
    REP(i,0,N - 1)
        humble.pop();
    cout << humble.top();
    cout << "FABCD
	return 0;
}
