#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;

#define NMAX 100000

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;

const string PROG = "hps";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N;
char pred[NMAX];
long fwd[NMAX], bwd[NMAX], sum[NMAX];

char winMove(char fj) {
    if (fj == 'H')
        return 'P';
    else if (fj == 'P')
            return 'S';
    else
        return 'H';
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N;
    char tmp;
    REP(i,0,N) {
        fin >> tmp;
        pred[i] = winMove(tmp);
        cout << pred[i] << " ";
    }
    cout << endl;

    long h = 0,p = 0,s = 0;
    REP(i,0,N) {
        if (pred[i] == 'H')
            h++;
        else if (pred[i] == 'P')
            p++;
        else
            s++;
        fwd[i] = max(h,p);
        fwd[i] = max(fwd[i],s);
    }
    h = 0;
    p = 0;
    s = 0;
    for(long i=N-1;i>=0;i--) {
        if (pred[i] == 'H')
            h++;
        else if (pred[i] == 'P')
            p++;
        else
            s++;
        bwd[i] = max(h,p);
        bwd[i] = max(bwd[i],s);
    }

    REP(i,0,N) {
        sum[i] = fwd[i] + bwd[i];
        cout << fwd[i] << " " << bwd[i] << endl;
    }


    long ans = *max_element(sum, sum+N);
    if (fwd[N-1] == N) {
        cout << N << endl;
        fout << N << endl;
    } else {
        cout << ans << endl;
        fout << ans << endl;
    }
	return 0;
}
