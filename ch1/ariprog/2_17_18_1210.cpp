/*
ID: kalyan2
TASK: ariprog
LANG: C++
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;

short N, M;
int bisquares[63002];
vii progs;
int myind = 0;

int mybinarySearch2(int arr[], int l, int r, int x)
{
	while (r >= l){
        int mid = l + (r - l)/2; 
        if (arr[mid] == x)  
            return mid;
        if (arr[mid] > x)
        	r = mid - 1;
        else
        	l = mid+1;
	}
   // We reach here when element is not 
   // present in array
   return -1;
}


int main() {
	ifstream fin;
	fin.open("ariprog.in");
	
	fin >> N;
	fin >> M;

	for(int i = M;i >=0;i--) {
		for(int j = i;j >=0;j--) {
			int mytemp = (i*i) + (j*j);
			// add to array only if not duplicate
			int inarray = 0;
			for (int k =0; k < myind; k++) {
				if (bisquares[k] == mytemp) {
					inarray = 1;
					break;
				}
			}
			if (!inarray)
				bisquares[myind++] = mytemp;				
		}
	}
	sort(&bisquares[0], &bisquares[myind]);
	
	// cout << "Finished generating bisquares" << endl;

	// for(int i = 0;i < myind;i++) {
	// 	cout << bisquares[i] << " ";
	// }
	// cout << endl;

	// generate all sequences of length 2
	// and then check if the rest of the sequence exists
	// sequences are N in length, so you can stop earlier
	for(int i = 0;i < myind-N+1;i++) {
		for(int j = i+1;j < myind-N+2;j++) {
			int wherelastfound = myind;
			int wherefound;
			bool isSequence = true;
			int seqlen = N - 1;
			int mydiff = bisquares[j] - bisquares[i];
			// how long can this sequence be?
			int thisN = (bisquares[myind-1]-bisquares[j])/mydiff;
			// if it can't be long enough, just bail
			if (thisN < (N-2))
				break;
			while (seqlen > 1) {
				int nextelem = bisquares[i] + seqlen*(mydiff);
				// if the element takes you beyond the edge of the data set, you can bail
				if (nextelem > bisquares[myind-1]) {
					isSequence = false;
					break;
				}
				// is the next element a valid bisquare?
				// if(!binary_search(&bisquares[0],&bisquares[myind],nextelem))
				wherefound = mybinarySearch2(bisquares,j+1,wherelastfound-1,nextelem);
				if(wherefound<0)
				{
					isSequence = false;
					break;
				}
				else {
					seqlen--;
					wherelastfound = wherefound;					
				}
			} 
			// if this is a valid sequence, push it in
			if (isSequence)
				progs.push_back(make_pair(bisquares[j]-bisquares[i], bisquares[i]));
		}
	}
	// cout << "Finished generating sequences" << endl;
	
	sort(progs.begin(), progs.end());
	// cout << "Sorted" << endl;

	FILE* fout;
	fout = fopen("ariprog.out", "w");
	if(progs.size() == 0) {
		fprintf(fout, "NONE\n");
	} else {
		for(int i = 0;i < progs.size();i++) {
			printf("%d %d\n", progs[i].second, progs[i].first);
			fprintf(fout,"%d %d\n", progs[i].second, progs[i].first);
		}
	}
	fclose(fout);

	return 0; 
}