with open("shell.in") as f:
    N = int(f.readline())

    steps = []
    for i in range(N):
        steps.append(list(map(int, f.readline().split())))
        steps[-1][0] -= 1
        steps[-1][1] -= 1
        steps[-1][2] -= 1

def sim(init):
    pos = [0, 0, 0]
    pos[init] = 1
    score = 0
    for step in steps:
        pos[step[0]], pos[step[1]] = pos[step[1]], pos[step[0]]
        if pos[step[2]] == 1:
            score += 1

    return score

ans = max([sim(i) for i in range(3)])
print(ans)
with open("shell.out", "w") as f:
    f.write(str(ans) + "\n")
