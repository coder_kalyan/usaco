"""
ID: kalyan2
TASK: skidesign
LANG: PYTHON3
"""
from math import floor, ceil
hills = []
with open("skidesign.in") as f:
    N = int(f.readline())
    for i in range(N):
        hills.append(int(f.readline()))

money = 0

hills.sort()
print(hills)
print()
"""
while max(hills) - min(hills) > 17:
    ma = max(hills)
    mi = min(hills)
    diff = ma - mi - 17
    hills[hills.index(ma)] -= floor(diff/2)
    hills[hills.index(mi)] += ceil(diff/2)
    to_add = (floor(diff/2) ** 2) + (ceil(diff/2) ** 2)
    hills.sort()
    # print(hills)
    print(ma, mi, diff/2, to_add)
    money += to_add
"""

hills.sort()

ma = max(hills)
mi = min(hills)
diff = ma - mi - 17
baseMin = mi + ceil(diff/2)
print(baseMin)
i = 0
while hills[i] < baseMin:
    change = baseMin - hills[i]
    # hills[i] += ceil(diff / 2)
    hills[i] = baseMin
    money += change ** 2
    i += 1
    
hills.sort()
baseMax = ma - floor(diff/2)
i = len(hills) - 1
# print(i)
while hills[i] > baseMax:
    change = hills[i] - baseMax
    # hills[i] += ceil(diff / 2)
    hills[i] = baseMax
    money += change ** 2
    # hills[i] -= floor(diff / 2)
    # money += floor(diff / 2) ** 2
    i -= 1
    

hills.sort()
print(hills)
print("Money:",money)

with open("skidesign.out", "w") as f:
    f.write(str(money))
    f.write("\n")
