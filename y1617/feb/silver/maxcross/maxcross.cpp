#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

#define MAXN 100000
int N, K, B, working[MAXN];
int main() {
    ifstream fin("maxcross.in");
    ofstream fout("maxcross.out");

    fin >> N >> K >> B;
    fill(working, working + N, 1);
    int id;
    for (int i = 0;i < B;i++) {
        fin >> id;
        working[id-1] = 0;
    }
    
    int nbroke = 0, ans = 0;
    for (int i = 0;i < K;i++)
        if (!working[i])
            nbroke++;
    ans = nbroke;
    //cout << nbroke << endl;
    for (int i = 1;i < (N - K + 1);i++) {
        if (!working[i-1])
            nbroke--;
        if (!working[i+K - 1])
            nbroke++;
        //cout << nbroke << endl;
        ans = min(ans, nbroke);
    }

    cout << ans << endl;
    fout << ans << endl;

    return 0;
}
