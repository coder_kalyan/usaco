"""
ID: kalyan2
LANG: PYTHON3
TASK: milk2
"""

events = []

with open("milk2.in") as f:
    N = int(f.readline())
    for i in range(N):
        tmp1, tmp2 = map(int, f.readline().split())
        events.append((tmp1,0,i))
        events.append((tmp2,1,i))

events.sort()
print(events)

max_milked = 0
max_none = 0

cur = []
start_milk = -1
stop_milk = -1
for event in events:
    if event[1]:
        cur.remove(event[2])
        print("Removed",event[2],"from cur")
    else:
        cur.append(event[2])
        print("Added",event[2],"to cur")
    if len(cur) >= 1 and start_milk == -1:
        # previously no one milking, now at least 1 milking
        start_milk = event[0]
        if stop_milk != -1:
            print(event[0] - stop_milk)
            max_none = max(max_none, event[0] - stop_milk)
            stop_milk = -1
    elif len(cur) == 0 and stop_milk == -1:
        # previously someone milking, now no one milking
        stop_milk = event[0]
        max_milked = max(max_milked, event[0] - start_milk)
        start_milk = -1

print(max_milked, max_none)
with open("milk2.out", "w") as f:
    f.write(str(max_milked) + " " + str(max_none) + "\n")
