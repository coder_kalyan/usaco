/*
ID: kalyan2
TASK: ariprog
LANG: C++
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>
// #include <tuple>

using namespace std;

typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;

int maxA, maxB, maxC;
int curA, curB, curC;
vector< vector<int> > movesDone;

void simPour(char sourceName, char destName, int a, int b, int c)
{
	// simulates moving milk from source to destination

	if(source == 0)
		return;
	if(dest == destMax)
		return;
	// int temp = max(source - dest, 0);
	// source = temp;
	// dest += 

	while(dest > 0 && source < sourceMax) {
		dest++;
		source--;
	}

	vector<int> temp;
	if(sourceName == 'a')
		temp.push_back(source);
	else if(destName == 'a')
		temp.push_back(dest);
	else
		temp.push_back(curA);

	if(sourceName == 'b')
		temp.push_back(source);
	else if(destName == 'b')
		temp.push_back(dest);
	else
		temp.push_back(curB);

	if(sourceName == 'c')
		temp.push_back(source);
	else if(destName == 'c')
		temp.push_back(dest);
	else
		temp.push_back(curC);
	
	for(int i = 0;i < movesDone.size();i++) {
		// vector<int> *temp2 = movesDone[i];
		if((movesDone[i][0] == temp[0]) && (movesDone[i][1] == temp[1]) && (movesDone[i][2] == temp[2]))
			return;
	}

	movesDone.push_back(temp);

	simPour('a', 'b', curA, curB, maxA, maxB);
	simPour('a', 'c', curA, curC, maxA, maxC);
	simPour('b', 'a', curB, curA, maxB, maxA);
	simPour('b', 'c', curB, curC, maxB, maxC);
	simPour('c', 'a', curC, curA, maxC, maxA);
	simPour('c', 'b', curC, curB, maxC, maxB);
}

int main()
{
	ifstream fin;
	fin.open("milk3.in");
	
	ofstream fout;
	fout.open("milk3.out");

	while(fin >> maxA >> maxB >> maxC) {
		// cout << maxA << maxB << maxC << endl;
		
		curA = 0;
		curB = 0;
		curC = maxC;

		simPour('c', 'a', curC, curA, maxC, maxA);
		simPour('c', 'b', curC, curB, maxC, maxB);

		for(int i = 0;i < movesDone.size();i++) {
			// tuple<int, int, int> *temp = movesDone[i];
			// vector<int> *temp = &(movesDone[i]);
			// if(get<0>(temp) == 0) {
			// 	cout << get<0>(temp) << get<1>(temp) << get<2>(temp) << endl;
			// 	fout << get<0>(temp) << get<1>(temp) << get<2>(temp) << endl;
			// }
			//if(movesDone[i][0] == 0) {
				cout << movesDone[i][0] << movesDone[i][1] << movesDone[i][2] << endl;
				// fout << *(temp)[0] << *(temp)[1] << *(temp)[2] << endl;
			//}
		}

		fout << endl;

		movesDone.clear();
	}

	return 0; 
}