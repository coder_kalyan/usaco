#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>

using namespace std;

int N;
int main() {
	ifstream fin;
	fin.open("skidesign.in");


	fin >> N;
	int hills[N];
	for(int i = 0;i < N;i++) {
		fin >> hills[i];
	}

	sort(hills, hills + N);
	
	// for(int i = 0;i < N;i++) {
	// 	cout << hills[i] << endl;
	// }
	
	float deltaE = hills[N - 1] - hills[0] - 17; // the amount of delta we have to remove
	if(deltaE <= 0) {
		// we don't have to remove anything - just print cost zero
		cout << "Final cost: " << deltaE << endl;
	} else {
		// we need to do some trimming/raising
		int thresMin = hills[0] + ceil(deltaE / 2);
		int thresMax = hills[N - 1] - floor(deltaE / 2);
		cout << deltaE << endl;
		cout << ceil(deltaE / 2) << " " << floor(deltaE / 2) << endl;
		cout << thresMin << " " << thresMax << endl << endl;
		int cost = 0;
		for(int i = 0;i < N;i++) {
			if(hills[i] < thresMin) {
				cost += pow(thresMin - hills[i], 2.0);
				hills[i] = thresMin;
			} else if(hills[i] > thresMax) {
				cost += pow(hills[i] - thresMax, 2.0);
				hills[i] = thresMax;
			}
			cout << hills[i] << endl;
		}

		cout << "Final cost: " << cost << endl;
	}


	return 0;
}