#include <iostream>
#include <fstream>
#include <queue>
#include <vector>
#include <climits>
#include <algorithm>

using namespace std;

int N, M, K;

int YUM[50001];
vector<pair<long,long> > adj[50001];
long dist[50001], orig[50001], vis[50001];

void dijkstra(long src) {
	fill(dist, dist + N, INT_MAX);
	fill(vis, vis + N, false);
	priority_queue<pair<long,long>> pq;
	pq.push(make_pair(dist[src] = 0, src));
	while (!pq.empty()) {
		auto curr = pq.top();
		pq.pop();
		int cw = curr.first, cv = curr.second;
		if (vis[cv])
			continue;
		vis[cv] = true;
		for (auto nb : adj[cv]) {
			if (!vis[nb.second] && nb.first+cw<dist[nb.second])
				pq.push(make_pair(nb.second,dist[nb.second]=nb.first+cw));
		}
	}
}

int main() {
	ifstream fin("dining.in");
	fin >> N >> M >> K;

	int a, b, t;
	for (int i = 0;i < M;i++) {
		fin >> a >> b >> t;
		adj[a].push_back(make_pair(a, t));
		adj[b].push_back(make_pair(b, t));
	}

	for (int i = 0;i < K;i++) {
		fin >> a >> b;
		YUM[a] = b;
	}

	dijkstra(N);
	copy(dist, dist + N, orig);

	/*for (int i = 0;i < adj[N].size();i++) {
		if (YUM[adj[N][i].first] > 0) {
			adj[N][i].second -= YUM[adj[N][i].first];
		} else {
			adj[N].erase(adj[N].begin() + i);
		}
	}

	dijkstra(N);*/

	for (int i = 0;i < N - 1;i++) {
		cout << orig[i] << " " << dist[i] << endl;
	}
	
	return 0;
}
