from timeit import timeit

items = [(1,2), (5,4), (8,10), (6,3), (4,2), (8, 3), (5, 3), (10, 1), (7, 2)]
maxw = 23

def solve(i, w, v):
    if i == 0 or w > maxw:
        return v #items[i]#[0], items[i][1]

    a = solve(i - 1, w + items[i][1], v + items[i][0])
    b = solve(i - 1, w, v)
    return max(a, b)


print(solve(len(items) - 1, 0, 0))
print(timeit(lambda:solve(len(items)-1,0,0), number=10000))
