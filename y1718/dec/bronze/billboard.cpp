#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>

using namespace std;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const string PROG = "billboard";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

int overlap(int a, int b, int h, int k) {
    // a, b is start and end of billboard
    // h, k is start and end of truck
    if (h >= a) {
        // truck started after billboard
        cout << (h >= b) << endl;
        if (h >= b)
            return 0;
        else if (k >= b)
            return b - h;
        else
            return k - h;
    } else {
        if (k <= a)
            return 0;
        else if (k <= b)
            return k - a;
        else
            return b - a;
    }
}

int main() {
	ifstream fin (FIN.c_str());
    int x1,y1,x2,y2;
    int x3,y3,x4,y4;
    int x5,y5,x6,y6;
    fin >> x1 >> y1 >> x2 >> y2 >> x3 >> y3 >> x4 >> y4 >> x5 >> y5 >> x6 >> y6;
    int oxa, oya, oxb, oyb;
    oxa = overlap(x1,x2,x5,x6);
    oya = overlap(y1,y2,y5,y6);
    oxb = overlap(x3,x4,x5,x6);
    oyb = overlap(y3,y4,y5,y6);
    int areaA = (x2 - x1) * (y2 - y1);
    int areaB = (x4 - x3) * (y4 - y3);
    int truckA = oxa * oya;
    int truckB = oxb * oyb;
    int overA = areaA - truckA;
    int overB = areaB - truckB;

    cout << overA << " " << overB << endl;

	ofstream fout (FOUT.c_str());
    fout << (overA + overB) << endl;

	return 0;
}
