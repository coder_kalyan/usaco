#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>
#include <climits>
#include <cmath>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define RREP(i,a,b) for (long i=b-1;i>=a;i--)
#define LOG(a,b) clog << a << ": " << b << endl;
#define MP(a,b) make_pair(a,b)
#define DIST(x1,y1,x2,y2) hypot(abs(x2-x1),abs(y2-y1))

class cmp;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;
typedef priority_queue<ii> pqiif;
typedef priority_queue<ii,vii,cmp> pqiis;
typedef vector<vi> vvi;
typedef vvi::iterator vvi_i;
typedef vector<vii> vvii;
typedef vvii::iterator vvii_i;

class cmp {
    bool operator() (const ii &a, const ii &b) {
        return a.second > b.second;
    }
};

bool CmpHeight(ii a, ii b) {
    return a.first > b.first;
}

#define INF INT_MAX

#define MAXN 100000

const string PROG = "bphoto";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N;
vii h;
long tree[MAXN];

void fenwick_update(long *tree, long N, long i, long delta) {
    // i++;
    // while (i < N) {
        // tree[i] += delta;
        // i += i & -i;
    // }
    for(;i<N;i|=i+1) {
        // cout << i << " ";
        tree[i] += delta;
    }
    // cout << endl;
}

long fenwick_query(long *tree, long i) {
    // long sum = 0;
    // i++;
    // while (i > 0) {
        // sum += tree[i];
        // i -= i & -i;
    // }
    long sum = 0;
    while (i >= 0) {
        sum += tree[i];
        i &= i + 1;
        i--;
    }
    return sum;
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N;
    h.resize(N);
    REP(i,0,N) {
        fin >> h[i].first;
        h[i].second = i;
    }

    sort(h.begin(),h.end(),CmpHeight);
    long ans = 0;
    long seen = 0;
    
    fill(tree,tree+N,0);
    for (ii height : h) {
        long lhs = fenwick_query(tree, height.second);
        long rhs = seen - lhs;
        // cout << lhs << " " << rhs << endl;
        // REP(j,0,N)
            // cout << tree[j] << " ";
        // cout << endl;
        if (max(lhs,rhs) > 2*min(lhs,rhs))
            ans++;
        fenwick_update(tree, N, height.second,1);
        seen++;
    }

    cout << ans << endl;
    fout << ans << endl;
	return 0;
}
