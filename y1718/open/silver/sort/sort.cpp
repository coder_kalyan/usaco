#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

#define MAXN 100000
int N;
pair<int, int> A[MAXN];

int main() {
    ifstream fin("sort.in");
    ofstream fout("sort.out");

    fin >> N;
    for (int i = 0;i < N;i++) {
        fin >> A[i].first;
        A[i].second = i;
    }

    sort(A, A + N);

    int ans = 0;
    for (int i = 0;i < N;i++)
        ans = max(ans, A[i].second - i);
    ans++;

    cout << ans << endl;
    fout << ans << endl;

    return 0;
}
