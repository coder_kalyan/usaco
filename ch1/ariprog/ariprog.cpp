/*
ID: kalyan2
TASK: ariprog
LANG: C++
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <utility>

using namespace std;

int N, M;

vector<int> bisquares;
int twom2 = 0;

bool search(int n1, int n2) {
	int d = n2 - n1;
	// find the k-th element of this series that will be bigger than 2M*M
	int k = ceil( (twom2 - n1)/d + 1) + 1;
	if(k <= N)
		return false;

	for(int check = N - 1;check > 1;check--) {
		if(find(bisquares.begin(), bisquares.end(), (n1 + check * d)) == bisquares.end())
			return false;
	}
	return true;
}

int main() {
	ifstream fin;
	fin.open("ariprog.in");
	
	fin >> N;
	fin >> M;
	twom2 = 2*M*M;

	for(int p = 0;p <= M;p++) {
		for(int q = p;q <= M;q++) {
			bisquares.push_back(p * p + q * q);
		}
	}

	// combs = combinations(bl, 2)
	// vector<int> progs;
	int r = 2;

    vector<bool> v(bisquares.size());
    fill(v.end() - r, v.end(), true);
    
    vector<pair<int, int> > progs;
    do {
    	int a = -1;
    	int b = -1;
        for (int i = 0; i < bisquares.size(); ++i) {
        	// int a, b;
            if (v[i]) {
                // cout << (i + 1) << " ";
                if(a == -1)
                	a = bisquares[i];
                else
                	b = bisquares[i];
            }
            
        }
        // cout << a << " " << b << endl;
        // cout << "Hooray! " << a << " " << (b - a) << endl;
        try {
        	/*if(search(a, b)) {
        		cout << "Hooray! " << a << " " << (b - a) << endl;
        	// progs.push_back(make_pair(a, b - a));
	        }*/
	    } catch(...) {

	    }
        // cout << endl;
        /*int a = bisquares[0];
        int b = bisquares[1];*/

        // cout << a << " " << b << endl;
        /*for (int i = 0; i < bisquares.size(); ++i) {
        	cout << (i + 1)
        }*/
        // cout << "\n";
    } while (next_permutation(v.begin(), v.end()));

    /*for(int i = 0;i < progs.size();i++) {

    	pair<int, int> prog = progs[i];
    	cout << prog.first << " " << prog.second << endl;
    }*/
/*for comb in combs:
	
	if search(a, b):
		progs.append((a, b - a))
	# l = 2
	#while search(a, b, l):
	#	l += 1

	#if l == N:
	#	progs.append((a, b - a))
	#print("Check")

# print(bisquares)
# print()
progs = sorted(progs, key = lambda x: (x[1], x[0]))
# print(progs)
if len(progs) == 0:
	print("NONE")
	with open("ariprog.out", "w") as f:
		f.write("NONE\n")
else:
	with open("ariprog.out", "w") as f:
		for (a, b) in progs:
			print(a, b)
			f.write("{} {}\n".format(a, b))
	*/
	return 0;
}