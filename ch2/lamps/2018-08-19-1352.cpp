/*
  ID: kalyan2
  LANG:C++11
  TASK: lamps
*/

#include <iostream>
#include <cstdio>
#include <fstream>
#include <vector>
#include <algorithm>
#include <bitset>

using namespace std;

typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef long long ll;
// typedef uint128_t li;
typedef bitset<100> li;

#define MAXSIZE 100

int N, C;
vi onEnd, offEnd, ans;
// vector< tuple<li, int, int> > memo;
//vector< pair<li, ii> > memo;
vector<pair<li, int> > memo;
bool found = false;

bool fitsReq(li out) {
  // this function assumes that out[] is of length N
  for (int on : onEnd)
    // if (!(out & (1 << on)))
    if (!out[on])
      return false;
  for (int off : offEnd)
    // if (out & (1 << off))
      if (out[off])
      return false;

  return true;
}

void flipold(int (&cur)[MAXSIZE], int btn) {
  switch (btn) {
  case 1:
    for (int i = 0;i < N;i ++) {
      if (i >= N)
        cout << i << endl;
      cur[i] = !(cur[i]);
    }
    break;
  case 2:
    for (int i = 0;i < N - 1;i += 2) {
      if (i >= N)
        cout << i << endl;
      cur[i] = !(cur[i]);
    }
    break;
  case 3:
    for (int i = 1;i < N;i += 2) {
      if (i >= N)
        cout << i << endl;
      cur[i] = !(cur[i]);
    }
    break;
  case 4:
    for (int i = 1;i < N - 2;i += 3) {
      if (i >= N)
        cout << i << endl;
      cur[i] = !(cur[i]);
    }
    break;
  default:
    break;
  }
}

void flip(li &cur, int btn) {
  switch (btn) {
  case 1:
    for (int i = 1;i <= N;i ++) {
      // cur ^= (1 << i);
      cur[i] = !cur[i];
    }
    break;
  case 2:

    for (int i = 1;i < N;i += 2) {
      // cur ^= (1 << i);
      cur[i] = !cur[i];
    }
    break;

  case 3:
    for (int i = 2;i <= N;i += 2) {
      // cur ^= (1 << i);
      cur[i] = !cur[i];
    }
    break;

  case 4:
    for (int i = 1;i <= N;i += 3) {
      // cur ^= (1 << i);
      cur[i] = !cur[i];
    }
    break;

  default:
    break;
  }
}
/*void solveold(int (&cur)[MAXSIZE], int c, int btn) {
  // lets perform the button press:
  flipold(cur, btn);

  // if we have made c moves, check if the current solution fits the requirement
  if (c == C) {
    if (fitsReq(cur)) {
      for (int i = 0;i < N;i++)
        cout << cur[i]; // << " ";
      cout << endl;
    }

    return;
  }

  // we can press btn 1, 2, 3, or 4. To allow for dp, we will pass
  // which btn into solve and recurse
  int d1[MAXSIZE], d2[MAXSIZE], d3[MAXSIZE], d4[MAXSIZE];
  copy(cur, cur + MAXSIZE, d1);
  copy(cur, cur + MAXSIZE, d2);
  copy(cur, cur + MAXSIZE, d3);
  copy(cur, cur + MAXSIZE, d4);
  solveold(d1, c + 1, 1);
  solveold(d2, c + 1, 2);
  solveold(d3, c + 1, 3);
  solveold(d4, c + 1, 4);
  }*/

void solve(li curprev, int c, int btn) {
  // lets perform the button press:
  li cur = curprev;
  flip(cur, btn);

  // we are computing solve for a particular combination.
  // to avoid repeating these computations, we must first
  // check if this combination of solve parameters is in
  // our memo table. If it isn't, we add it and continue...
  // pair<li, ii> comb = make_pair(cur, make_pair(c, btn));
  pair<li, int> comb = make_pair(cur, c);
  // if (memo.find(comb) != memo.end()) {
  if (find(memo.begin(), memo.end(), comb) != memo.end()) {
    // we have computed this before - break here
    return;
  } else {
    // this is a new combination - store it in memo for future reference
    memo.push_back(comb);
    }

  // if we have made c moves, check if the current solution fits the requirement
  if (c == C - 1) {
    if (fitsReq((li) (cur))) {
      ans.push_back(cur.to_ullong());
      /*for (int i = 1;i <= N;i++) {
        //if (cur & (1 << i))
        if (cur[i])
          cout << 1;
        else
        cout << 0;*/
      }
      cout << endl;

      if (!found)
        found = true;
    }

    return;
  }

  // we can press btn 1, 2, 3, or 4. To allow for dp, we will pass
  // which btn into solve and recurse
  // int d1[MAXSIZE], d2[MAXSIZE], d3[MAXSIZE], d4[MAXSIZE];
  // copy(cur, cur + MAXSIZE, d1);
  // copy(cur, cur + MAXSIZE, d2);
  // copy(cur, cur + MAXSIZE, d3);
  // copy(cur, cur + MAXSIZE, d4);
  solve(cur, c + 1, 1);
  solve(cur, c + 1, 2);
  solve(cur, c + 1, 3);
  solve(cur, c + 1, 4);
}
int main() {
  ifstream fin("lamps.in"); // = fopen("lamps.in", "r");

  fin >> N >> C;
  // cout << N << " " << C << endl;

  /*if (C == 0) {
    ofstream fout("lamps.out");
    for (int i = 0;i < N;i++) {
      cout << "1";
      fout << "1";
    }
    cout << endl;
    fout << endl;
    fout.close();

    return 0;
    }*/
  int tmp;
  fin >> tmp;
  while (tmp != -1) {
    onEnd.push_back(tmp);
    // cout << tmp;
    fin >> tmp;
  }
  // cout << endl;

  fin >> tmp;
  while (tmp != -1) {
    offEnd.push_back(tmp);
    //  cout << tmp;
    fin >> tmp;
  }
  // cout << endl;

  // int s1[MAXSIZE];
  // int s2[MAXSIZE];
  // int s3[MAXSIZE];
  // int s4[MAXSIZE];
  // fill(s1, s1 + MAXSIZE, 0);
  // fill(s2, s2 + MAXSIZE, 0);
  // fill(s3, s3 + MAXSIZE, 0);
  // fill(s4, s4 + MAXSIZE, 0);
  // solve(s1, 0, 1);
  // solve(s2, 0, 2);
  // solve(s3, 0, 3);
  // solve(s4, 0, 4);
  freopen("lamps.out", "w", stdout);
  li allOn;
  for (int i = 0;i < 128;i++)
    allOn |= (1 << i);
  if (C != 0) {
    solve(allOn, 0, 1);
    solve(allOn, 0, 2);
    solve(allOn, 0, 3);
    solve(allOn, 0, 4);
  } else {
    // the only possible state is all turned on - if this fits requirements its an answer
    if (fitsReq(allOn)) {
      found = true;
      for (int i = 0;i < N;i++)
        cout << "1";
      cout << endl;
    }
  }

  if (!found)
    cout << "IMPOSSIBLE" << endl;
  return 0;
}
