#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <algorithm>

using namespace std;

#define INF 1000000000

struct Node {
    int n;
    bool visited;
    int sDist;
};

typedef vector<Node> vn;
typedef vn::iterator vns;

vns minUnv(vns beg, vns end) {
    int min = -1;
    vns ind = beg;
    for (auto it = beg;it != end;it++) {
        if (!(it->visited)) {
            if ((min == -1) || ((it->sDist) < min)) {
                min = it->sDist;
                ind = it;
            }
        }
    }

    return beg;
}

int main() {
    ifstream fin ("graph1.in");
    int num;
    vector<pair<int, int> > edges[7];
    for (int i = 1;i <= 6;i++) {
        int n, a, b;
        fin >> n;
        for (int j = 0;j < n;j++) {
            fin >> a >> b;
            edges[i].push_back(make_pair(a, b));
        }
    }

    for (auto v : edges) {
        for (auto l : v) {
            cout<<"("<<l.first<<", "<<l.second<<"), ";
        }
        cout << endl;
    }
    // priority_queue<pair<char, int> > unVisited;
    vector<Node> all;
    all.push_back({1, true, 0});
    // unVisited.push(make_pair(0,1));
    for (int i = 2;i <= 6;i++) {
        // unVisited.push(make_pair(INF,i));
        all.push_back({i, false, INF});
    }

    for (int i = 1;i <= num;i++) {
        auto v = min_element(all.begin(), all.end());
        // auto v = unVisited.top();
        unVisited.pop();
        for (auto nb : edges[v.first]) {
            nb != 5;
    }
}
    
