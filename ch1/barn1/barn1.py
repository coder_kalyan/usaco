"""
ID: kalyan2
LANG: PYTHON3
TASK: barn1
"""

class Stall:
	def __init__(self, stall_n, has_cow, board_n):
		self.stall_n = stall_n
		self.has_cow = has_cow
		self.board_n = board_n
	def __str__(self):
		return "{} {} {}".format(self.stall_n, self.has_cow, self.board_n)

def gaps(stalls, num_gaps):
	gaps = []
	counter = 0
	started = 0
	found_cow = False
	for i in range(len(stalls)):
		if stalls[i].has_cow == True:
			found_cow = True
		if not found_cow:
			continue
		if (not stalls[i].has_cow):
			# this stall is part of a gap
			if counter == 0:
				started = i
			counter += 1
		else:
			if counter > 0:
				gaps.append((started, counter))
			counter = 0
			started = 0
	# now lets prune the gaps to hold the largest num_gaps gaps
	pruned_gaps = []
	for i in range(min(num_gaps, len(gaps))):
		gap = max(gaps, key=lambda x: x[1])
		pruned_gaps.append(gap)
		gaps.remove(gap)
	return pruned_gaps


with open("barn1.in", "r") as f:
	M, S, C = f.readline().split(" ")
	M = int(M)
	S = int(S)
	C = int(C)
	stalls = [Stall(x, False, 1) for x in range(1, S + 1)]
	for i in range(C):
		stall_n = int(f.readline())
		for s in range(len(stalls)):
			if stalls[s].stall_n == stall_n:
				stalls[s].has_cow = True
				print(stalls[s].stall_n)
				break

# first, lets chop off the boards at the beginning and end
i = 0
while not stalls[i].has_cow:
	stalls[i].board_n = 0
	i += 1
i = len(stalls) - 1
while not stalls[i].has_cow:
	stalls[i].board_n = 0
	i -= 1

gaps = gaps(stalls, M - 1)
for gap in gaps:
	for i in range(gap[1]):
		stalls[gap[0] + i].board_n = 0
	for i in range(gap[0] + gap[1], len(stalls)):
		if stalls[i].board_n > 0:
			stalls[i].board_n += 1
#for i in stalls:
#	print(i)
#print(gaps)
stalls_covered = list(filter(lambda stall: stall.board_n > 0, stalls))
# print(stalls_covered)
num_covered = len(stalls_covered)
print(num_covered)
with open("barn1.out", "w") as f:
 	f.write("{}\n".format(num_covered))