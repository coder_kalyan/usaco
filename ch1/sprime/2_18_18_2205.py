"""
ID: kalyan2
TASK: sprime
LANG: PYTHON3
"""

from math import *
from itertools import *

pprimes = []

fin = open("sprime.in", "r")
fout = open("sprime.out", "w")

def is_prime(n):
	if n == 1:
		return False
	elif n == 2 or n == 3:
		return True
	elif n % 2 == 0 or n % 3 == 0:
		return False
	for i in range(2, int(ceil(sqrt(n)) + 1)):
		if n % i == 0:
			return False

	return True

N = int(fin.readline())

options = [[1,2,3,5,7,9]] +  [[0,1,3,7,9]] * (N - 2) + [[1,3,7,9]]

sprimes = []
for pro in product(*options):
	total = 0
	for elem in pro:
		total = total * 10 + elem
		if not is_prime(total):
			break
	else:
		sprimes.append(total)
# print(sprimes)
fout.write("\n".join(map(str,sprimes)))
fout.write("\n")
