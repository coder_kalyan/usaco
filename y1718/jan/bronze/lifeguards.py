shifts = []
with open("lifeguards.in") as f:
    N = int(f.readline())
    for i in range(N):
        shifts.append(list(map(int,f.readline().split())))
    print(shifts)

def cover(skip_id):
    covered = [0] * 1000
    for i in range(N):
        if i == skip_id:
            continue
        print(shifts[i])
        for j in range(shifts[i][0] - 0,shifts[i][1] - 0):
            covered[j] = 1
    return covered.count(1)

max_c = 0
for i in range(N):
    tmp = cover(i)
    if tmp > max_c:
        max_c = tmp

print(max_c)

with open("lifeguards.out", "w") as f:
    f.write(str(max_c) + "\n")
