/*
  ID: kalyan2
  LANG:C++11
  TASK: nocows
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;

const string PROG = "nocows";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N, K;
int dp[201][201];

int tree(int a, int b) {
    int i, j, sum=0, temp1, temp2;
    if (a==0 || b==0) return 0;
    if (a==1) return 1;
    if (dp[a][b]!=-1) return dp[a][b];
    else 
    for (i=1;i<a-1;i++)
        sum=(sum+tree(i,b-1)*tree(a-i-1,b-1))%9901;
    dp[a][b]=sum;
    return sum;
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N >> K;
    REP(i, 0, 200)
        REP(j, 0, 200)
            dp[i][j] = -1;
    int ans = (9901 + tree(N, K) - tree(N, K-1)) % 9901;
    cout << ans << endl;
    fout << ans << endl;
	return 0;
}
