/*
  ID: kalyan2
  LANG:C++11
  TASK: agrinet
*/

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>
#include <climits>
#include <cmath>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define RREP(i,a,b) for (long i=b-1;i>=a;i--)
#define LOG(a,b) clog << a << ": " << b << endl;
#define MP(a,b) make_pair(a,b)
#define DIST(x1,y1,x2,y2) hypot(abs(x2-x1),abs(y2-y1))

class cmp;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;
typedef priority_queue<ii> pqiif;
typedef priority_queue<ii,vii,cmp> pqiis;
typedef vector<vi> vvi;
typedef vvi::iterator vvi_i;
typedef vector<vii> vvii;
typedef vvii::iterator vvii_i;

class cmp {
    bool operator() (const ii &a, const ii &b) {
        return a.second > b.second;
    }
};

#define INF INT_MAX

#define MAXN 100

const string PROG = "agrinet";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N;
long mat[MAXN][MAXN];
ll dist[MAXN];
long source[MAXN];
bool intree[MAXN];

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N;
    REP(j,0,N)
        REP(i,0,N)
            fin >> mat[j][i];

    fill(dist, dist+MAXN, INF);
    fill(intree, intree+MAXN, false);
    fill(source, source+MAXN, -1);

    int treesize = 1, treecost = 0;
    intree[0] = true;
    REP(i, 0, N) {
        if (mat[0][i] != 0) {
            dist[i] = mat[0][i];
            source[i] = 1;
        }
    }

    while (treesize < N) {
        // auto mInd = min_element(dist,dist+N);
        // int i = distance(dist, mInd);
        int i = 0, m = INF;
        REP(x,0,N) {
            if ((!intree[x]) && (dist[x] != INF) && (dist[x] < m)) {
                m = dist[x];
                i = x;
            }
        }
        treesize++;
        treecost += dist[i];
        // cout << dist[i] << " ";
        intree[i] = true;

        REP(j,0,N) {
            if (mat[i][j] != 0) {
                if (dist[j] > mat[i][j]) {
                    dist[j] = mat[i][j];
                    source[j] = i;
                }
            }
        }
    }

    // cout << endl;
    // cout << treesize << endl;
    cout << treecost << endl;
    fout << treecost << endl;

	return 0;
}
