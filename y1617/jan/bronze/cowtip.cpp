#include <iostream>
#include <fstream>

using namespace std;

#define RIGHT 0
#define WRONG 1
#define MAXN 10

char grid[MAXN*MAXN];
short N;

void toggle(short rowsize, short colsize) {
    for (int row = 0;row < rowsize;row++)
        for (int col = 0;col < colsize;col++)
            grid[col+row*N] = (grid[col+row*N]==0) ? 1 : 0;
}

void disp() {
    for (int i = 0;i < N*N;i++)
        cout << grid[i] << " ";
    cout << endl;
    return;
}

int main() {
    ifstream fin ("cowtip.in");
    ofstream fout ("cowtip.out");

    fin >> N;
    for (int i = 0;i < N*N;i++)
        fin >> grid[i];
    for (int i = 0;i < N*N;i++)
        grid[i] -= '0';
    
    disp();
    short numtoggles = 0;
    for (int row = N-1;row >= 0;row--)
        for (int col = N-1;col >= 0;col--)
            cout << (int) grid[col+row*N];
            // if ((int) grid[col+row*N] == WRONG) {
                // toggle(MAXN-row+1,MAXN-col+1);
                // disp();
                // numtoggles++;
            // }

    cout << numtoggles << endl;
}
