"""
USACO Program
ID: kalyan2
LANG: PYTHON3
TASK: namenum
"""

charmap = {
	"2": ["A", "B", "C"],
	"3": ["D", "E", "F"],
	"4": ["G", "H", "I"],
	"5": ["J", "K", "L"],
	"6": ["M", "N", "O"],
	"7": ["P", "R", "S"],
	"8": ["T", "U", "V"],
	"9": ["W", "X", "Y"]
}

with open("namenum.in") as f:
	ID = f.readline()
	ID = ID.replace("\n", "")

with open("dict.txt") as f:
	allowed_names = f.read().split("\n")

# prune the dictionary for short words
allowed_names = [name for name in allowed_names if len(name) == len(ID)]
f = open("namenum.out", "w")

#names = []
def permute(s, i, total):
	global names
	if i == total:
		for c in charmap[ID[i]]:
			if s + c in allowed_names:
				f.write(s + c)
				f.write("\n")
				print(s + c)	
	else:
		for c in charmap[ID[i]]:
			permute(s + c, i + 1, total)

#if len(names) > 0:
#	f.write("\n".join(names))
permute("", 0, len(ID) - 1)
f.close()
f = open("namenum.out", "r")
if f.read() == "":
	f2 = open("namenum.out", "w")
	f2.write("NONE\n")
	f2.close()
f.close()
f.close()
