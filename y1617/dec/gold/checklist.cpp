#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>
#include <climits>
#include <cmath>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define RREP(i,a,b) for (long i=b-1;i>=a;i--)
#define LOG(a,b) clog << a << ": " << b << endl;
#define MP(a,b) make_pair(a,b)
#define DIST(x1,y1,x2,y2) hypot(abs(x2-x1),abs(y2-y1))

class cmp;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;
typedef priority_queue<ii> pqiif;
typedef priority_queue<ii,vii,cmp> pqiis;
typedef vector<vi> vvi;
typedef vvi::iterator vvi_i;
typedef vector<vii> vvii;
typedef vvii::iterator vvii_i;

class cmp {
    bool operator() (const ii &a, const ii &b) {
        return a.second > b.second;
    }
};

#define INF INT_MAX
#define MAXN 1000

const string PROG = "checklist";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

int H, G;
vii h, g;
long dp[MAXN+1][MAXN+1][2];

long dist(ii a, ii b) {
    long d1 = a.first - b.first;
    long d2 = a.second - b.second;
    return (d1*d1) + (d2*d2);
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> H >> G;
    h.resize(H);
    g.resize(G);
    REP(i,0,H)
        fin >> h[i].first >> h[i].second;
    REP(i,0,G)
        fin >> g[i].first >> g[i].second;

    REP(i,0,H+1)
        REP(j,0,G+1)
            REP(k,0,2)
                dp[i][j][k] = INF;
    dp[1][0][0] = 0;

    REP(i,0,H+1) {
        REP(j,0,G+1) {
            REP(k,0,2) {
                if (k == 0 && i == 0)
                    continue;
                if (k == 1 && j == 0)
                    continue;
                ii src;
                if (k == 0) src = h[i-1];
                else src = g[j-1];
                if (i < H)
                    dp[i+1][j][0] = min(dp[i+1][j][0],dp[i][j][k]+dist(src,h[i]));
                if (j < G)
                    dp[i][j+1][1] = min(dp[i][j+1][1],dp[i][j][k]+dist(src,g[j]));
                // cout << i << " " << j << endl;
            }
        }
    }

    // REP(i,0,H+1) {
        // REP(j,0,G+1)
            // cout << "(" << dp[i][j][0] << "," << dp[i][j][1] << "), ";
        // cout << endl;
    // }
            
    cout << dp[H][G][0] << endl;
    fout << dp[H][G][0] << endl;

	return 0;
}
