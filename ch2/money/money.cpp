/*
  ID: kalyan2
  LANG:C++11
  TASK: money
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;

#define MAXN 10000
#define MAXV 25

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;

const string PROG = "money";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

int V, N;
vi coins;
ll dp[MAXN+1];

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> V >> N;
    coins.resize(V);
    REP(i,0,V) fin >> coins[i];

    dp[0] = 1;
    for (auto cn : coins) {
        for (int i = cn;i<=N;i++)
            dp[i] += dp[i-cn];
    }

    cout << dp[N] << endl;
    fout << dp[N] << endl;

	return 0;
}
