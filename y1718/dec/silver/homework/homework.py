with open("homework.in") as f:
    N = int(f.readline())
    QS = list(map(int, f.readline().split()))

m = [0] * N
s = [0] * N
m[N - 1] = QS[N - 1]
s[N - 1] = QS[N - 1]

for i in range(N - 2, -1, -1):
    m[i] = min(m[i + 1], QS[i])
    s[i] = s[i + 1] + QS[i]

# print(m)
# print(s)

scores = [0] * (N + 1)
for k in range(1, N - 1):
    scores[k] = (s[k] - m[k]) / (N - k - 1)

# print(scores)
mx = max(scores)
ks = []
for k in range(1, N - 1):
    if scores[k] == mx:
        ks.append(k)
        
ans = "\n".join(map(str, ks))
print(ans)
with open("homework.out", "w") as f:
    f.write(ans)
    f.write("\n")
