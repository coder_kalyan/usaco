#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;

const string PROG = "div7";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N;
vi cows;

bool isGroup(long n) {
    // return true if there is a subset of cows with size n
    // else false
    ll sum = accumulate(cows.begin(), cows.begin()+n, 0);
    REP(i, 0, N-n) {
        if (sum % 7 == 0)
            return true;

        sum -= cows[i];
        sum += cows[i + n];
    }

    return false;
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N;
    cows.resize(N);
    REP(i,0,N) fin >> cows[i];

    int max = 0;
    REP(i, 0, N) {
        if(isGroup(i))
            max = i;
    }

    cout << max << endl;
    fout << max << endl;

	return 0;
}
