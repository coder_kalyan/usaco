#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;

#define MAXN 1000

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;

const string PROG = "template";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

struct Edge {
    int x1, y1, x2, y2;
};

int N;
vector<Edge> edges;

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N;
    edges.resize(N);

    char tmp;
    int x = 0, y = 0;
    int xp = 0, yp = 0;

    REP(i, 0, N) {
        fin >> tmp;
        switch (tmp) {
            case 'N':
                y += 1;
                break;
            case 'S':
                y -= 1;
                break;
            case 'E':
                x += 1;
                break;
            case 'W':
                x -= 1;
                break;
            default:
                break;
        }

        edges.push_back({xp, yp, x, y});
        edges.push_back({x, y, xp, yp});
        xp = x;
        yp = y;
    }

    
           
	return 0;
}
