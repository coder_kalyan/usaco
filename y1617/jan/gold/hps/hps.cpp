#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <queue>
#include <algorithm>
#include <cmath>
#include <map>
#include <climits>
#include <string>

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef vector<int> vi;
#define INF 1000000000

int N, K;
int fj[100001];
int dp[100001][21][3];

int solve(int n, int k, int m) {
    //cout << n << " " << k << " " << m << endl;
    if (n == N) return fj[n] == m;
    if (k == 0) return (fj[n] == m) + solve(n + 1, k, m);
    if (dp[n][k][m] != 0) return dp[n][k][m];

    int ret = -INF;
    ret = max(ret, solve(n + 1, k, m));
    ret = max(ret, solve(n + 1, k - 1, 'H')); 
    ret = max(ret, solve(n + 1, k - 1, 'P')); 
    ret = max(ret, solve(n + 1, k - 1, 'S')); 

    return dp[n][k][m] = (ret + (fj[n] == m));
}

int main() {
    ifstream fin("hps.in");
    ofstream fout("hps.out");

    fin >> N >> K;
    char tmp;
    for (int i=1;i<=N;i++) {
        fin >> tmp;
        fj[i] = tmp;
    }

    int ans = -INF;
    ans = max(ans, solve(1, K, 'H'));
    ans = max(ans, solve(1, K, 'P'));
    ans = max(ans, solve(1, K, 'S'));

    cout << ans << endl;
    fout << ans << endl;
    
    return 0;
}
