#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>
#include <climits>
#include <cmath>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define RREP(i,a,b) for (long i=b-1;i>=a;i--)
#define LOG(a,b) clog << a << ": " << b << endl;
#define MP(a,b) make_pair(a,b)
#define DIST(x1,y1,x2,y2) hypot(abs(x2-x1),abs(y2-y1))

class cmp;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;
typedef priority_queue<ii> pqiif;
typedef priority_queue<ii,vii,cmp> pqiis;
typedef vector<vi> vvi;
typedef vvi::iterator vvi_i;
typedef vector<vii> vvii;
typedef vvii::iterator vvii_i;

class cmp {
    bool operator() (const ii &a, const ii &b) {
        return a.second > b.second;
    }
};

#define INF INT_MAX
#define MAXN 10000

const string PROG = "piepie";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

int N, D;
int A[2*MAXN], B[2*MAXN];
int dist[2*MAXN];

class cmpA {
    bool operator() (int a, int b) {
        return A[a] < A[b];
    }
};
class cmpB {
    bool operator() (int a, int b) {
        return B[a] < B[b];
    }
};

typedef set<int, cmpA> sa;
typedef set<int, cmpB> sb;
sa sA;
sb sB;

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N >> D;
    vi zeroes;
    REP(i,0,N) {
        fin >> A[i] >> B[i];
        sA.insert(i);
        dist[i] = INF;
    }
    REP(i,N,2*N) {
        fin >> A[i] >> B[i];
        sB.insert(i);
    }

    
    
	return 0;
}
