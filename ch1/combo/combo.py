"""
ID: kalyan2
LANG: PYTHON3
TASK: combo
"""

with open("combo.in", "r") as f:
	num_pos = int(f.readline())		# the number of positions on each dial
	if num_pos == 0:
		with open("combo.out", "w") as f:
			print(0)
			f.write("0")
		exit(0)

	key_a = f.readline().split(" ")		# farmer john's lock key
	key_a = list(map(int, key_a))
	key_b = f.readline().split(" ")		# the master lock key
	key_b = list(map(int, key_b))
	
	print(key_a)
	print(key_b)

positions = [x for x in range(1, num_pos + 1)]
def all_accepted(base):
	# returns a list of accepted positions based on base - the lock has a error of 2
	return [positions[(base - 3) % num_pos],positions[(base - 2) % num_pos],positions[(base - 1) % num_pos],positions[(base) % num_pos],positions[(base +1) % num_pos]]
def all_combinations(key):
	combs = set()		# we are using a set because they do NOT store repeats
	for a in all_accepted(key[0]):
		for b in all_accepted(key[1]):
			for c in all_accepted(key[2]):
				combs.add((a, b, c))		# like list.append()
	return combs
combs = all_combinations(key_a)
combs.update(all_combinations(key_b))		# like list.extend()
print(len(combs))
with open("combo.out", "w") as f:
	f.write("{}\n".format(len(combs)))
