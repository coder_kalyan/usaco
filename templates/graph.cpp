#include <vector>
#include <algorithm>
#include <climits>
#include <queue>

using namespace std;

#define INF INT_MAX

void floyd(long **mat, long N) {
    for (long k=0;k<N;k++)
        for (long i=0;i<N;i++)
            for (long j=0;j<N;j++)
                if (mat[i][k]+mat[k][j]<mat[i][j])
                    mat[i][j] = mat[i][k]+mat[k][j];
}

struct Pair2ndGr {
    bool operator()(pair<long, long> a, pair<long, long> b) {
        return a.second > b.second;
    }
};

void dijkstra(vector<pair<long, long> > *adj, long *dis, bool *vis, long N, long src) {
    fill(dis,dis+N,INF);
    fill(vis,vis+N,false);
    priority_queue<pair<long,long>,vector<pair<long,long> >,Pair2ndGr> pq;
    pq.push(make_pair(src,dis[src]=0));
    while (!pq.empty()) {
        pair<long,long> curr = pq.top();
        pq.pop();
        int cv = curr.first, cw = curr.second;
        if (vis[cv])
            continue;
        vis[cv] = true;
        for (auto nb : adj[cv]) {
            if (!vis[nb.first] && nb.second+cw<dis[nb.first])
                pq.push(make_pair(nb.first,dis[nb.first]=nb.second+cw));
        }
    }
}

long prim_min_key(long *key, bool *mstSet, long N) {
    long min = INF, min_idx;
    for (long v = 0;v < N;v++) {
        if ((mstSet[v] == false) && (key[v] < min))
            min = key[v], min_idx = v;
    }
}

void prim(vector<pair<long,long> > *adj, long *par, long *key, bool *mstSet, long N) {
    fill(key,key+N,INF);
    fill(mstSet,mstSet+N,false);

    key[0] = 0;
    par[0] = -1;
    for (long c = 0;c < N-1;c++) {
        int u = prim_min_key(key, mstSet, N);
        mstSet[u] = true;
        for (pair<long,long> nb : adj[u]) {
            if ((mstSet[nb.first] == false) && (nb.second < key[nb.first]))
                par[nb.first] = u, key[nb.first] = nb.second;
        }
    }
}


void fenwick_update(long *tree, long N, long i, long delta) {
    for (; i < N; i |= i + 1)
        tree[i] += delta;
}

void fenwick_build(long *src, long *tree, long N) {
    fill(tree,tree+N,0);
    for (long i = 0;i < N;i++)
        fenwick_update(tree, N, i, src[i]);
}

long fenwick_query(long *tree, long ind) {
    long sum = 0;
    while (ind >= 0) {
        sum += tree[ind];
        ind &= ind + 1;
        ind--;
    }
    return sum;
}
