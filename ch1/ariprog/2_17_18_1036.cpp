/*
ID: kalyan2
TASK: ariprog
LANG: C++
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;

short N, M;
int bisquares[63002];
vii progs;
// vector<bool> mask;
int myind = 0;

int main() {
	ifstream fin;
	fin.open("ariprog.in");
	
	fin >> N;
	fin >> M;

	// int myind = 0;
	for(int i = M;i >=0;i--) {
		for(int j = i;j >=0;j--) {
			int mytemp = (i*i) + (j*j);
			// add to array only if not duplicate
			int inarray = 0;
			for (int k =0; k < myind; k++) {
				if (bisquares[k] == mytemp) {
					inarray = 1;
					break;
				}
			}
			if (!inarray)
				bisquares[myind++] = mytemp;				
		}
	}

	sort(&bisquares[0], &bisquares[myind]);
	
	cout << "Finished generating bisquares" << endl;

	//for(int i = 0;i < bisquares.size();i++) {
	// for(int i = 0;i < myind;i++) {
	// 	cout << bisquares[i] << " ";
	// }
	// cout << endl;

	// generate all sequences of length 2
	// and then check if the rest of the sequence exists
	for(int i = 0;i < myind-1;i++) {
		for(int j = i+1;j < myind;j++) {
			bool isSequence = true;
			int seqlen = N - 1;
			while ( (seqlen >= 2) && isSequence ) {
				int nextelem = bisquares[i] + seqlen*(bisquares[j] - bisquares[i]);
				if(!binary_search(&bisquares[0],&bisquares[myind],nextelem))
					isSequence = false;
				else
					seqlen--;
			} 
			// if this is a valid sequence, push it in
			if (isSequence)
				progs.push_back(make_pair(bisquares[j]-bisquares[i], bisquares[i]));
		}
	}
	cout << "Finished generating sequences" << endl;
	
	sort(progs.begin(), progs.end());
	cout << "Sorted" << endl;

	FILE* fout;
	fout = fopen("ariprog.out", "w");
	if(progs.size() == 0) {
		fprintf(fout, "NONE\n");
	} else {
		for(int i = 0;i < progs.size();i++) {
			printf("%d %d\n", progs[i].second, progs[i].first);
			fprintf(fout,"%d %d\n", progs[i].second, progs[i].first);
		}
	}
	fclose(fout);

	return 0; 
}