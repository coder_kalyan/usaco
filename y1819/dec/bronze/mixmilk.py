class Bucket:
    def __init__(self, c, m):
        self.c = c
        self.m = m

with open("mixmilk.in") as f:
    c1, m1 = map(int, f.readline().split())
    bkt1 = Bucket(c1, m1)
    c2, m2 = map(int, f.readline().split())
    bkt2 = Bucket(c2, m2)
    c3, m3 = map(int, f.readline().split())
    bkt3 = Bucket(c3, m3)

print(bkt1.c, bkt1.m)
print(bkt2.c, bkt2.m)
print(bkt3.c, bkt3.m)

def pour(a, b):
    amt = min(a.m, b.c - b.m)
    a.m -= amt
    b.m += amt

    return a, b

buckets = [bkt1, bkt2, bkt3]
ai = 0
bi = 1
for i in range(100):
    a, b = pour(buckets[ai], buckets[bi])
    buckets[ai] = a
    buckets[bi] = b
    ai = (ai + 1) % 3
    bi = (bi + 1) % 3

with open("mixmilk.out", "w") as f:
    for bucket in buckets:
        print(bucket.m)
        f.write(str(bucket.m))
        f.write("\n")

