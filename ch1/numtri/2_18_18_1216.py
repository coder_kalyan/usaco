"""
ID: kalyan2
TASK: numtri
LANG: PYTHON3
"""

largest = 0
tri = []

fin = open("numtri.in", "r")
fout = open("numtri.out", "w")


R = int(fin.readline())

for i in range(R):
	line = fin.readline()
	row = []
	for i in line.split(" "):
		row.append([int(i), 0])
	tri.append(row)
	# tri.append(list(map(int, ))

tri[0][0][1] = tri[0][0][0]

for rowN in range(1, R):
	for colN, elem in enumerate(tri[rowN]):
		if colN == 0:
			tri[rowN][0][1] = tri[rowN][0][0] + tri[rowN - 1][0][1]
		elif colN == rowN:
			tri[rowN][colN][1] = tri[rowN][colN][0] + tri[rowN - 1][rowN - 2][1]
		else:
			# print(colN-2)
			#if tri[rowN][colN][0] == 5:
			#	print colN, tri[rowN-1][colN-1][1], tri[rowN-1][colN][1]
			tri[rowN][colN][1] = tri[rowN][colN][0] + max(tri[rowN-1][colN-1][1], tri[rowN-1][colN][1])

# print(tri)
"""for row in tri:
	for elem in row:
		print elem,#[1],
	print
"""
largest = 0
for elem in tri[-1]:
	if elem[1] > largest:
		largest = elem[1]

# largest = max(row[-1])
print(largest)
fout.write(str(largest) + "\n")
