#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>

using namespace std;

#define MAXN 100000
long cur[MAXN], shuffled[MAXN], tmp[MAXN];

#define REP(i,a,b) for(long i = a;i < b;i++)
typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;

const string PROG = "shuffle";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N;

void doShuffle() {
    REP(i, 0, N)
        clog << cur[i] << shuffled[i] << endl;

    fill(tmp,tmp+N,0);
    REP(i, 0, N) {
        tmp[shuffled[i]] += cur[i];
        // clog << i << "(" << shuffled[i] << "," << tmp[i] << ") ";
    }

    copy(tmp, tmp+N, cur);
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N;
    long tmp2;
    REP(i,0,N)
        fin >> shuffled[i];
    REP(i,0,N)
        clog << shuffled[i] << " ";
    clog << endl;

    fill(cur,cur+N,1);

    long nZero, lZero;
    do {
        lZero = nZero;
        doShuffle();
        nZero = count(cur, cur+N,0);
        REP(i,0,N)
            clog << cur[i] << " ";
        clog << endl;
    } while (true); //while (nZero != lZero);

    cout << nZero << endl;
    // REP(i,0,N)
        // clog << cur[i] << " ";
    // clog << endl;

	return 0;
}
