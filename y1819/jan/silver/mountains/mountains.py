with open("mountains.in") as f:
    N = int(f.readline())
    orig = []
    for i in range(N):
        orig.append(tuple(map(int, f.readline().split())))

print(orig)
vis = []

def visible(a, b):
    if a[1] < b[1]:
        # a is lower than b
        if abs(b[0] - a[0]) <= b[1] - a[1]:
            return 0
        else:
            return -1
    elif a[1] > b[1]:
        # b is lower than a
        if abs(b[0] - a[0]) <= a[1] - b[1]:
            return 1
        else:
            return -1
    else:
        # same height, they can't obscure each other
        return -1

while len(orig) > 0:
    cur = orig[0]
    del orig[0]
    
