from itertools import *

def max_row(frag):
    m = 0
    for i in range(0, N * N, N):
        tmp = frag[i:i+N]
        if tmp.count("#") > m:
            m = tmp.count("#")

    return m

def max_col(frag):
    m = 0
    for i in range(0, N):
        tmp = frag[i] + frag[i+N] + frag[i+2*N]+frag[i+3*N]
        if tmp.count("#") > m:
            m = tmp.count("#")

    return m

with open("bcs.in") as f:
    N, K = map(int, f.readline().split())
    figure = ""
    for j in range(N):
        figure += f.readline().replace("\n","")
    num_hash = figure.count("#")
    mr = max_row(figure)
    mc = max_col(figure)

    frags = []
    for i in range(K):
        tmp = ""
        for j in range(N):
            tmp += f.readline().replace("\n","")
        frags.append(tmp)


for frag1, frag2 in combinations(frags, 2):
    if frag1.count("#") + frag2.count("#") != num_hash:
        continue

    b = False
    for i in range(0, N * N, N):
        tmp = figure[i:i+N]
        if tmp.count("#") > mr:
            b = True
            break
    if b:
        continue

    for i in range(0, N):
        tmp = figure[i] + figure[i+N] + figure[i+2*N]+figure[i+3*N]
        if tmp.count("#") > mc:
            b = True
            break
    if b:
        continue

    print(frag1, frag2)
