/* USACO Program
ID: kalyan2
LANG: C
TASK: gift1
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Person {
	char name[20];
	unsigned int initialMoney;
	unsigned int numPeople;
	unsigned int finalMoney;
};

struct Person persons[10];

unsigned int getPersonIndex(char name[20]) {
	int personIndex;
	
	personIndex = 0;
	while(strcmp(name,persons[personIndex].name) != 0)
		personIndex++;
	return personIndex;
}

void main(void) {
	FILE *fin, *fout;
	int loop;
	unsigned int numPersons;
	int personIndex;
	char cmpName[20];
	unsigned int money,numGifts;
	unsigned int loop2;
	char tempName[20];
	int index2;
	
	// open the files
	fin = fopen("gift1.in","r");
	fout = fopen("gift1.out", "w");
	
	// read in the persons
	fscanf(fin, "%d\n", &numPersons);
	for(loop = 0;loop < numPersons;loop++) {
		fscanf(fin, "%s\n",persons[loop].name);
		printf("%s\n",persons[loop].name);
		persons[loop].initialMoney = 0;
		persons[loop].finalMoney = 0;	
	}
	
	printf("\n");
	
	for(loop = 0;loop < numPersons;loop++) {
		fscanf(fin,"%s\n",cmpName);
		
		personIndex = getPersonIndex(cmpName);
		
		fscanf(fin,"%d %d\n",&money,&numGifts);
		persons[personIndex].initialMoney = money;
		
		if(money > 0) {
			persons[personIndex].finalMoney -= money;
			persons[personIndex].finalMoney += money % numGifts;
			
			for(loop2 = 0;loop2 < numGifts;loop2++) {
				fscanf(fin,"%s\n",tempName);
				
				index2 = getPersonIndex(tempName);
				persons[index2].finalMoney += money/numGifts;
			}
		}
	}
	for(loop = 0;loop < numPersons;loop++) {
		fprintf(fout,"%s %d\n",persons[loop].name,persons[loop].finalMoney);
	}
	
	exit(0);
}
