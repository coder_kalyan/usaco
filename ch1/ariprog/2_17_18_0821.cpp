/*
ID: kalyan2
TASK: ariprog
LANG: C++
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;

short N, M;
vi bisquares;
vii progs;
vector<bool> mask;

// find all sequences of length 2 - this is simply (M+1)^2 choose 2, since any
// two numbers will be a sequence
// then for each of those, see if a third number is available
// continue this process until 

int main() {
	ifstream fin;
	fin.open("ariprog.in");
	
	fin >> N;
	fin >> M;

	// cout << N << endl << M << endl;

	for(int i = 0;i <= M;i++) {
		for(int j = i;j <= M;j++) {
			bisquares.push_back(i * i + j * j);
		}
	}

	sort(bisquares.begin(), bisquares.end());
	bisquares.erase(unique(bisquares.begin(), bisquares.end()), bisquares.end());
	
	cout << "Finished generating bisquares" << endl;

	// for(int i = 0;i < bisquares.size();i++) {
	// 	cout << bisquares[i] << " ";
	// }
	// cout << endl;

	// generate all sequences of length 2
	for(int i = 0;i < bisquares.size()-2;i++) {
		for(int j = i+1;j < bisquares.size()-1;j++) {
			// i, j here is the choose 2 set
			// progs.push_back(make_pair(diff, first));
			// we will save in the format difference, first
			progs.push_back(make_pair(bisquares[j]-bisquares[i], bisquares[i]));
			mask.push_back(true);
		}
	}

	cout << "Finished seq length 2" << endl;
	// for(int i = 0;i < progs.size();i++) {
	// 	printf("%d %d\n", progs[i].second, progs[i].first);
	// }

	// now for each of these sequences, see if the rest of the sequence exists
	// starting from the last element
	for (int i = 0; i < progs.size(); i++) {
		int seqlen = N - 1;
		while ( (seqlen >= 2) && mask[i] ) {
			// int foundone = 0;
			// int wherefound = 0;
			// int thisFound = 0;
			int nextelem = progs[i].second + seqlen*progs[i].first;
				//for (int k = wherefound; k < bisquares.size(); k++) {
				// for (int k = 0; bisquares[k] <= nextelem; k++) {
				// 	if (nextelem == bisquares[k]) {
				// 		thisFound = 1;
				// 		wherefound = k-1;
				// 		foundone++;
				// 		break;
				// 	}
				// }
				/*if(find(bisquares.begin(),bisquares.end(),nextelem) != bisquares.end()) {
					thisFound = 1;
					foundone++;
				}*/
			if(!binary_search(bisquares.begin(),bisquares.end(),nextelem))
				mask[i] = false;
			else
				seqlen--;
		}
	}

		// cout << "Generated combinations" << endl;
		// sort(progs.begin(), progs.end());
		// cout << "Sorted combinations" << endl;
		// progs.erase(unique(progs.begin(), progs.end()), progs.end());
	cout << "Finished generating sequences" << endl;
	vii filtered;
	for(int i = 0;i < progs.size();i++) {
		if(mask[i])
			filtered.push_back(progs[i]);
	}
	
	sort(filtered.begin(), filtered.end());
	cout << "Sorted" << endl;
	FILE* fout;
	fout = fopen("ariprog.out", "w");
	if(filtered.size() == 0) {
		fprintf(fout, "NONE\n");
	} else {
		for(int i = 0;i < filtered.size();i++) {
			printf("%d %d\n", filtered[i].second, filtered[i].first);
			fprintf(fout,"%d %d\n", filtered[i].second, filtered[i].first);
		}
	}


	fclose(fout);
	return 0; 
}