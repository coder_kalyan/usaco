"""
ID: kalyan2
TASK: pprime
LANG: PYTHON3
"""

from math import *
from itertools import *

pprimes = []

fin = open("pprime.in", "r")
fout = open("pprime.out", "w")

def is_prime(n):
	for i in range(2, int(ceil(sqrt(n)) + 1)):
		if n % i == 0:
			return False

	return True

a, b = list(map(int,fin.readline().split(" ")))
# print(str(a),str(b))
palindromes = []

sizes = list(range(len(str(a)), len(str(b)) + 1))
print("sizes: ", sizes)


for size in sizes:
	if size > 1:
		f_options = (1,3,7,9)
	else:
		f_options = (1,3,5,7,9)
	r_options = tuple(range(10))
	options = [f_options[:]]
	num_reg = (size - 1) // 2
	print(num_reg)
	for i in range(num_reg):
		options.append(r_options)
	print(options)

	for pro in product(*options):
		temp = "".join(map(str,pro))
		if size > 1:
			if size % 2 == 0:
				temp += temp[::-1]
			else:
				temp += "".join(reversed(temp))[1:]
		palindromes.append(int(temp))
print(palindromes)
for p in palindromes:
	if p < a or p > b:
		# print("wrong size")
		continue
	if not is_prime(p):
		continue
	pprimes.append(p)
pprimes = list(map(str, pprimes))
#print(pprimes)
# print "\n".join(pprimes)
fout.write("\n".join(pprimes))
fout.write("\n")
