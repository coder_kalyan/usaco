# multiple implementations of fibonnaci, in increasing efficiency

# naive recursive solution
def fib_naive(n):
    if n == 1:
        return 1
    if n == 2:
        return 2
    return fib_naive(n - 1) + fib_naive(n - 2)


memo = [-1 for i in range(100000)]
memo[1] = 1
memo[2] = 2

def fib_dp1(n):
    if memo[n] != -1:
        return memo[n]
    ans = fib_dp1(n - 1) + fib_dp1(n - 2)
    memo[n] = ans
    return ans

def fib_dp2(n):
    a = 1
    b = 2
    for i in range(3, n + 1):
        a, b = b, a + b
    return b
