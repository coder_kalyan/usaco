/* USACO Program
ID: kalyan2
LANG: C
TASK: milk2
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned short N;

// we have 2 arrays, one for all the start times, and one for all the end times
int starts[5000];
int ends[5000];

int display(int *l, int n);
void shiftr(int *l, int length, int start, int end);
void shiftl(int *l, int length, int start, int end);
void insert(int *l, int length, int from, int to);
int find_sorted_location(int *l, int length, int value);
void insertion_sort(int *l, int length);
void double_insertion_sort(int *l1, int *l2, int length);
int min(int *l, int length);

void main(void) {
	FILE *fin, *fout;
	int i;
    // represents the current "team"
    int currentStart, currentEnd;
    int longestMilk;
    int longestSleep;

	// open the files
	fin = fopen("milk2.in","r");
	fout = fopen("milk2.out", "w");

	fscanf(fin, "%hu\n", &N);
	for(i = 0;i < N;i++) {
        fscanf(fin,"%d ",&starts[i]);
		fscanf(fin,"%d\n",&ends[i]);
	}

    // for(i = 0;i < N;i++) {
	// 	printf("%d ",starts[i]);
    //     printf("%d\n",ends[i]);
	// }

    double_insertion_sort(starts,ends,N);

    currentStart = starts[0];
    currentEnd = ends[0];
    longestMilk = currentEnd - currentStart;
    longestSleep = 0;
    for(i = 1;i < N;i++) {
        if((starts[i] <= currentEnd) && (ends[i] > currentEnd)) {
            // group current team with current farmer
            currentEnd = ends[i];
        } else if(starts[i] > currentEnd) {
            // our team is complete
            if((currentEnd - currentStart) > longestMilk) {
                longestMilk = currentEnd - currentStart;
            }
            //printf("%d\n",currentStart);
            if((starts[i] - currentEnd) > longestSleep) {
                longestSleep = starts[i] - currentEnd;
            }
            currentStart = starts[i];
            currentEnd = ends[i];
        }
    }
    if((currentEnd - currentStart) > longestMilk) {
        longestMilk = currentEnd - currentStart;
    }
    //printf("%d\n",currentStart);
    if((starts[i] - currentEnd) > longestSleep) {
        longestSleep = starts[i] - currentEnd;
    }

    printf("%d %d\n",longestMilk,longestSleep);
    fprintf(fout,"%d %d\n",longestMilk,longestSleep);

	fclose(fin);
	fclose(fout);

	exit(0);
}

int display(int *l, int n) {
    int i;
    //printf("%d\n",n);
    printf("[");
    for(i = 0;i < n;i++)
        if(i == (n - 1))
            printf("%d",l[i]);
        else
            printf("%d, ",l[i]);
    puts("]");
}

void shiftr(int *l, int length, int start, int end) {
    int i;
    // starting at index start, move all the elements in array l to the right,
    // ending at end, and dumping the last element
    for(i = end - 1;i >= start;i--) {
        l[i+1] = l[i];
    }
}

void shiftl(int *l, int length, int start, int end) {
    int i;
    // starting at index start, move all the elements in array l to the right,
    // ending at end, and dumping the last element
    for(i = start+1;i <= end;i++) {
        l[i-1] = l[i];
    }
}

void insert(int *l, int length, int from, int to) {
    // inserts value at from to to, shifting everything else down
    // TODO - add shiftl
    int v;
    v = l[from];
    shiftr(l,length,to, from);
    l[to] = v;
}

int find_sorted_location(int *l, int length, int value) {
    // assuming forward sorting(leastto greatest), finds out where value
    // should go if to ultimately achieve a sorted list.
    int i;
    i = 0;
    while(i < length && l[i] < value) {
        i++;
    }

    return i;
}

void insertion_sort(int *l, int length) {
    int i,prev,targetloc;
    // loop through the array. if you find that a value is less than
    // the previous value, then move that value to the correct place
    i = 0;
    prev = -1; // TODO - set to min of array
    while(i < length) {
        if(l[i] < prev) {
            targetloc = find_sorted_location(l,length,l[i]);
            insert(l,length,i,targetloc);
            display(l,length);
        } else {
            prev = l[i];
            i++;
        }
    }
}

void double_insertion_sort(int *l1, int *l2, int length) {
    int i,prev,targetloc;
    // loop through the array. if you find that a value is less than
    // the previous value, then move that value to the correct place
    i = 0;
    prev = -1; // TODO - set to min of array
    while(i < length) {
        if(l1[i] < prev) {
            targetloc = find_sorted_location(l1,length,l1[i]);
            insert(l1,length,i,targetloc);
            insert(l2,length,i,targetloc);
            //display(l,length);
        } else {
            prev = l1[i];
            i++;
        }
    }
}

int max(int *l, int length) {
    int k,i;
    k = l[0];
    for(i = 0;i < length;i++) {
        if(l[i] > k)
            k = l[i];
    }

    return k;
}

int min(int *l, int length) {
    int k,i;
    k = l[0];
    for(i = 0;i < length;i++) {
        if(l[i] < k)
            k = l[i];
    }

    return k;
}
