#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

struct Edge {
	int a, b, w;
};

struct Query {
	int k, v, ind;
};

bool cmpEdge(Edge x, Edge y) {
	return x.w > y.w;
}

bool cmpQuery(Query x, Query y) {
	return x.k > y.k;
};

int N, Q;
vector<Edge> edges;
vector<Query> queries;
vector<int> ans;
vector<int> par, sz;

// TODO what does this do?
int find(int x) {
	return par[x] == x ? x : (par[x] = find(par[x]));
}

void merge(int a, int b) {
	int fa = find(a);
	int fb = find(b);
    // cout << fa << " " << fb << endl;
	sz[fb] += sz[fa];
	par[fa] = fb;
}

int main() {
	ifstream fin("mootube.in");
	ofstream fout("mootube.out");
	
	fin >> N >> Q;
	Edge tmp;
	for(int i = 1;i < N;i++) {
		fin >> tmp.a >> tmp.b >> tmp.w;
        // cout << tmp.a << " " << tmp.b << " " << tmp.w << endl;
		edges.push_back(tmp);
	}
	
	sort(edges.begin(), edges.end(), cmpEdge);
	
	for(int i = 0;i < N;i++) {
		par.push_back(i);
		sz.push_back(1);
	}
	
	Query tmp2;
	for (int i = 0;i < Q;i++) {
        fin >> tmp2.k >> tmp2.v;
        // cout << tmp2.k << " " << tmp2.v << endl;
		tmp2.ind = i;
		queries.push_back(tmp2);
	}
	
	sort(queries.begin(), queries.end(), cmpQuery);
	
	ans.resize(Q);
	int idx = 0;
	for (auto query : queries) {
		// cout << query.k << endl;
		while(idx < edges.size() && edges[idx].w >= query.k) {
            // cout << "test" << idx << endl;
            // cout << idx << " ";
            merge(edges[idx].a, edges[idx].b);
			idx++;
		}
        // ans[query.ind] = 1;
        ans[query.ind] = sz[find(query.v)] - 1;
        // cout << ans[query.ind] << endl;
	}
	
	for (int x : ans) {
		// cout << x << endl;
		fout << x << endl;
	}
}
