/*
  ID: kalyan2
  LANG:C++11
  TASK: milk2
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;

const string PROG = "milk2";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

struct Event {
    long time, ind;
    bool isAdd;
};

vector<Event> events;
long N;

bool cmp(auto& a, auto& b) {
    return a.time < b.time;
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N;
    REP(i,0,N) {
        Event ev;
        fin >> ev.time;
        ev.isAdd = true;
        ev.ind = i;
        events.push_back(ev);
        fin >> ev.time;
        ev.isAdd = false;
        ev.ind = i;
        events.push_back(ev);
    }

    sort(events.begin(), events.end(), cmp);
	return 0;
}
