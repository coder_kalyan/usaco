#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

#define MAXN 100000
int L, N, RF, RB, ismax[MAXN] = {}, RLOC[MAXN], RGRASS[MAXN];
//pair<int, int> REST[MAXN];

int main() {
    ifstream fin("reststops.in");
    ofstream fout("reststops.out");

    fin >> L >> N >> RF >> RB;
    for (int i = 0;i < N;i++)
        fin >> RLOC[i] >> RGRASS[i];
        //fin >> REST[i].first >> REST[i].second;

    //for (int i = 0;i <= N;i++) cout << RLOC[i] << " "<< RGRASS[i] << endl;

    //sort(REST, REST + N);
     
    int mx = 0;
    for (int i = N-1;i>=0;i--) {
        if (RGRASS[i] > mx) {
            mx = RGRASS[i];
            ismax[i] = 1;
        } else
            ismax[i] = 0;
    }
    //for (int i = N - 1;i >= 0;i--) {
        //if (RGRASS[i] > max) {
            //max = RGRASS[i];
            //maxi = i;
        //}
        //rightmax[i - 1] = maxi;
        ////rightmax[i] = max(rightmax[i+1], REST[i].second);
    //}
    //for (int i = 0;i < N;i++) cout << rightmax[i] << endl;

    //for (int i = 0;i < N;i++) cout << rightmax[i] << " " ;
    //cout << endl;


    long long ans = 0, tf = 0, tb = 0, prevloc = 0;
    for (int i = 0;i < N;i++) {
        if (ismax[i]) {
            tf += (RLOC[i] - prevloc) * ((long long)RF);
            tb += (RLOC[i] - prevloc) * ((long long)RB);
            ans += (tf - tb) * ((long long)RGRASS[i]);
            tb = tf;
            prevloc = RLOC[i];
        }
    }
    //int curi = -1, curpos, waittime, prevpos = 0, ans;
    //while (curi < N) {
        //curi = rightmax[curi + 1];
        //cout << curi << endl;
        //curpos = RLOC[curi];
        //waittime = (curpos - prevpos) * (RB - RF);
        //ans += waittime * RGRASS[curi];
        //cout << ans << endl;
    //}
    cout << ans << endl;
    fout << ans << endl;

    //int resti = 0;
    //while (resti < N) {
        //cout << resti << " " << rightmax[resti] << endl;
        //resti = rightmax[resti];
    //}

    return 0;
}
