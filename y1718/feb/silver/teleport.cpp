#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;

#define MAXN 100000

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef long double ld;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef pair<ld, ld> dd;
typedef vector<ld> vd;
typedef vd::iterator vd_i;
typedef vector<dd> vdd;
typedef vdd::iterator vdd_i;

const string PROG = "teleport";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

ld N;
ld a[MAXN], b[MAXN];

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N;
    REP(i,0,N)
        fin >> a[i] >> b[i];

    
	return 0;
}
