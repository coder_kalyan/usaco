/*
  ID: kalyan2
  LANG:C++11
  TASK: zerosum
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;

const string PROG = "zerosum";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

int N;
ifstream fin (FIN.c_str());
ofstream fout (FOUT.c_str());

void dfs(int s,int sum,int num,int sign,string str)
{
    // cout << s << endl;
    if(s==N)
    {
        // if(sum+sign*num==0)
        if(sum==0)
        cout << str << endl;
        return;
    }
    dfs(s+1,sum,num*10+(s+1),sign,str+" "+char((s+1)+'0'));
    dfs(s+1,sum+sign*num,s+1,1,str+"+"+char((s+1)+'0'));
    dfs(s+1,sum+sign*num,s+1,-1,str+"-"+char((s+1)+'0'));   
}

int main() {
    fin >> N;
    dfs(1,0,1,1,"1");   
    // compute(1, 0, 1, 1, "1");

	return 0;
}
