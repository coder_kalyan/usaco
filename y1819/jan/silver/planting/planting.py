import pdb

with open("planting.in") as f:
    N = int(f.readline())
    CON = []
    for i in range(N + 1):
        CON.append([])
    for i in range(N - 1):
        a, b = map(int, f.readline().split())
        CON[a].append(b)
        CON[b].append(a)

# print(N)
# print(CON)

ncolors = 1
COL = [0] * (N + 1)

def dfs(cur):
    global ncolors

    # print("->", cur, end=" ")
    # print(COL[cur])
    if COL[cur] != 0:
        # print("already done")
        return
    rel = set()
    rel.add(COL[cur])
    for n1 in CON[cur]:
        rel.add(COL[n1])
        for n2 in CON[n1]:
            rel.add(COL[n2])
        
    # print(cur, ":", rel)
    for c in range(1, ncolors + 1):
        # print(c)
        if c not in rel:
            COL[cur] = c
            # print(c, COL[cur])
            break
    else:
        # none of the existing colors work
        # print("none of the existing colors work")
        ncolors += 1
        COL[cur] = ncolors
    # print(cur, COL[cur])

    # print(COL)
    for nb in CON[cur]:
        dfs(nb)

# root the tree at some random point, like the first one
# print("hi")
dfs(1)
# print("hi")
print(ncolors)
with open("planting.out", "w") as f:
    f.write(str(ncolors))
    f.write("\n")
