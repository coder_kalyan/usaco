/*
ID: kalyan2
LANG: C++
TASK: butter
*/

#include <iostream>
#include <fstream>
#include <map>
#include <set>
#include <vector>
#include <climits>
#include <numeric>

using namespace std;

#define INF INT_MAX
typedef pair<int, int> ii;
typedef vector<ii> vii;

int N, P, C;
int cpast[500];
vii adj[800];
//map<int,int> dist;
int dist[800];


//void dijkstra(int src) {
    //set<ii> visited;
    //visited.insert(make_pair(0,src));
    //while (!visited.empty()) {
        //int i = visited.begin()->second;
        //visited.erase(visited.begin());
        //for (auto j : adj[i]) {
            //int v = j.first, wt = j.second;
            //cout << dist[v] << endl;
            //if (dist[v] > dist[i] + wt) {
                //if (dist[v] != INF)
                    //visited.erase(visited.find(make_pair(dist[v], v)));
                //dist[v] = dist[i] + wt;
                //visited.insert(make_pair(dist[v], v));
            //}
        //}
    //}
//}

void dijkstra(int src) {
    set<ii> visited;
    visited.insert(make_pair(0, src));
    dist[src] = 0;
    while (!visited.empty()) {
        auto tmp = *(visited.begin());
        visited.erase(visited.begin());
        int u = tmp.second;
        for (auto nb : adj[u]) {
            int v = nb.first, wt = nb.second;
            if (dist[v] > dist[u] + wt) {
                if (dist[v] != INF)
                    visited.erase(visited.find(make_pair(dist[v], v)));
                dist[v] = dist[u] + wt;
                visited.insert(make_pair(dist[v], v));
            }
        }
    }
}


int main() {
    ifstream fin("butter.in");
    ofstream fout("butter.out");

    fin >> N >> P >> C;
    for (int i=0;i<N;i++) fin >> cpast[i], cpast[i]--;
    int a, b, c;
    for (int i=0;i<C;i++) {
        fin >> a >> b >> c;
        a--, b--;
        adj[a].push_back(make_pair(b, c));
        adj[b].push_back(make_pair(a, c));
    }

    //for (int i=0;i<N;i++) cout << cpast[i] << endl;

    int ans = INF;
    //dijkstra(1);
    //for (int i=1;i<=P;i++) cout << dist[i] << " ";
    for (int i=0;i<P;i++) {
        fill(dist,dist+P,INF);
        fill(dist,dist+P,INF);
        dijkstra(i);
        int s = 0;
        for (int j=0;j<N;j++)
            s += dist[cpast[j]];
        ans = min(ans, s);
    }

    cout << ans << endl;
    fout << ans << endl;
    return 0;
}
