from bisect import insort
entries = []
board_n = ["Bessie", "Elsie", "Mildred"]
board_v = [7, 7, 7]
num_top = 3

with open("measurement.in") as f:
    N = int(f.readline())
    for i in range(N):
        day, cow, add = f.readline().split()
        insort(entries, [int(day), cow, int(add)])

#    print(entries)

count = 0

for entry in entries:
    old1, old2 = board_v[0], board_v[1]
    old_num_top = num_top
    board_v[board_n.index(entry[1])] += entry[2]
    old = board_n[0]
    z = sorted(zip(board_v, board_n), reverse=True)
    board_v, board_n = map(list, zip(*z))
    num_top = board_v.count(board_v[0])
    print(entry, z, end=" ")
    if board_n[0] != old:
        count += 1
        print("Yes")
    elif old_num_top != num_top:
        count += 1
        print("Yea")
    #  elif board_v[0] == board_v[1]:
        #  if flag == False:
            #  count += 1
            #  print("Yea")
            #  flag = True
        #  else:
            #  print()
    elif old1 == old2 and count == 0:
        count += 1
        print("Yah")
    else:
        print()
    

print(count)
with open("measurement.out", "w") as f:
    f.write(str(count) + "\n")
