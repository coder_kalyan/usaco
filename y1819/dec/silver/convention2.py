# SOLVED 10/10
cows = []
queue = []
with open("convention2.in") as f:
    N = int(f.readline())
    for i in range(N):
        a, t = map(int, f.readline().split())
        cows.append((a, t, i))

cows.sort(key=lambda x:x[0])
time_idx = cows[0][0]
cur = cows[0]
cur_start = time_idx
cow_idx = 1
max_wait = 0
while cow_idx < len(cows): #len(cows) > 0:
    # jump to the end of the current cow's eating
    time_idx += cur[1]

    # check if anyone joined the queue during that time
    while cow_idx < len(cows) and cows[cow_idx][0] <= time_idx:
        if len(queue) > 0:
            i = 0
            while i < len(queue) and cows[cow_idx][2] > queue[i][2]:
                i += 1
            queue.insert(i,cows[cow_idx])
        else:
            queue.append(cows[cow_idx])
        cow_idx += 1

    if len(queue) > 0:
        # if the queue has cows in it, put the highest priority one in the pasture
        cur = queue[0]
        del queue[0]
        #max_wait = max(max_wait, time_idx-cur[0])
        if time_idx - cur[0] > max_wait:
            max_wait = time_idx - cur[0]
    else:
        # queue is empty, so jump to the first cow and put her in the pasture
        cur = cows[cow_idx]
        cow_idx += 1
        time_idx = cur[0]
    
print(max_wait)
with open("convention2.out", "w") as f:
    f.write(str(max_wait))
    f.write("\n")
