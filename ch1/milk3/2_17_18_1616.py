"""
ID: kalyan2
TASK: milk3
LANG: PYTHON3
"""

moves_done = []

def sim_pour(source, dest, cur, _max):
	temp = cur[:]
	if temp[source] == 0:
		return
	if temp[dest] == _max[dest]:
		return

	while (temp[dest] < _max[dest]) and (temp[source] > 0):
		temp[source] -= 1
		temp[dest] += 1
		
	if temp in moves_done:
		return

	moves_done.append(temp[:])

	sim_pour(0, 1, temp, _max)
	sim_pour(0, 2, temp, _max)
	sim_pour(1, 0, temp, _max)
	sim_pour(1, 2, temp, _max)
	sim_pour(2, 0, temp, _max)
	sim_pour(2, 1, temp, _max)

	return

fin = open("milk3.in", "r")
fout = open("milk3.out", "w+")

line = fin.readline()
while line:
	_max = list(map(int, line.split(" ")))
	cur = [0, 0, _max[2]]

	moves_done = [cur[:]]

	sim_pour(2, 0, cur[:], _max)
	sim_pour(2, 1, cur[:], _max)

	# possible = list(map(str,sorted(list({x[2] for x in filter(lambda x: x[0] == 0, moves_done)}))))
	possible = []
	for x in moves_done:
		if x[0] == 0:
			possible.append(x[2])

	possible = sorted(possible)
	possible = list(map(str, possible))

	print(" ".join(possible))
	fout.write(" ".join(possible))
	line = fin.readline()
