"""
ID: kalyan2
TASK: skidesign
LANG: PYTHON3             
"""

with open("skidesign.in") as f:
	N = int(f.readline())
	hills = []
	for i in range(N):
		hills.append(int(f.readline()))



hills = sorted(hills)
print(hills)

if max(hills) - min(hills) <= 17:
	print(0)
	with open("skidesign.out", "w") as f:
		f.write("0")
		f.write("\n")
	exit(0)

def getCost(tMin):
	# tMin = minHill
	tMax = tMin + 17 # hills[0] + 17
	cost = 0
	for i, hill in enumerate(hills):
		if hill < tMin:
			cost += (tMin - hill) ** 2
		elif hill > tMax:
			cost += (hill - tMax) ** 2

	return cost

costs = []
for i in range(hills[0], hills[-1] - 18):
	cost = getCost(i)
	costs.append(cost)

print(costs)
print(min(costs))

with open("skidesign.out", "w") as f:
	f.write(str(min(costs)))
	f.write("\n")
