#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>
#include <climits>
#include <cmath>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define RREP(i,a,b) for (long i=b-1;i>=a;i--)
#define LOG(a,b) clog << a << ": " << b << endl;
#define MP(a,b) make_pair(a,b)
#define DIST(x1,y1,x2,y2) hypot(abs(x2-x1),abs(y2-y1))

#define DEBUG 1

#ifdef DEBUG
#define COUT(str) cout str;
#else
#define COUT(str) {}
#endif


struct cmp;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;
typedef priority_queue<ii> pqiif;
typedef priority_queue<ii,vii,cmp> pqiis;
typedef vector<vi> vvi;
typedef vvi::iterator vvi_i;
typedef vector<vii> vvii;
typedef vvii::iterator vvii_i;

struct State {
    int ind, enj, prio;
};

struct cmp {
    bool operator()(State a, State b) {
        return a.prio > b.prio;
    }
};

#define INF INT_MAX

#define MAXN 100000
// #define MAXN 100

const string PROG = "cowland";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N, Q;
// vii myadj[MAXN+1];
vector<vii> myadj;
int enj[MAXN+1];//, dis[MAXN+1];
// bool vis[MAXN];
bitset<MAXN> vis;

long bfs(long src, long dest) {
    long ret = 0;
    // fill(vis,vis+MAXN,false);
    vis.reset();
    priority_queue<State,vector<State>,cmp> q;
    State st;
    st.ind = src;
    st.enj = enj[src];
    st.prio = 0;
    q.push(st);
    while (!q.empty()) {
        State curr = q.top();
        q.pop();
        if (vis[curr.ind])
            continue;
        // cout << curr.first << " " << curr.second << endl;
        vis[curr.ind] = true;
        if (curr.ind == dest) {
            ret = curr.enj;
            break;
        }
        myadj[src].push_back(MP(curr.ind,curr.enj));
        myadj[curr.ind].push_back(MP(src,curr.enj));
        // ii tmp;
        // long myind = curr.ind;
        for (ii nb : myadj[curr.ind]) {
            // cout << "NB" << nb << " " << curr.second << " " << enj[nb] << endl;
            st.ind = nb.first;
            st.enj = curr.enj ^ enj[nb.first];
            st.prio = nb.second;
            q.push(st);
            // tmp = MP(nb.first, nb.second + 1);
            // myadj[curr.ind].push_back(tmp);
            // myadj[src].push_back(tmp);
            // myadj[curr.ind].push_back(MP(nb.first, nb.second + 1));
            // cout << MP(nb.first, nb.second + 1).first;
            // cout << myadj[curr.ind].size();
            // myadj[curr.ind].push_back(make_pair(0,0));
            // myadj[src].push_back(make_pair(0,0));
            // if (src != curr.ind) myadj[src].push_back(MP(curr.ind, nb.second + 1));
        }
    }

    return ret;
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N >> Q;
    REP(i,1,N+1)
        fin >> enj[i];
    int s, a, b;
    myadj.resize(MAXN + 10);
    REP(i,1,N) {
        fin >> a >> b;
        myadj[a].push_back(MP(b,0));
        myadj[b].push_back(MP(a,0));
    }
    // REP(i,1,N) {
        // for (int j : myadj[i])
            // cout << j << " ";
        // cout << endl;
    // }
    // cout << endl;
    
    // fout << "TEST" << endl;
    long ans;
    REP(i,0,Q) {
        fin >> s >> a >> b;
        if (s == 2) {
            ans = bfs(a,b);
            cout << ans << endl;
            fout << ans << endl;
        } else {
            enj[a] = b;
        }
    }
	return 0;
}
