/*
ID: kalyan2
TASK: wormhole
LANG: C++                 
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

struct Hole {
	long id;
	long x;
	long y;
	long onEntry;
	long onExit;
};

typedef pair<long, long> ii;
typedef vector<ii> vii;
typedef vector<long> vi;
typedef struct Hole Hole;
typedef vector<Hole> vh;

long N;		// number of wormholes - 2 <= N <= 12, N even
vector<vii> connections;
vh holes;
long numPermsDone;

void display_holes() {
	// cout << holes.size() << endl;
	for(long i = 0;i < holes.size(); i++) {
		// cout << holes[i].id << " ";
		// cout << holes[i].onEntry << "," << holes[i].onExit << " ";
		printf("%ld | %ld | %ld\n", holes[i].id, holes[i].onEntry, holes[i].onExit);
	}
	cout << endl;
}

void permute(vii& paired, vi& nums_left) {
	long f = nums_left[0];
	for(long i = 1; i < nums_left.size(); i++) {
		long current = nums_left[i];
		vii p2;
		vi n2;
		for(long j = 0;j < paired.size(); j++) {
			p2.push_back(paired[j]);
		}
		pair<long, long> p = make_pair(f, current);
		//onn conn;
		//conn.a = f;
		//conn.b = current;
		p2.push_back(p);
		for(long j = 0;j < nums_left.size(); j++) {
			if(nums_left[j] != f && nums_left[j] != current)
				n2.push_back(nums_left[j]);
		}
		if(n2.size() > 0) {
			permute(p2, n2);
		} else {
			connections.push_back(p2);
			for(long i = 0;i < p2.size(); i++) {
				// cout << "(" << p2[i * 2] << ", " << p2[i * 2 + 1] << "), ";
				// prlongf("%d <-> %d\n", p2[i].first, p2[i].second);
			}
			// cout << endl;
		}
	}
}


void generatePairs() {
	vii pairs;
	vi nums_left;
	for(long i = 1;i <= N;i++) {
		nums_left.push_back(i);
	}
	permute(pairs, nums_left);
}

Hole* getHoleById(long id) {
	vh::iterator it;
	for(it = holes.begin();it != holes.end();it++) {
		if((*it).id == id) {
			return &(*it);
		}
	}

	return NULL;
}

Hole* getHoleByCoord(long x, long y) {
	vh::iterator it;
	for(it = holes.begin();it != holes.end();it++) {
		if((*it).x == x && (*it).y == y) {
			return &(*it);
		}
	}

	return NULL;
}

bool cmpByX(const Hole &a, const Hole &b) {
	return a.x < b.x;
}

bool setup_next_permutation() {
	if(numPermsDone == connections.size())
		return false;

	// lets first set up based on just generated wormhole connections
	vii permutation = connections[numPermsDone];
	for(long i = 0;i < permutation.size();i++) {
		ii curConn = permutation[i];
		(*getHoleById(curConn.first)).onEntry = curConn.second;
		(*getHoleById(curConn.first)).onExit = -1;
		(*getHoleById(curConn.second)).onEntry = curConn.first;
		(*getHoleById(curConn.second)).onExit = -1;
	}
	// now we need to add in connections for Bessie's +x moving
	for(long i = 0;i < holes.size();i++) {
		vh adjacent;
		for(long j = 0;j < holes.size();j++) {
			if(j == i)
				continue;
			// printf("%ld:%ld ",holes[j].x, holes[i].x);
			if(holes[j].y == holes[i].y && holes[j].x > holes[i].x) {
				adjacent.push_back(holes[j]);
				// cout << "y ";
				// cout << holes[j].id;
			}
			// cout << "| ";
		}
		//cout << endl;
		sort(adjacent.begin(), adjacent.end(), cmpByX);
		//cout << adjacent.size() << " ";
		for(int j = 0;j < adjacent.size();j++) {
			//cout << adjacent[j].id;
			// printf("%ld:%ld | ", adjacent[j].id, adjacent[j].x);
		}
		// cout << endl;
		if(adjacent.size() > 0)
			holes[i].onExit = adjacent[0].id;

	}
	numPermsDone++;
	return true;
}

bool walk(long index) {
	bool isEntry = true;
	long start = holes[index].id;
	long id = start;
	bool isWalk;
	while(true) {
		if(isEntry) {
			long temp = (*getHoleById(id)).onEntry;
			printf("%ld -> %ld\n",id, temp);
			id = temp;
		} else {
			long temp = (*getHoleById(id)).onExit;
			printf("%ld -> %ld\n",id, temp);
			id = temp;
			if(id == -1) {
				isWalk = false;
				break;
			}
		}
		if((*getHoleById(id)).id == start && !isEntry) {
			// we found a cycle
			isWalk = true;
			break;
		}
		isEntry = !isEntry;
		
	}
	return isWalk;
}
bool isCycle() {
	for(long i = 0;i < holes.size();i++) {
		if(walk(i)) {
			cout << "Found a cycle!" << endl;
			return true;
		}
	}

	return false;
}
int main() {
	// generatePairs();
	ifstream fin;
	fin.open("wormhole.in");

	fin >> N;
	// cout << N;

	// read in the wormholes
	// even though our main algorithm doesn't use wormhole position, we need it
	// for checking for wormholes on the same line

	// first just read in the wormholes
	for(long i = 1;i <= N;i++) {
		Hole hole;
		hole.id = i;
		hole.onEntry = -1;
		hole.onExit = -1;
		fin >> hole.x >> hole.y;
		holes.push_back(hole);
	}

	fin.close();
	
	numPermsDone = 0;
	generatePairs();
	/*for(long i = 0;i < connections.size();i++) {
		for(long j = 0;j < connections[i].size();j++) {
			prlongf("(%d, %d), ",connections[i][j].first, connections[i][j].second);
		}
		cout << endl;
	}*/
	long total = 0;
	while(setup_next_permutation()) {
		display_holes();
		if(isCycle()) {
			total++;
			// cout << "Found cycle!" << endl;
		}
	}

	cout << "Total cycles: " << total << endl;
	ofstream fout;
	fout.open("wormhole.out");
	fout << total << endl;
	fout.close();
	return 0;
}
