#include <iostream>
#include <fstream>
#include <map>
#include <string>

using namespace std;

int N;
map<string, int> a, b;
int main() {
    //ifstream fin("tshirts.in");
    //ofstream fout("tshirts.out");

    //fin >> N;
    cin >> N;
    string tmp;
    for (int i = 0;i < N;i++) {
        //fin >> tmp;
        cin >> tmp;
        a[tmp] = a[tmp] + 1;
    }
    for (int i = 0;i < N;i++) {
        cin >> tmp;
        b[tmp] = b[tmp] + 1;
    }
    int ans = 0;
    for (auto it = a.begin();it != a.end();it++) {
        ans += max(it->second - b[it->first], 0);
    }

    cout << ans << endl;
    //fout << ans << endl;
    return 0;
}
