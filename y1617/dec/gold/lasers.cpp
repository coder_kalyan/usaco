#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>
#include <climits>
#include <cmath>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define RREP(i,a,b) for (long i=b-1;i>=a;i--)
#define LOG(a,b) clog << a << ": " << b << endl;
#define MP(a,b) make_pair(a,b)
#define DIST(x1,y1,x2,y2) hypot(abs(x2-x1),abs(y2-y1))

struct Cmp2;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;
typedef priority_queue<ii> pqiif;
typedef priority_queue<ii,vii,Cmp2> pqiis;
typedef vector<vi> vvi;
typedef vvi::iterator vvi_i;
typedef vector<vii> vvii;
typedef vvii::iterator vvii_i;

struct Cmp {
    bool operator()(ii a, ii b) {
        return a.first < b.first;
    }
};

struct Cmp2 {
    bool operator()(ii a, ii b) {
        return a.second > b.second;
    }
};

typedef multiset<ii, Cmp> set_t;
#define INF INT_MAX
#define MAXN 100000

const string PROG = "lasers";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N, XL, YL, XB, YB;
vii posts;
set_t postx, posty;
long dis[MAXN];
// bool vis[MAXN];

long bfs(bool flip) {
    queue<pair<int,bool>> q;
    q.push(MP(0,flip));
    // fill(vis,vis+N,0);
    fill(dis,dis+MAXN,-1);
    dis[0] = 0;
    while (!q.empty()) {
        ii curr = q.front();
        q.pop();
        int curri = curr.first;
        // int currn = posts[curr.first].first, curri = posts[curr.first].second;
        // cout << curr.second << " - " << currn << " " << curri << ":" << endl;
        // if (dis[curri] != -1) continue;

        if ((curr.second)) {
            int currn = posts[curri].first;
            for (set_t::iterator it=postx.lower_bound(MP(currn,0));
                    it != postx.upper_bound(MP(currn,0));
                    it++) {
                if (dis[it->second] != -1) continue;
                dis[it->second] = dis[curri] + 1;
                // cout << posts[it->second].first << endl;
                // cout << posts[it->second].first<<" "<<posts[it->second].second<<endl;
                q.push(MP(it->second,!curr.second));
                // cout << it->first << " " << it->second << endl;
            }
            // cout << postx.count(MP(7,5)) << endl;
            // cout << endl;
        } else {
            int currn = posts[curri].second;
            for (set_t::iterator it=posty.lower_bound(MP(currn,0));
                    it != posty.upper_bound(MP(currn,0));
                    it++) {
                if (dis[it->second] != -1) continue;
                dis[it->second] = dis[curri] + 1;
                // cout << posts[it->second].first << endl;
                // cout << posts[it->second].first<<" "<<posts[it->second].second<<endl;
                q.push(MP(it->second,!curr.second));
                // q.push(it->second);
                // cout << it->first << " " << it->second << endl;
            }
            // cout << endl;
        }
    }

    return dis[N+1] - 1;
}


int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N >> XL >> YL >> XB >> YB;
    int tmp1, tmp2;
    postx.insert(MP(XL,0));
    posty.insert(MP(YL,0));
    posts.push_back(MP(XL,YL));
    int i;
    for (i = 1;i <= N;i++) {
        fin >> tmp1;
        // cout << tmp1 << " " << i << endl;
        postx.insert(MP(tmp1,i));
        fin >> tmp2;
        posty.insert(MP(tmp2,i));
        posts.push_back(MP(tmp1,tmp2));
        // fin >> posts[i].first >> posts[i].second;
    }
    postx.insert(MP(XB,i));
    posty.insert(MP(YB,i));
    posts.push_back(MP(XB,YB));
    // for (ii post : postx) {
        // cout << post.first << " " << post.second << endl;
    // }
    // cout << endl;
    // for (ii post : posty) {
        // cout << post.first << " " << post.second << endl;
    // }
    // cout << endl;

    // for (set_t::iterator it=postx.lower_bound(MP(XL,0));
            // it != postx.upper_bound(MP(XL,0));
            // it++) {
        // cout << posts[it->second].first<<" "<<posts[it->second].second<<endl;
        // // cout << it->first << " " << it->second << endl;
    // }
    // cout << "--" << endl;
    // for (set_t::iterator it=posty.lower_bound(MP(YL,0));
            // it != posty.upper_bound(MP(YL,0));
            // it++) {
        // cout << posts[it->second].first<<" "<<posts[it->second].second<<endl;
        // // cout << it->first << " " << it->second << endl;
    // }

    int ans = min(bfs(true), bfs(false));
    ans = max(ans, -1);
    cout << ans << endl;
    fout << ans << endl;
    // cout << bfs(true) << endl;
    // cout << bfs(false) << endl;
	return 0;
}
