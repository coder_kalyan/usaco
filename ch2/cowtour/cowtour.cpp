/*
  ID: kalyan2
  LANG:C++11
  TASK: cowtour
*/

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>
#include <climits>
#include <cmath>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define RREP(i,a,b) for (long i=b-1;i>=a;i--)
#define LOG(a,b) clog << a << ": " << b << endl;
#define MP(a,b) make_pair(a,b)
#define DIST(x1,y1,x2,y2) hypot(abs(x2-x1),abs(y2-y1))

class cmp;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;
typedef priority_queue<ii> pqiif;
typedef priority_queue<ii,vii,cmp> pqiis;
typedef vector<vi> vvi;
typedef vvi::iterator vvi_i;
typedef vector<vii> vvii;
typedef vvii::iterator vvii_i;

class cmp {
    bool operator() (const ii &a, const ii &b) {
        return a.second > b.second;
    }
};

#define INF INT_MAX

#define MAXN 150

const string PROG = "cowtour";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

int N;
ii locs[MAXN];
vector<pair<int, double> > adj[MAXN];
int group[MAXN];
double mat[MAXN][MAXN];
int s;

double dist(int a, int b) {
    double d1 = abs(locs[a].first - locs[b].first);
    double d2 = abs(locs[a].second - locs[b].second);
    return sqrt((d1*d1)+(d2*d2));
}

void floodFill(int i, int g) {
    s++;
    for (auto vert : adj[i]) {
        if (group[vert.first] == 0) {
            group[vert.first] = g;
            floodFill(vert.first, g);
        }
    }
}

void floyd() {
    REP(k,0,N)
        REP(i,0,N)
            REP(j,0,N)
                if (mat[i][k]+mat[k][j]<mat[i][j])
                    mat[i][j] = mat[i][k]+mat[k][j];
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N;
    REP(i,0,N)
        fin >> locs[i].first >> locs[i].second;
    char tmp;
    REP(j,0,N)
        REP(i,0,N) {
            fin >> tmp;
            if (tmp == '1') {
                double d = dist(i,j);
                adj[i].push_back(MP(j,d));
                mat[i][j] = d;
            } else {
                mat[i][j] = INF;
            }
            // mat[j][i] = tmp - 48;
        }

    // REP(i,0,N) {
        // cout << i << ": ";
        // for (auto v : adj[i])
            // cout << "("<<v.first<<","<<v.second<<") ";
        // cout << endl;
    // }
    // REP(j,0,N) {
        // REP(i,0,N)
            // cout << mat[j][i] << " ";
        // cout << endl;
    // }

    // fill(group,group+N,0);
    // int g = 1;
    // vi gSize;
    // REP(i,0,N)
        // if (group[i] == 0) {
            // s = 0;
            // floodFill(i,g);
            // gSize.push_back(s-1);
            // g++;
        // }
    // for (auto size : gSize)
        // cout << size << endl;
    // cout << endl;
    // REP(i,0,N)
        // cout << group[i] << endl;
    floyd();
    REP(j,0,N) {
        REP(i,0,N) {
            if (mat[j][i] < INF)
                cout << setw(8) << mat[j][i];
            else
                cout << setw(8) << "INF";
        }
        cout << endl;
    }

    int ind = 0;
    double m = 0;
    int x, y;
    vii best;
    // for (auto size : gSize) {
        // REP(j,ind,ind+size) {
            // REP(i,ind,ind+size) {
                // if (mat[j][i] > m)
                    // m = max(m,mat[j][i]);
            // }
        // }
        // ind += size;
    // }
    // REP(n,0,g) {
	return 0;
}
