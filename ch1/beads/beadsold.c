/* USACO Program
ID: kalyan2
LANG: C
TASK: beads
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned char compare(char direction,char a,char b) {
	// direction: 1 is forward, 2 is backward
	/*if(a == 'w' || b == 'w' || a == b)
		return 1;
	else
		return 0;*/
	if(a == b)
		return 1;
	else if(direction == 1 && b == 'w')
		return 2;
	else if(direction == 2 && a == 'w')
		return 3;
	else
		return 0;
}

void main(void) {
	FILE *fin, *fout;
	unsigned short N;
	char beads[350];
	unsigned short numStart,numEnd,highestTotal;
	unsigned short loop;

	// open the files
	fin = fopen("beads.in","r");
	fout = fopen("beads.out", "w");

	// read in the value of  and the beads string
	fscanf(fin, "%hu\n", &N);
	fscanf(fin,"%s\n",beads);

	//printf("%hu\n",N);
	//printf("%s\n",beads);

	highestTotal = 0;
	for(loop = 0;loop < N;loop++) {
		numStart = countForward(beads,N);
		numEnd = countBackward(beads,N);
		printf("%s\n",beads);
		if(numStart + numEnd > highestTotal) {
			highestTotal = numStart + numEnd;
			printf("%hu %hu\n",numStart,numEnd);
		}
		shift(beads,N);
	}

	printf("\n%hu\n",highestTotal);
	//printf("%s\n",beads);
	//shift(beads,N);
	//printf("%s\n",beads);
	//shift(beads,N);
	//printf("%s\n",beads);
	exit(0);
}
