/*
ID: kalyan2
LANG: C
TASK: ride
*/



#include <stdio.h>
#include <stdlib.h>

void main(void) {
	/* input and output files */
    FILE *fin  = fopen("ride.in", "r");
    FILE *fout = fopen("ride.out", "w");
    
    
    int a, b;
    fscanf(fin, "%d %d", &a, &b);	/* the two input integers*/
    fprintf(fout, "%d\n", a+b);
    exit(0);
}
