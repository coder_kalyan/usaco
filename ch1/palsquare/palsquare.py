"""
ID: kalyan2
LANG: PYTHON3
TASK: palsquare
"""

def is_palindrome(s):
	return s == s[::-1]

chars = [str(x) for x in range(0, 10)]
chars.extend([chr(x) for x in range(65, 75)])
def convert_base(n, base):
	left = n
	converted = ""
	while left >= base:
		converted  = chars[left % base] + converted
		left //= base
	converted = chars[left] + converted
	return converted

with open("palsquare.in", "r") as f:
	base = int(f.readline())
l = [[x,x**2] for x in range(1, 301) if is_palindrome(convert_base(x**2, base))]
with open("palsquare.out", "w") as f:
	for n in l:
		print("{} {}".format(convert_base(n[0], base), convert_base(n[1], base)))
		# print("{} {}".format(n[0], n[1]))
		f.write("{} {}\n".format(convert_base(n[0], base), convert_base(n[1], base)))