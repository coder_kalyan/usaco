/*
  ID: kalyan2
  LANG:C++11
  TASK: runround
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>

using namespace std;

typedef long long ll;
typedef unsigned long ul;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const string PROG = "runround";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

ul M;

bool isRun(ul n) {
    char digits[9];     // ans always fits into unsigned long, 2^32 is 9 decimal digits
    short nDig = sprintf(digits, "%lu", n);
    bool seen[10];  // seen only needs 9 dig but array is reused for unique checker
    fill(seen, seen + 10, false);
    short pos = 0, conv, unused = nDig;
    seen[0] = true;
    short keepAlive = 1;
    // checks that all digits are unique
    for (int i = 0;i < nDig;i++) {
        cout << digits[i] << " " << seen[digits[i]] << endl;
        if (!seen[digits[i]])
            seen[digits[i]] = true;
        else
            return false;
    }
    cout << "Unique" << endl;
    fill(seen, seen + 10, false);

    while (true) {
        conv = (short) digits[pos] - 48;
        if (conv == 0)
            return false;
        pos = (pos + conv) % (nDig);
        unused--;
        // cout << (unused > 0) << endl;
        if (!seen[pos])
            seen[pos] = true;
        else if (unused > 0)
            return false;
        else
            return true;
    }

    // cout << keepAlive << endl;
    // if (keepAlive == -1)
        // return false;
    // else
        // return true;
}

int main() {
	ifstream fin (FIN);
    fin >> M;

    bool keepAlive = true;
    // do {
        // M++;
        // keepAlive = !isRun(M);
       // cout << M << " ";
    // } while(keepAlive);
    while (!isRun(++M)) {}
    cout << M << endl;

	ofstream fout (FOUT);
    fout << M << endl;

	return 0;
}
