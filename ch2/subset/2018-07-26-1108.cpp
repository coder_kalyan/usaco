#include <iostream>
#include <fstream>

using namespace std;

typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef long long ll;

int divide(int N, vi left);

short N;
short sig, halfsig;

int main(void)
{
	ifstream fin;
  ofstream fout;

	fin.open("subset.in");

  fin >> N;

  fin.close();
  cout << N << endl;

  sig = N * (N + 1) / 2;

  // the objective is to devide the set {1..N}
  // into subsets of equal sum
  // these subsets should all equal half of
  // the sum of the original set (which itself can be calculated using sigma N)
  // as a result of this, if sigma N is not even, there will be no answers
  if (sig % 2 == 1) {
    cout << 0 << endl;
    fout.open("subset.out");
    fout << 0 << endl;
    fout.close();
  }

  halfsig = sig / 2;    // this value can be used to check if subset is half

	return 0;
}

// now our objective is to find all subsets of {1..N} that
// equal halfsig

int divide(int N, vi left) {
  if (N == halfsig) {
    // hooray! we have found an answer!
    return
  }
}
