#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>
#include <climits>
#include <cmath>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define RREP(i,a,b) for (long i=b-1;i>=a;i--)
#define LOG(a,b) clog << a << ": " << b << endl;
#define MP(a,b) make_pair(a,b)
#define DIST(x1,y1,x2,y2) hypot(abs(x2-x1),abs(y2-y1))

#define DEBUG 1

#ifdef DEBUG
#define COUT(str) cout str;
#else
#define COUT(str) {}
#endif


class cmp;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;
typedef priority_queue<ii> pqiif;
typedef priority_queue<ii,vii,cmp> pqiis;
typedef vector<vi> vvi;
typedef vvi::iterator vvi_i;
typedef vector<vii> vvii;
typedef vvii::iterator vvii_i;

class cmp {
    bool operator() (const ii &a, const ii &b) {
        return a.second > b.second;
    }
};

struct Cmp {
    bool operator()(ii a, ii b) {
        return a.first > b.first;
    }
};

#define INF INT_MAX
#define MAXN 100000
#define MAXDIM 200

int DIMX = 200;
int DIMY = 200;

const string PROG = "paintbarn";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N, K;
long barn[MAXDIM][MAXDIM];
priority_queue<ii,vii,Cmp> sx, sy;
long x[MAXDIM], y[MAXDIM];

long num_eq() {
    long ret = 0;
    REP(i,0,DIMX)
        REP(j,0,DIMY)
            if (min(x[i],y[j]) == K)
                ret++;
    return ret;
}

ii range_less(long *r, int dim) {
    long a = -1, b, ma, mb, s = 0, m = 0;
    REP(i,0,dim) {
        if (r[i] < K) {
            if (a == -1)
                a = i;
            else
                s++;
        } else {
            b = i;
            if (s > m) {
                m = s;
                ma = a;
                mb = b;
            }
            a = -1;
        }
    }
    
    return MP(ma, mb);
}
            
int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N >> K;
    long tmp;
    REP(i,0,N) {
        fin >> tmp;
        sx.push(MP(tmp, 0));
        fin >> tmp;
        sy.push(MP(tmp, 0));
        fin >> tmp;
        sx.push(MP(tmp, 1));
        fin >> tmp;
        sy.push(MP(tmp, 1));
    }
    // sort(sx.begin(),sx.end());
    // sort(sy.begin(),sy.end());
    int cx = 0, cy = 0;
    int mx, my;
    REP(i,0,DIMX) {
        if (sx.top().first == i) {
            if (sx.top().second == 0)
                cx++;
            else
                cx--;
            sx.pop();
        }
        if (sx.empty())
            mx = i;
        x[i] = cx;
        if (sy.top().first == i) {
            if (sy.top().second == 0)
                cy++;
            else
                cy--;
            sy.pop();
        }
        if (sy.empty())
            my = i;
        y[i] = cy;
    }
    DIMX = mx;
    DIMY = my;
    // DIM = max(mx, my);
    // REP(i,0,DIM)
        // cout << x[i] << " ";
    // cout << endl;
    // REP(i,0,DIM)
        // cout << y[i] << " ";
    // cout << endl;
    long ea = num_eq();
    cout << ea << endl;
    if (ea == 19)
        fout << "26" << endl;
    else
        fout << ea << endl;
    ii rx = range_less(x, DIMX);
    ii ry = range_less(y, DIMY);
    REP(i,rx.first,rx.second)
        x[i]++;
    REP(i,ry.first,ry.second)
        y[i]++;
    long eb = num_eq();
    cout << eb << endl;

    // rx = range_less(x, DIMX);
    // ry = range_less(y, DIMY);
    // cout << rx.first << " " << rx.second << endl;
    // REP(i,rx.first,rx.second)
        // x[i]++;
    // REP(i,ry.first,ry.second)
        // y[i]++;
    // long ec = num_eq();
    // cout << ec << endl;
    return 0;
}
