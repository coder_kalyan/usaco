/*
ID: kalyan2
LANG: C++
TASK: ratios
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <queue>
#include <algorithm>
#include <cmath>
#include <map>
#include <climits>
#include <string>

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef vector<int> vi;
#define INF 1000000000

int givens[4][3];

bool valid(double a, double b, double c) {
    if (a == 0) return false;
    if (a == 4 && b == 4) cout << a << " " << b << " " << c << endl;
    if (!(a==b || givens[0][0]==0||givens[0][1]==0) ||
        !(b==c || givens[0][1]==0||givens[0][2]==0)) return false;
    if (a == 4 && b == 4) cout << "got" << endl;
    return true;
    //return a == (int)a;
    //cout << "hi" << endl;
    //if (givens[0][0] == 0 && a != 0) return false;
    //if (givens[0][1] == 0 && b != 0) return false;
    //if (givens[0][2] == 0 && c != 0) return false;

    //if (a % givens[0][0] != 0) return false;
    //if (b % givens[0][1] != 0) return false;
    //if (c % givens[0][2] != 0) return false;


    //bool check1 = givens[0][0] == 0 ? true : (a / givens[0][0] == b
    //return (a / givens[0][0] == b / givens[0][1])
        //&& (b / givens[0][1] == c / givens[0][2]);
}

int main() {
    ifstream fin("ratios.in");
    ofstream fout("ratios.out");

    for (int i=0;i<4;i++)
        for (int j=0;j<3;j++)
            fin >> givens[i][j];

    int ans = INF;
    int best[4];
    for (int a=0;a< 100;a++) {
        for (int b=0;b< 100;b++) {
            for (int c=0;c< 100;c++) {
                //if (a == 0 && b == 0 && c == 0) continue;
                double h = 1.0*(a*givens[1][0]+b*givens[2][0]+c*givens[3][0])/givens[0][0];
                double j = 1.0*(a*givens[1][1]+b*givens[2][1]+c*givens[3][1])/givens[0][1];
                double k = 1.0*(a*givens[1][2]+b*givens[2][2]+c*givens[3][2])/givens[0][2];
                //if (a==3&&b==5&&c==1) cout << (1*givens[0][2]) << endl;
                if (a==3&&b==5&&c==1) cout << (h) << endl;
                //int a1 = givens[1][0]*a, b1 = givens[1][1]*a, c1 = givens[1][2]*a;
                //int a2 = givens[2][0]*b, b2 = givens[2][1]*b, c2 = givens[2][2]*b;
                //int a3 = givens[3][0]*c, b3 = givens[3][1]*c, c3 = givens[3][2]*c;
                //if (valid(a1+a2+a3,b1+b2+b3,c1+c2+c3)) {
                if (valid(h,j,k)) {
                    if (a+b+c < ans) {
                        ans = a + b + c;
                        best[0] = a;
                        best[1] = b;
                        best[2] = c;
                        best[3] = h; //(a+a2+a3)/givens[0][0];
                    }
                }
            }
        }
    }

    if (ans == INF) {
        fout << "NONE" << endl;
        return 0;
    }

    for (int i=0;i<4;i++)
        fout << best[i] << " ";
    fout << endl;
    //cout << ans << endl;
    
    return 0;
}
