#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;

#define INRANGE(a,b,c) (c >= a) && (c <= b)

#define MAXN 20000

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;

const string PROG = "helpcross";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long C, N;
long cknTime[MAXN], cknUse[MAXN];
bitset<MAXN> cknAvail; 
ii cowTime[MAXN];
vi cowOpt[MAXN];

bool cmpOptAmt(vi a, vi b) {
    return a.size() < b.size();
}

long getChoice(long i) {
    long m = -1;
    // cout << endl;
    for (auto it : cowOpt[i]) {
        // cout << cknAvail[it] << endl;
        if (cknAvail[it])
            if ((m == -1) || (it < m))
                m = it;
    }
    // cout << endl;

    return m;
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> C >> N;
    REP(i,0,C)
        fin >> cknTime[i];
    REP(i,0,N)
        fin >> cowTime[i].first >> cowTime[i].second;
    REP(i,0,N)
        REP(j,0,C)
            if (INRANGE(cowTime[i].first,cowTime[i].second,cknTime[j]))
                cowOpt[i].push_back(j);

    sort(cowOpt,cowOpt+N,cmpOptAmt);

    // REP(i,0,N) {
        // REP(j,0,cowOpt[i].size())
            // cout << cowOpt[i][j] << " ";
        // cout << endl;
    // }

    // fill(cknAvail,cknAvail+C,1);
    cknAvail.set();

    long ans = 0;
    REP(i,0,N) {
        long choice = getChoice(i);
        if (choice == -1) {
            // welp no available chickens for this cow
            // too bad :(
            continue;
        } else {
            // we have found a pair! (i,choice)
            // make sure the chicken is marked
            // as unavailable
            cknAvail[choice] = 0;
            ans++;
        }
    }
    
    cout << ans << endl;
    fout << ans << endl;
	return 0;
}
