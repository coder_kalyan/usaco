#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;

const string PROG = "mountains";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N;
vii mountains;

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N;
    mountains.resize(N);
    REP(i,0,N)
        fin >> mountains[i].second >> mountains[i].first;

    sort(mountains.begin(),mountains.end());
    long x1, y1, x2, y2;
    long i = N;
    vii_i beg = mountains.begin();
    while(i > 1) {
        x1 = mountains[i].second;
        y1 = mountains[i].first;
        for (long j = i-1;j>=0;j--) {
            x2 = mountains[j].second;
            y2 = mountains[j].first;
            if (abs(x2-x1)<=(y1-y2)) {
                // invisible
                mountains.erase(beg+j);
                j--;
                i--;
            }
        }
        i--;
    }

    int s = mountains.size();
    cout << s << endl;
    fout << s << endl;

	return 0;
}
