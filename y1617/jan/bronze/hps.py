from itertools import *

HOOF = 10
PAPER = 15
SCISSORS = 20

games = []

with open("hps.in") as f:
    N = int(f.readline())
    for i in range(N):
        a, b = map(int, f.readline().split())
        games.append([a, b])

def compare(a, b):
    if a == HOOF and b == SCISSORS:
        return 0
    elif b == HOOF and a == SCISSORS:
        return 1
    elif a == SCISSORS and b == PAPER:
        return 0
    elif b == SCISSORS and a == PAPER:
        return 1
    elif a == PAPER and b == HOOF:
        return 0
    elif b == PAPER and a == HOOF:
        return 1
    else:
        return -1

def replace(x, y, z):
    new = [[i, j] for i, j in games]
    for i in range(N):
        if games[i][0] == 1:
            new[i][0] = x
        elif games[i][0] == 2:
            new[i][0] = y
        elif games[i][0] == 3:
            new[i][0] = z
        
        if games[i][1] == 1:
            new[i][1] = x
        elif games[i][1] == 2:
            new[i][1] = y
        elif games[i][1] == 3:
            new[i][1] = z

    return new
        
max_s = 0
for comb in permutations([HOOF, PAPER, SCISSORS]):
    result = replace(*comb)
    s = 0
    for r in result:
        if compare(*r) == 0:
            s += 1
    if s > max_s:
        max_s = s

print(max_s)
with open("hps.out", "w") as f:
    f.write(str(max_s) + "\n")
