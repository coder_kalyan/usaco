#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>

using namespace std;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const string PROG = "rental";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

long N, M, R;
map<int, pair<int, int>> cows;

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    fin >> N >> M >> R;
    long tmp;
    for (long i = 0;i < N;i++) {
        fin >> tmp;
        cows.insert(make_pair(tmp,make_pair(0,0)));
    }


	return 0;
}
