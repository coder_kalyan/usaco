/* ID: kalyan2
  LANG:C++11
  TASK: concom
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>

using namespace std;

#define MAXN 100

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const string PROG = "concom";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

short N, C;
short own[MAXN][MAXN];

void disp();
void isContr();
vi controlled(short a);

void disp() {
    for (int i = 0;i < N;i++) {
        for (int j = 0;j < N;j++)
            cout << own[i][j] << " ";
        cout << endl;
    }
}

bool isContr(short a, short b) {
    // cout << a << " " << b << endl;
    // company a controls company b if...
    // a = b
    // a owns >50% of b
    // a controls K companies (C1..Ck) where Ci owns Xi% of b and X1..Xk > 50%
    if (a == b)
        return true;
    if (own[a][b] > 50)
        return true;
    cout << "test1" << endl;
    vi contr = controlled(a);
    if (contr.size() < 1)
        return false;

    cout << "test2" << endl;
    short cOwn;
    for (vi::iterator it = contr.begin();it != contr.end();it++)
        cOwn += own[*it][b];
    if (cOwn > 50)
        return true;
    cout << "Not controlled" << endl;

    return false;
}

vi controlled(short comp) {
    vi sol;
    for (int a = 0;a < C;a++) {
        cout << a << " ";
        if (isContr(a, comp))
           sol.push_back(a);
    }
    return sol;
}

int main() {
	ifstream fin (FIN);
    fin >> N;
    short i, j, p;
    for (short i = 0;i <= N;i++) {
        fin >> i >> j >> p;
        C = max(C, i);
        C = max(C, j);
        own[i-1][j-1] = p;
    }
	
    disp();
    for (int a = 0;a < C;a++)
        for (int b = 0;b < C;b++)
            if (isContr(a, b))
                cout << (a+1) << " controls " << (b+1) << endl;

	ofstream fout (FOUT);

	return 0;
}
