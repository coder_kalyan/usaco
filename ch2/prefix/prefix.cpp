/*
  ID: kalyan2
  LANG:C++11
  TASK: prefix
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <bitset>
#include <set>
#include <cstdio>
#include <cstring>
#include <map>
#include <queue>
#include <numeric>
#include <functional>

using namespace std;

#define REP(i,a,b) for (long i=a;i < b;i++)
#define LOG(a,b) clog << a << ": " << b << endl;

typedef unsigned char uchar;
typedef unsigned long ul;
typedef unsigned short us;
typedef long long ll;
typedef pair<long, long> ii;
typedef vector<long> vi;
typedef vi::iterator vi_i;
typedef vector<ii> vii;
typedef vii::iterator vii_i;
typedef queue<long> qi;
typedef priority_queue<long> pqi;

const string PROG = "prefix";
string FIN = PROG + ".in";
string FOUT = PROG + ".out";

vector<string> prim;
string seq;
vi sizes;

long maxLen = 0;

void solve(long pos) {
    cout << seq.size() << " " << pos << endl;
    if (pos > seq.size()) {
        maxLen = max(maxLen, pos);
        // cout << "breaking" << endl;
        return;
    }

    for (auto sz : sizes) {
        if ((pos + sz) > seq.size()) {
            maxLen = max(maxLen, pos);
            // cout << "cont" << endl;
            continue;
        }

        string sub = seq.substr(pos,sz);
        if (!binary_search(prim.begin(),prim.end(),sub)) {
            maxLen = max(maxLen, pos);
        } else {
            // cout << sub << " " << pos << endl;
            solve(pos+sz);
        }
    }
}

int main() {
	ifstream fin (FIN.c_str());
	ofstream fout (FOUT.c_str());

    string tmp;
    fin >> tmp;
    int sz;
    while (tmp != ".") {
        prim.push_back(tmp);
        sz = tmp.size();
        if (find(sizes.begin(),sizes.end(),sz)==sizes.end())
            sizes.push_back(sz);
        fin >> tmp;
    }
    while (fin >> tmp)
        seq += tmp;
    // fin >> seq;

    sort(prim.begin(), prim.end());

    // for (auto s : sizes)
        // cout << s << " ";
    // cout << endl;
    // for (auto s : prim)
        // cout << s << " ";
    // cout << endl;
    // cout << seq << endl;

    solve(0);
    // cout << endl;
    cout << maxLen << endl;
    fout << maxLen << endl;
	return 0;
}
