"""
ID: kalyan2
LANG: PYTHON3
TASK: inflate
"""

dp = [0] * 10001

with open("inflate.in") as f:
    M, N = map(int, f.readline().split())
    cl = []
    for i in range(N):
        cl.append(list(map(int, f.readline().split())))

#  print(M, N)
#  for c in cl:
    #  print(c)
for i in range(N):
    for j in range(cl[i][1],M + 1):
        dp[j] = max(dp[j], dp[j - cl[i][1]] + cl[i][0])

with open("inflate.out", "w") as f:
    f.write(str(dp[M]))
    f.write("\n")
