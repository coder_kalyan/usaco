#include <iostream>
#include <fstream>
#include <string>
 
using namespace std;

int N, M;
//string plain[500], spotty[500];
string plain, spotty;
int P[500][50], S[500][50], C[64];

int test_set(int i, int j, int k) {
    //if (i == j || j == k || i == k)
        //return 0;
    for (int x = 0;x < 64;x++)
        C[x] = 0;
    for (int x = 0;x < N;x++)
        C[P[x][i] * 16 + P[x][j] * 4 + P[x][k]] = 1;
    for (int x = 0;x < N;x++)
        if (C[S[x][i] * 16 + S[x][j] * 4 + S[x][k]] == 1)
            return 0;
    return 1;
}

int main() {
    ifstream fin("cownomics.in");
    ofstream fout("cownomics.out");

    fin >> N >> M;
    for (int i = 0;i < N;i++) {
        //fin >> spotty[i];
        fin >> spotty;
        for (int j = 0;j < M;j++) {
            if (spotty[j] == 'A') S[i][j] = 0;
            if (spotty[j] == 'C') S[i][j] = 1;
            if (spotty[j] == 'G') S[i][j] = 2;
            if (spotty[j] == 'T') S[i][j] = 3;
        }
    }
    for (int i = 0;i < N;i++) {
        //fin >> plain[i];
        fin >> plain;
        for (int j = 0;j < M;j++) {
            if (plain[j] == 'A') P[i][j] = 0;
            if (plain[j] == 'C') P[i][j] = 1;
            if (plain[j] == 'G') P[i][j] = 2;
            if (plain[j] == 'T') P[i][j] = 3;
        }
    }

    int ans = 0;
    for (int i =0;i < M;i++)
        for (int j = i + 1;j < M;j++)
            for (int k = j + 1;k < M;k++)
                ans += test_set(i, j, k);

    cout << ans << endl;
    fout << ans << endl;

    return 0;
}
